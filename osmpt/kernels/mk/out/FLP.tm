\begindata

PATH_VALUES = ( '/home/flp/mpt/mpt/mpt/kernels/' )

PATH_SYMBOLS = ( 'KERNELS' )

KERNELS_TO_LOAD = (

'/home/flp/mpt/mpt/mpt/kernels/lsk/naif/naif0012.tls', 
'/home/flp/mpt/mpt/mpt/kernels/spk/naif/de432s.bsp', 
'/home/flp/mpt/mpt/mpt/kernels/pck/naif/pck00010.tpc', 
'/home/flp/mpt/mpt/mpt/kernels/pck/naif/earth_000101_180827_180605.bpc', 

'/home/flp/mpt/mpt/mpt/kernels/sclk/flp.tsc', 
'/home/flp/mpt/mpt/mpt/kernels/fk/FLP.tf', 
'/home/flp/mpt/mpt/mpt/kernels/ik/instruments.ti',

'/home/flp/mpt/mpt/mpt/kernels/fk/stations/stations.tf', 
'/home/flp/mpt/mpt/mpt/kernels/spk/stations/stations.bsp',

'/home/flp/mpt/mpt/mpt/kernels/spk/out/spk_master_merge_new.bsp',

'/home/flp/mpt/mpt/mpt/kernels/ck/out/ck_master.bc',

'/home/flp/mpt/mpt/mpt/kernels/fk/targets/AACHEN.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/AALEN.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/AREA_51.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/BARCELONA.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/BAYREUTH.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/BEIJING.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/BERLIN.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/BOCHUM.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/HOF.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/HONG_KONG.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/HONOLULU.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/LAS_VEGAS.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/LONDON.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/LOS_ANGELES.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/MADRID.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/MARKTSCHORGAST.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/MILAN.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/MUNICH.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/NEW_YORK.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/SAN_FRANCISCO.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/SINGAPUR.tf',
'/home/flp/mpt/mpt/mpt/kernels/fk/targets/SYDNEY.tf',

'/home/flp/mpt/mpt/mpt/kernels/spk/targets/AACHEN.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/AALEN.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/AREA_51.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/BARCELONA.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/BAYREUTH.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/BEIJING.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/BERLIN.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/BOCHUM.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/HOF.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/HONG_KONG.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/HONOLULU.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/LAS_VEGAS.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/LONDON.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/LOS_ANGELES.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/MADRID.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/MARKTSCHORGAST.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/MILAN.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/MUNICH.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/NEW_YORK.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/SAN_FRANCISCO.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/SINGAPUR.bsp',
'/home/flp/mpt/mpt/mpt/kernels/spk/targets/SYDNEY.bsp',

