KPL/FK
 
   FILE: /home/flp/mpt/mpt/mpt/kernels/fk/targets/AACHEN.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-06-20T13:18:24
   PINPOINT DEFINITIONS FILE: /home/flp/mpt/mpt/mpt/kernels/spk/spk_target_setup
   PINPOINT PCK FILE:         /home/flp/mpt/mpt/mpt/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/mpt/mpt/mpt/kernels/spk/targets/AACHEN.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'AACHEN'
   NAIF_BODY_CODE                      += 399262
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame AACHEN_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame AACHEN_TOPO is centered at the
      site AACHEN, which has Cartesian coordinates
 
         X (km):                  0.4018664261502E+04
         Y (km):                  0.4282951678034E+03
         Z (km):                  0.4918007275809E+04
 
      and planetodetic coordinates
 
         Longitude (deg):         6.0834200000000
         Latitude  (deg):        50.7766400000000
         Altitude   (km):         0.1780000000017E+00
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_AACHEN_TOPO                   =  1399262
   FRAME_1399262_NAME                  =  'AACHEN_TOPO'
   FRAME_1399262_CLASS                 =  4
   FRAME_1399262_CLASS_ID              =  1399262
   FRAME_1399262_CENTER                =  399262
 
   OBJECT_399262_FRAME                 =  'AACHEN_TOPO'
 
   TKFRAME_1399262_RELATIVE            =  'ITRF93'
   TKFRAME_1399262_SPEC                =  'ANGLES'
   TKFRAME_1399262_UNITS               =  'DEGREES'
   TKFRAME_1399262_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399262_ANGLES              =  (   -6.0834200000000,
                                             -39.2233600000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/mpt/mpt/mpt/kernels/spk/spk_target_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'AACHEN' )
AACHEN_CENTER       = 399
AACHEN_FRAME        = 'ITRF93'
AACHEN_IDCODE       = 399262
AACHEN_LATLON       = ( 50.77664 6.08342 0.178 )
AACHEN_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
AACHEN_UP       = 'Z'
AACHEN_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 
