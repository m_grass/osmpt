KPL/FK
 
   FILE: /home/flp/H/h/kernels/fk/out/MILAN.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-04-30T11:59:35
   PINPOINT DEFINITIONS FILE: /home/flp/H/h/kernels/spk/spk_101_setup
   PINPOINT PCK FILE:         /home/flp/H/h/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/H/h/kernels/spk/targets/MILAN.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'MILAN'
   NAIF_BODY_CODE                      += 399234
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame MILAN_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame MILAN_TOPO is centered at the
      site MILAN, which has Cartesian coordinates
 
         X (km):                  0.4423535572072E+04
         Y (km):                  0.7156253227723E+03
         Z (km):                  0.4523775186577E+04
 
      and planetodetic coordinates
 
         Longitude (deg):         9.1895100000000
         Latitude  (deg):        45.4642700000000
         Altitude   (km):         0.1270000000010E+00
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_MILAN_TOPO                    =  1399234
   FRAME_1399234_NAME                  =  'MILAN_TOPO'
   FRAME_1399234_CLASS                 =  4
   FRAME_1399234_CLASS_ID              =  1399234
   FRAME_1399234_CENTER                =  399234
 
   OBJECT_399234_FRAME                 =  'MILAN_TOPO'
 
   TKFRAME_1399234_RELATIVE            =  'ITRF93'
   TKFRAME_1399234_SPEC                =  'ANGLES'
   TKFRAME_1399234_UNITS               =  'DEGREES'
   TKFRAME_1399234_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399234_ANGLES              =  (   -9.1895100000000,
                                             -44.5357300000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/H/h/kernels/spk/spk_101_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'MILAN' )
MILAN_CENTER       = 399
MILAN_FRAME        = 'ITRF93'
MILAN_IDCODE       = 399234
MILAN_LATLON       = ( 45.46427 9.18951 0.127 )
MILAN_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
MILAN_UP       = 'Z'
MILAN_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 
