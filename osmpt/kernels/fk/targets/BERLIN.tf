KPL/FK
 
   FILE: /home/flp/H/h/kernels/fk/out/BERLIN.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-04-30T17:46:15
   PINPOINT DEFINITIONS FILE: /home/flp/H/h/kernels/spk/spk_101_setup
   PINPOINT PCK FILE:         /home/flp/H/h/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/H/h/kernels/spk/targets/BERLIN.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'BERLIN'
   NAIF_BODY_CODE                      += 399235
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame BERLIN_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame BERLIN_TOPO is centered at the
      site BERLIN, which has Cartesian coordinates
 
         X (km):                  0.3782807846049E+04
         Y (km):                  0.9019266527403E+03
         Z (km):                  0.5038548772289E+04
 
      and planetodetic coordinates
 
         Longitude (deg):        13.4105300000000
         Latitude  (deg):        52.5243700000000
         Altitude   (km):         0.4300000000099E-01
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_BERLIN_TOPO                   =  1399235
   FRAME_1399235_NAME                  =  'BERLIN_TOPO'
   FRAME_1399235_CLASS                 =  4
   FRAME_1399235_CLASS_ID              =  1399235
   FRAME_1399235_CENTER                =  399235
 
   OBJECT_399235_FRAME                 =  'BERLIN_TOPO'
 
   TKFRAME_1399235_RELATIVE            =  'ITRF93'
   TKFRAME_1399235_SPEC                =  'ANGLES'
   TKFRAME_1399235_UNITS               =  'DEGREES'
   TKFRAME_1399235_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399235_ANGLES              =  (  -13.4105300000000,
                                             -37.4756300000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/H/h/kernels/spk/spk_101_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'BERLIN' )
BERLIN_CENTER       = 399
BERLIN_FRAME        = 'ITRF93'
BERLIN_IDCODE       = 399235
BERLIN_LATLON       = ( 52.52437 13.41053 0.043 )
BERLIN_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
BERLIN_UP       = 'Z'
BERLIN_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 
