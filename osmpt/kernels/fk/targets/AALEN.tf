KPL/FK
 
   FILE: /home/flp/H/h/kernels/fk/targets/AALEN.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-06-13T15:41:55
   PINPOINT DEFINITIONS FILE: /home/flp/H/h/kernels/spk/spk_target_setup
   PINPOINT PCK FILE:         /home/flp/H/h/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/H/h/kernels/spk/targets/AALEN.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'AALEN'
   NAIF_BODY_CODE                      += 399257
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame AALEN_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame AALEN_TOPO is centered at the
      site AALEN, which has Cartesian coordinates
 
         X (km):                  0.4141220509918E+04
         Y (km):                  0.7371641077996E+03
         Z (km):                  0.4779028995802E+04
 
      and planetodetic coordinates
 
         Longitude (deg):        10.0933000000000
         Latitude  (deg):        48.8377700000000
         Altitude   (km):         0.4330000000018E+00
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_AALEN_TOPO                    =  1399257
   FRAME_1399257_NAME                  =  'AALEN_TOPO'
   FRAME_1399257_CLASS                 =  4
   FRAME_1399257_CLASS_ID              =  1399257
   FRAME_1399257_CENTER                =  399257
 
   OBJECT_399257_FRAME                 =  'AALEN_TOPO'
 
   TKFRAME_1399257_RELATIVE            =  'ITRF93'
   TKFRAME_1399257_SPEC                =  'ANGLES'
   TKFRAME_1399257_UNITS               =  'DEGREES'
   TKFRAME_1399257_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399257_ANGLES              =  (  -10.0933000000000,
                                             -41.1622300000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/H/h/kernels/spk/spk_target_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'AALEN' )
AALEN_CENTER       = 399
AALEN_FRAME        = 'ITRF93'
AALEN_IDCODE       = 399257
AALEN_LATLON       = ( 48.83777 10.0933 0.433 )
AALEN_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
AALEN_UP       = 'Z'
AALEN_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 
