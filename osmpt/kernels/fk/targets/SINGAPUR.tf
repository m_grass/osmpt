KPL/FK
 
   FILE: /home/flp/H/h/kernels/fk/targets/SINGAPUR.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-05-06T16:35:19
   PINPOINT DEFINITIONS FILE: /home/flp/H/h/kernels/spk/spk_101_setup
   PINPOINT PCK FILE:         /home/flp/H/h/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/H/h/kernels/spk/targets/SINGAPUR.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'SINGAPUR'
   NAIF_BODY_CODE                      += 399247
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame SINGAPUR_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame SINGAPUR_TOPO is centered at the
      site SINGAPUR, which has Cartesian coordinates
 
         X (km):                 -0.1526432596244E+04
         Y (km):                  0.6191159310374E+04
         Z (km):                  0.1425930345482E+03
 
      and planetodetic coordinates
 
         Longitude (deg):       103.8500700000000
         Latitude  (deg):         1.2896700000000
         Altitude   (km):         0.2299999999990E-01
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_SINGAPUR_TOPO                 =  1399247
   FRAME_1399247_NAME                  =  'SINGAPUR_TOPO'
   FRAME_1399247_CLASS                 =  4
   FRAME_1399247_CLASS_ID              =  1399247
   FRAME_1399247_CENTER                =  399247
 
   OBJECT_399247_FRAME                 =  'SINGAPUR_TOPO'
 
   TKFRAME_1399247_RELATIVE            =  'ITRF93'
   TKFRAME_1399247_SPEC                =  'ANGLES'
   TKFRAME_1399247_UNITS               =  'DEGREES'
   TKFRAME_1399247_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399247_ANGLES              =  ( -103.8500700000000,
                                             -88.7103300000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/H/h/kernels/spk/spk_101_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'SINGAPUR' )
SINGAPUR_CENTER       = 399
SINGAPUR_FRAME        = 'ITRF93'
SINGAPUR_IDCODE       = 399247
SINGAPUR_LATLON       = ( 1.28967 103.85007 0.023 )
SINGAPUR_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
SINGAPUR_UP       = 'Z'
SINGAPUR_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 
