KPL/FK
 
   FILE: /home/flp/H/h/kernels/fk/targets/AREA_51.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-05-06T17:14:46
   PINPOINT DEFINITIONS FILE: /home/flp/H/h/kernels/spk/spk_101_setup
   PINPOINT PCK FILE:         /home/flp/H/h/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/H/h/kernels/spk/targets/AREA_51.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'AREA_51'
   NAIF_BODY_CODE                      += 399248
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame AREA_51_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame AREA_51_TOPO is centered at the
      site AREA_51, which has Cartesian coordinates
 
         X (km):                 -0.2207325634023E+04
         Y (km):                 -0.4444738904850E+04
         Z (km):                  0.3993458546230E+04
 
      and planetodetic coordinates
 
         Longitude (deg):      -116.4097010000000
         Latitude  (deg):        39.0117760000000
         Altitude   (km):         0.2000000000002E+00
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_AREA_51_TOPO                  =  1399248
   FRAME_1399248_NAME                  =  'AREA_51_TOPO'
   FRAME_1399248_CLASS                 =  4
   FRAME_1399248_CLASS_ID              =  1399248
   FRAME_1399248_CENTER                =  399248
 
   OBJECT_399248_FRAME                 =  'AREA_51_TOPO'
 
   TKFRAME_1399248_RELATIVE            =  'ITRF93'
   TKFRAME_1399248_SPEC                =  'ANGLES'
   TKFRAME_1399248_UNITS               =  'DEGREES'
   TKFRAME_1399248_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399248_ANGLES              =  ( -243.5902990000000,
                                             -50.9882240000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/H/h/kernels/spk/spk_101_setup
--------------------------------------------------------------------------------
 
begindata
 
SITES                      += ( 'AREA_51' )
AREA_51_CENTER       = 399
AREA_51_FRAME        = 'ITRF93'
AREA_51_IDCODE       = 399248
AREA_51_LATLON       = ( 39.011776 -116.409701 0.2 )
AREA_51_BOUNDS       = (@2017-JAN-1, @2020-JAN-1)
AREA_51_UP       = 'Z'
AREA_51_NORTH      = 'X'
 
begintext
 
 
begintext
 
[End of definitions file]
 
