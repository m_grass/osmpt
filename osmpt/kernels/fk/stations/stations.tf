KPL/FK
 
   FILE: /home/flp/mpt/mpt/mpt/kernels/fk/stations/stations.tf
 
   This file was created by PINPOINT.
 
   PINPOINT Version 3.2.0 --- September 6, 2016
   PINPOINT RUN DATE/TIME:    2018-06-20T20:05:29
   PINPOINT DEFINITIONS FILE: /home/flp/mpt/mpt/mpt/kernels/spk/station_setup
   PINPOINT PCK FILE:         /home/flp/mpt/mpt/mpt/kernels/pck/naif/pck00010.tpc
   PINPOINT SPK FILE:         /home/flp/mpt/mpt/mpt/kernels/spk/stations/stations.bsp
 
   The input definitions file is appended to this
   file as a comment block.
 
 
   Body-name mapping follows:
 
\begindata
 
   NAIF_BODY_NAME                      += 'IRS'
   NAIF_BODY_CODE                      += 399101
 
   NAIF_BODY_NAME                      += 'GS1'
   NAIF_BODY_CODE                      += 399102
 
   NAIF_BODY_NAME                      += 'HAM'
   NAIF_BODY_CODE                      += 399103
 
   NAIF_BODY_NAME                      += 'INU'
   NAIF_BODY_CODE                      += 399104
 
   NAIF_BODY_NAME                      += 'KIE'
   NAIF_BODY_CODE                      += 399105
 
   NAIF_BODY_NAME                      += 'MAL'
   NAIF_BODY_CODE                      += 399106
 
   NAIF_BODY_NAME                      += 'NYA'
   NAIF_BODY_CODE                      += 399107
 
   NAIF_BODY_NAME                      += 'OHG'
   NAIF_BODY_CODE                      += 399108
 
   NAIF_BODY_NAME                      += 'OPH'
   NAIF_BODY_CODE                      += 399109
 
   NAIF_BODY_NAME                      += 'WHM'
   NAIF_BODY_CODE                      += 399110
 
\begintext
 
 
   Reference frame specifications follow:
 
 
   Topocentric frame IRS_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame IRS_TOPO is centered at the
      site IRS, which has Cartesian coordinates
 
         X (km):                  0.4160668879061E+04
         Y (km):                  0.6667171879098E+03
         Z (km):                  0.4772612328157E+04
 
      and planetodetic coordinates
 
         Longitude (deg):         9.1038400000000
         Latitude  (deg):        48.7495000000000
         Altitude   (km):         0.5000000000004E+00
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_IRS_TOPO                      =  1399101
   FRAME_1399101_NAME                  =  'IRS_TOPO'
   FRAME_1399101_CLASS                 =  4
   FRAME_1399101_CLASS_ID              =  1399101
   FRAME_1399101_CENTER                =  399101
 
   OBJECT_399101_FRAME                 =  'IRS_TOPO'
 
   TKFRAME_1399101_RELATIVE            =  'ITRF93'
   TKFRAME_1399101_SPEC                =  'ANGLES'
   TKFRAME_1399101_UNITS               =  'DEGREES'
   TKFRAME_1399101_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399101_ANGLES              =  (   -9.1038400000000,
                                             -41.2505000000000,
                                             180.0000000000000 )
 
 
\begintext
 
   Topocentric frame GS1_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame GS1_TOPO is centered at the
      site GS1, which has Cartesian coordinates
 
         X (km):                  0.5390245839570E+04
         Y (km):                 -0.1597871151659E+04
         Z (km):                  0.3007041397579E+04
 
      and planetodetic coordinates
 
         Longitude (deg):       -16.5118000000000
         Latitude  (deg):        28.3009000000000
         Altitude   (km):         0.2393000000000E+01
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_GS1_TOPO                      =  1399102
   FRAME_1399102_NAME                  =  'GS1_TOPO'
   FRAME_1399102_CLASS                 =  4
   FRAME_1399102_CLASS_ID              =  1399102
   FRAME_1399102_CENTER                =  399102
 
   OBJECT_399102_FRAME                 =  'GS1_TOPO'
 
   TKFRAME_1399102_RELATIVE            =  'ITRF93'
   TKFRAME_1399102_SPEC                =  'ANGLES'
   TKFRAME_1399102_UNITS               =  'DEGREES'
   TKFRAME_1399102_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399102_ANGLES              =  ( -343.4882000000000,
                                             -61.6991000000000,
                                             180.0000000000000 )
 
 
\begintext
 
   Topocentric frame HAM_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame HAM_TOPO is centered at the
      site HAM, which has Cartesian coordinates
 
         X (km):                 -0.2398285970656E+04
         Y (km):                 -0.1413036709562E+04
         Z (km):                  0.5719819023748E+04
 
      and planetodetic coordinates
 
         Longitude (deg):      -149.4940000000000
         Latitude  (deg):        64.2008000000000
         Altitude   (km):         0.3630000000003E+00
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_HAM_TOPO                      =  1399103
   FRAME_1399103_NAME                  =  'HAM_TOPO'
   FRAME_1399103_CLASS                 =  4
   FRAME_1399103_CLASS_ID              =  1399103
   FRAME_1399103_CENTER                =  399103
 
   OBJECT_399103_FRAME                 =  'HAM_TOPO'
 
   TKFRAME_1399103_RELATIVE            =  'ITRF93'
   TKFRAME_1399103_SPEC                =  'ANGLES'
   TKFRAME_1399103_UNITS               =  'DEGREES'
   TKFRAME_1399103_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399103_ANGLES              =  ( -210.5060000000000,
                                             -25.7992000000000,
                                             180.0000000000000 )
 
 
\begintext
 
   Topocentric frame INU_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame INU_TOPO is centered at the
      site INU, which has Cartesian coordinates
 
         X (km):                 -0.1628089429530E+04
         Y (km):                 -0.1713011753305E+04
         Z (km):                  0.5904389799978E+04
 
      and planetodetic coordinates
 
         Longitude (deg):      -133.5440000000000
         Latitude  (deg):        68.3183000000000
         Altitude   (km):         0.9600000000035E-01
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_INU_TOPO                      =  1399104
   FRAME_1399104_NAME                  =  'INU_TOPO'
   FRAME_1399104_CLASS                 =  4
   FRAME_1399104_CLASS_ID              =  1399104
   FRAME_1399104_CENTER                =  399104
 
   OBJECT_399104_FRAME                 =  'INU_TOPO'
 
   TKFRAME_1399104_RELATIVE            =  'ITRF93'
   TKFRAME_1399104_SPEC                =  'ANGLES'
   TKFRAME_1399104_UNITS               =  'DEGREES'
   TKFRAME_1399104_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399104_ANGLES              =  ( -226.4560000000000,
                                             -21.6817000000000,
                                             180.0000000000000 )
 
 
\begintext
 
   Topocentric frame KIE_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame KIE_TOPO is centered at the
      site KIE, which has Cartesian coordinates
 
         X (km):                  0.3674705530575E+04
         Y (km):                  0.6599788933728E+03
         Z (km):                  0.5153889317077E+04
 
      and planetodetic coordinates
 
         Longitude (deg):        10.1818000000000
         Latitude  (deg):        54.2629000000000
         Altitude   (km):         0.0000000000000E+00
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_KIE_TOPO                      =  1399105
   FRAME_1399105_NAME                  =  'KIE_TOPO'
   FRAME_1399105_CLASS                 =  4
   FRAME_1399105_CLASS_ID              =  1399105
   FRAME_1399105_CENTER                =  399105
 
   OBJECT_399105_FRAME                 =  'KIE_TOPO'
 
   TKFRAME_1399105_RELATIVE            =  'ITRF93'
   TKFRAME_1399105_SPEC                =  'ANGLES'
   TKFRAME_1399105_UNITS               =  'DEGREES'
   TKFRAME_1399105_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399105_ANGLES              =  (  -10.1818000000000,
                                             -35.7371000000000,
                                             180.0000000000000 )
 
 
\begintext
 
   Topocentric frame MAL_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame MAL_TOPO is centered at the
      site MAL, which has Cartesian coordinates
 
         X (km):                 -0.1293723612208E+04
         Y (km):                  0.6236739143797E+04
         Z (km):                  0.3305362863433E+03
 
      and planetodetic coordinates
 
         Longitude (deg):       101.7190000000000
         Latitude  (deg):         2.9906000000000
         Altitude   (km):         0.1457594859013E-11
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_MAL_TOPO                      =  1399106
   FRAME_1399106_NAME                  =  'MAL_TOPO'
   FRAME_1399106_CLASS                 =  4
   FRAME_1399106_CLASS_ID              =  1399106
   FRAME_1399106_CENTER                =  399106
 
   OBJECT_399106_FRAME                 =  'MAL_TOPO'
 
   TKFRAME_1399106_RELATIVE            =  'ITRF93'
   TKFRAME_1399106_SPEC                =  'ANGLES'
   TKFRAME_1399106_UNITS               =  'DEGREES'
   TKFRAME_1399106_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399106_ANGLES              =  ( -101.7190000000000,
                                             -87.0094000000000,
                                             180.0000000000000 )
 
 
\begintext
 
   Topocentric frame NYA_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame NYA_TOPO is centered at the
      site NYA, which has Cartesian coordinates
 
         X (km):                  0.1202545576501E+04
         Y (km):                  0.2530544348595E+03
         Z (km):                  0.6237696852499E+04
 
      and planetodetic coordinates
 
         Longitude (deg):        11.8835000000000
         Latitude  (deg):        78.9277000000000
         Altitude   (km):         0.4800000000069E-01
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_NYA_TOPO                      =  1399107
   FRAME_1399107_NAME                  =  'NYA_TOPO'
   FRAME_1399107_CLASS                 =  4
   FRAME_1399107_CLASS_ID              =  1399107
   FRAME_1399107_CENTER                =  399107
 
   OBJECT_399107_FRAME                 =  'NYA_TOPO'
 
   TKFRAME_1399107_RELATIVE            =  'ITRF93'
   TKFRAME_1399107_SPEC                =  'ANGLES'
   TKFRAME_1399107_UNITS               =  'DEGREES'
   TKFRAME_1399107_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399107_ANGLES              =  (  -11.8835000000000,
                                             -11.0723000000000,
                                             180.0000000000000 )
 
 
\begintext
 
   Topocentric frame OHG_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame OHG_TOPO is centered at the
      site OHG, which has Cartesian coordinates
 
         X (km):                  0.1525828513386E+04
         Y (km):                 -0.2432454515704E+04
         Z (km):                 -0.5676147872801E+04
 
      and planetodetic coordinates
 
         Longitude (deg):       -57.9008000000000
         Latitude  (deg):       -63.3211000000000
         Altitude   (km):         0.1200000000051E-01
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_OHG_TOPO                      =  1399108
   FRAME_1399108_NAME                  =  'OHG_TOPO'
   FRAME_1399108_CLASS                 =  4
   FRAME_1399108_CLASS_ID              =  1399108
   FRAME_1399108_CENTER                =  399108
 
   OBJECT_399108_FRAME                 =  'OHG_TOPO'
 
   TKFRAME_1399108_RELATIVE            =  'ITRF93'
   TKFRAME_1399108_SPEC                =  'ANGLES'
   TKFRAME_1399108_UNITS               =  'DEGREES'
   TKFRAME_1399108_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399108_ANGLES              =  ( -302.0992000000001,
                                            -153.3211000000000,
                                             180.0000000000000 )
 
 
\begintext
 
   Topocentric frame OPH_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame OPH_TOPO is centered at the
      site OPH, which has Cartesian coordinates
 
         X (km):                  0.4186672102597E+04
         Y (km):                  0.8349156100544E+03
         Z (km):                  0.4723614655267E+04
 
      and planetodetic coordinates
 
         Longitude (deg):        11.2781000000000
         Latitude  (deg):        48.0847000000000
         Altitude   (km):         0.5940000000012E+00
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_OPH_TOPO                      =  1399109
   FRAME_1399109_NAME                  =  'OPH_TOPO'
   FRAME_1399109_CLASS                 =  4
   FRAME_1399109_CLASS_ID              =  1399109
   FRAME_1399109_CENTER                =  399109
 
   OBJECT_399109_FRAME                 =  'OPH_TOPO'
 
   TKFRAME_1399109_RELATIVE            =  'ITRF93'
   TKFRAME_1399109_SPEC                =  'ANGLES'
   TKFRAME_1399109_UNITS               =  'DEGREES'
   TKFRAME_1399109_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399109_ANGLES              =  (  -11.2781000000000,
                                             -41.9153000000000,
                                             180.0000000000000 )
 
 
\begintext
 
   Topocentric frame WHM_TOPO
 
      The Z axis of this frame points toward the zenith.
      The X axis of this frame points North.
 
      Topocentric frame WHM_TOPO is centered at the
      site WHM, which has Cartesian coordinates
 
         X (km):                  0.4206090958125E+04
         Y (km):                  0.8240816284107E+03
         Z (km):                  0.4708436865878E+04
 
      and planetodetic coordinates
 
         Longitude (deg):        11.0853000000000
         Latitude  (deg):        47.8801000000000
         Altitude   (km):         0.6630000000019E+00
 
      These planetodetic coordinates are expressed relative to
      a reference spheroid having the dimensions
 
         Equatorial radius (km):  6.3781366000000E+03
         Polar radius      (km):  6.3567519000000E+03
 
      All of the above coordinates are relative to the frame ITRF93.
 
 
\begindata
 
   FRAME_WHM_TOPO                      =  1399110
   FRAME_1399110_NAME                  =  'WHM_TOPO'
   FRAME_1399110_CLASS                 =  4
   FRAME_1399110_CLASS_ID              =  1399110
   FRAME_1399110_CENTER                =  399110
 
   OBJECT_399110_FRAME                 =  'WHM_TOPO'
 
   TKFRAME_1399110_RELATIVE            =  'ITRF93'
   TKFRAME_1399110_SPEC                =  'ANGLES'
   TKFRAME_1399110_UNITS               =  'DEGREES'
   TKFRAME_1399110_AXES                =  ( 3, 2, 3 )
   TKFRAME_1399110_ANGLES              =  (  -11.0853000000000,
                                             -42.1199000000000,
                                             180.0000000000000 )
 
\begintext
 
 
Definitions file /home/flp/mpt/mpt/mpt/kernels/spk/station_setup
--------------------------------------------------------------------------------
 
begindata
 
      SITES += ( 'IRS' )
      IRS_CENTER = 399
      IRS_FRAME  = 'ITRF93'
      IRS_IDCODE =  399101
      IRS_LATLON = ( 48.7495, 9.10384 0.500 )
      IRS_BOUNDS = ( @2017-JAN-1, @2025-JAN-1 )
      IRS_UP    =  'Z'
      IRS_NORTH =  'X'
 
begintext
 
begindata
 
      SITES += ( 'GS1' )
      GS1_CENTER = 399
      GS1_FRAME  = 'ITRF93'
      GS1_IDCODE =  399102
      GS1_LATLON = ( 28.3009 -16.5118 2.393 )
      GS1_BOUNDS = ( @2017-JAN-1, @2025-JAN-1 )
      GS1_UP    =  'Z'
      GS1_NORTH =  'X'
 
begintext
 
begindata
 
      SITES += ( 'HAM' )
      HAM_CENTER = 399
      HAM_FRAME  = 'ITRF93'
      HAM_IDCODE =  399103
      HAM_LATLON = ( 64.2008 -149.494 0.363 )
      HAM_BOUNDS = ( @2017-JAN-1, @2025-JAN-1 )
      HAM_UP    =  'Z'
      HAM_NORTH =  'X'
 
begintext
 
begindata
 
      SITES += ( 'INU' )
      INU_CENTER = 399
      INU_FRAME  = 'ITRF93'
      INU_IDCODE =  399104
      INU_LATLON = ( 68.3183 -133.544 0.096 )
      INU_BOUNDS = ( @2017-JAN-1, @2025-JAN-1 )
      INU_UP    =  'Z'
      INU_NORTH =  'X'
 
begintext
 
begindata
 
      SITES += ( 'KIE' )
      KIE_CENTER = 399
      KIE_FRAME  = 'ITRF93'
      KIE_IDCODE =  399105
      KIE_LATLON = ( 54.2629 10.1818 0.000 )
      KIE_BOUNDS = ( @2017-JAN-1, @2025-JAN-1 )
      KIE_UP    =  'Z'
      KIE_NORTH =  'X'
 
begintext
 
begindata
 
      SITES += ( 'MAL' )
      MAL_CENTER = 399
      MAL_FRAME  = 'ITRF93'
      MAL_IDCODE =  399106
      MAL_LATLON = ( 2.9906 101.719 0.000 )
      MAL_BOUNDS = ( @2017-JAN-1, @2025-JAN-1 )
      MAL_UP    =  'Z'
      MAL_NORTH =  'X'
 
begintext
 
begindata
 
      SITES += ( 'NYA' )
      NYA_CENTER = 399
      NYA_FRAME  = 'ITRF93'
      NYA_IDCODE =  399107
      NYA_LATLON = ( 78.9277 11.8835 0.048 )
      NYA_BOUNDS = ( @2017-JAN-1, @2025-JAN-1 )
      NYA_UP    =  'Z'
      NYA_NORTH =  'X'
 
begintext
 
begindata
 
      SITES += ( 'OHG' )
      OHG_CENTER = 399
      OHG_FRAME  = 'ITRF93'
      OHG_IDCODE =  399108
      OHG_LATLON = (-63.3211 -57.9008 0.012 )
      OHG_BOUNDS = ( @2017-JAN-1, @2025-JAN-1 )
      OHG_UP    =  'Z'
      OHG_NORTH =  'X'
 
begintext
 
begindata
 
      SITES += ( 'OPH' )
      OPH_CENTER = 399
      OPH_FRAME  = 'ITRF93'
      OPH_IDCODE =  399109
      OPH_LATLON = ( 48.0847 11.2781 0.594 )
      OPH_BOUNDS = ( @2017-JAN-1, @2025-JAN-1 )
      OPH_UP    =  'Z'
      OPH_NORTH =  'X'
 
begintext
 
begindata
 
      SITES += ( 'WHM' )
      WHM_CENTER = 399
      WHM_FRAME  = 'ITRF93'
      WHM_IDCODE =  399110
      WHM_LATLON = ( 47.8801 11.0853 0.663 )
      WHM_BOUNDS = ( @2017-JAN-1, @2025-JAN-1 )
      WHM_UP    =  'Z'
      WHM_NORTH =  'X'
 
begintext
 
begintext
 
[End of definitions file]
 
