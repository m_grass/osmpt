KPL/FK

FLP name to id mapping:

\begindata

      NAIF_BODY_CODE  += ( -513 )
      NAIF_BODY_NAME  += ( 'FLP' )

\begintext


FLP SC frame:

\begindata

      NAIF_BODY_CODE  += ( -513000 )
      NAIF_BODY_NAME  += ( 'FLP_SPACECRAFT' )

      FRAME_FLP_SPACECRAFT        = -513000
      FRAME_-513000_NAME             = 'FLP_SPACECRAFT'
      FRAME_-513000_CLASS            = 3
      FRAME_-513000_CLASS_ID         = -513000
      FRAME_-513000_CENTER           = -513
      CK_-513000_SCLK                = -513
      CK_-513000_SPK                 = -513

\begintext



FLP PAMCAM frame:

\begindata

      NAIF_BODY_CODE  += ( -513100 )
      NAIF_BODY_NAME  += ( 'FLP_PAMCAM' )

      FRAME_FLP_PAMCAM                =  -513100
      FRAME_-513100_NAME                = 'FLP_PAMCAM'
      FRAME_-513100_CLASS               =  4
      FRAME_-513100_CLASS_ID            =  -513100
      FRAME_-513100_CENTER              =  -513
      TKFRAME_-513100_RELATIVE          = 'FLP_SPACECRAFT'
      TKFRAME_-513100_SPEC              = 'ANGLES'
      TKFRAME_-513100_UNITS             = 'DEGREES'
      TKFRAME_-513100_AXES              = ( 1,   2,   3   )
      TKFRAME_-513100_ANGLES            = ( 0.0, 0.0, 0.0 )

\begintext



FLP OSIRIS frame:

\begindata

      NAIF_BODY_CODE  += ( -513200 )
      NAIF_BODY_NAME  += ( 'FLP_OSIRIS' )

      FRAME_FLP_OSIRIS                  =  -513200
      FRAME_-513200_NAME                = 'FLP_OSIRIS'
      FRAME_-513200_CLASS               =  4
      FRAME_-513200_CLASS_ID            =  -513200
      FRAME_-513200_CENTER              =  -513
      TKFRAME_-513200_RELATIVE          = 'FLP_SPACECRAFT'
      TKFRAME_-513200_SPEC              = 'ANGLES'
      TKFRAME_-513200_UNITS             = 'DEGREES'
      TKFRAME_-513200_AXES              = ( 1,   2,   3   )
      TKFRAME_-513200_ANGLES            = ( 0.0, 0.0, 0.0 )

\begintext


FLP MICS frame:

\begindata

      NAIF_BODY_CODE  += ( -513300 )
      NAIF_BODY_NAME  += ( 'FLP_MICS' )

      FRAME_FLP_MICS                    =  -513300
      FRAME_-513300_NAME                = 'FLP_MICS'
      FRAME_-513300_CLASS               =  4
      FRAME_-513300_CLASS_ID            =  -513300
      FRAME_-513300_CENTER              =  -513
      TKFRAME_-513300_RELATIVE          = 'FLP_SPACECRAFT'
      TKFRAME_-513300_SPEC              = 'ANGLES'
      TKFRAME_-513300_UNITS             = 'DEGREES'
      TKFRAME_-513300_AXES              = ( 1,   2,   3   )
      TKFRAME_-513300_ANGLES            = ( 0.0, 0.0, 0.0 )

\begintext


Dynamic frame needed to be used to check for sign in the quaterion_angle.py script
(rotation direction around velocity vector)

\begindata

      FRAME_FLP_ANGLE_SIGN             =  -513002
      FRAME_-513002_NAME               =  'FLP_ANGLE_SIGN'
      FRAME_-513002_CLASS              =  5
      FRAME_-513002_CLASS_ID           =  -513002
      FRAME_-513002_CENTER             =  513

      FRAME_-513002_RELATIVE           =  'J2000'
      FRAME_-513002_DEF_STYLE          =  'PARAMETERIZED'
      FRAME_-513002_FAMILY             =  'TWO-VECTOR'

      FRAME_-513002_PRI_AXIS           = 'X'
      FRAME_-513002_PRI_VECTOR_DEF     = 'OBSERVER_TARGET_VELOCITY'
      FRAME_-513002_PRI_OBSERVER       = 'EARTH'
      FRAME_-513002_PRI_TARGET         = -513
      FRAME_-513002_PRI_FRAME          = 'J2000'
      FRAME_-513002_PRI_ABCORR         = 'NONE'

      FRAME_-513002_SEC_AXIS           =  'Z'
      FRAME_-513002_SEC_VECTOR_DEF     = 'OBSERVER_TARGET_POSITION'
      FRAME_-513002_SEC_OBSERVER       = -513
      FRAME_-513002_SEC_TARGET         = 'EARTH'
      FRAME_-513002_SEC_ABCORR         = 'NONE'

\begintext


Dynamic frame to validate values (not used anymore, could be deleted)

\begindata

      FRAME_FLP_T                  =  -513001
      FRAME_-513001_NAME               =  'FLP_T'
      FRAME_-513001_CLASS              =  5
      FRAME_-513001_CLASS_ID           =  -513001
      FRAME_-513001_CENTER             =  399

      FRAME_-513001_RELATIVE           =  'J2000'
      FRAME_-513001_DEF_STYLE          =  'PARAMETERIZED'
      FRAME_-513001_FAMILY             =  'TWO-VECTOR'

      FRAME_-513001_PRI_AXIS           =  'Z'
      FRAME_-513001_PRI_VECTOR_DEF = 'OBSERVER_TARGET_POSITION'
      FRAME_-513001_PRI_OBSERVER   = -513
      FRAME_-513001_PRI_TARGET     = 'IRS'
      FRAME_-513001_PRI_ABCORR     = 'NONE'

      FRAME_-513001_SEC_AXIS           = 'X'
      FRAME_-513001_SEC_VECTOR_DEF = 'OBSERVER_TARGET_VELOCITY'
      FRAME_-513001_SEC_OBSERVER   = 'EARTH'
      FRAME_-513001_SEC_TARGET     = -513
      FRAME_-513001_SEC_FRAME      = 'J2000'
      FRAME_-513001_SEC_ABCORR     = 'NONE'

\begintext