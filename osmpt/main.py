'''
This is the master file used to produce the pip install mpt resources. This means you should change the project directory
every time you want to produce a resource to the pypi directory. This needs to be done that the installed package works
smoothly inside the VM box.
Git is using a slightly different project directory...

Because the file is quite long it takes a while until pycharm checked all cross-connections to see if variables are
available or not...
'''

import os
import subprocess
import csv
import datetime
import re
import matplotlib.pyplot as plt
import numpy as np
import spiceypy as cspice
import random
import json
from collections import OrderedDict
from mpl_toolkits.basemap import Basemap
from matplotlib.patches import Polygon


# ------------------------ MAIN FUNCTIONS ------------------------
# ----------------------------------------------------------------
# ----------------------------------------------------------------

# ----------------------------------------------------------------
# Programs
# ----------------------------------------------------------------



def path_setup():
    '''
    This script stores all hard coded paths used within this project. It therefore defines the folder structure of the
    project. The paths philosophy follows the below mentioned ideas:
    - keep the folder structure simple and intuitive.
    - create new folders where needed to remove old files easily without affecting other parts of the project
    - keep names during development phase to not produce new errors even it more intuitive folder names/combinations might
    be useful until you do a general update...
    Nevertheless the paths are grouped dependent on there first script usage (following a "normal" user flow).
    So far, no hard naming conventions were defined.
    :return: All paths needed
    '''

    class flp_path_class:

        # the paths are ordered the way they are first used except naif input files, which are listed first
        def __init__(self):
            # define path beginnings:
            home_dir = '/home/flp/'

            # directory for git: (development)
            project_dir = '/home/flp/mpt/mpt/mpt/'
            # directory for pypi:
            #project_dir = '/home/flp/mpt/'

            # naif input files
            self.lsk_file = project_dir + 'kernels/lsk/naif/naif0012.tls'
            self.pck_normal = project_dir + 'kernels/pck/naif/pck00010.tpc'
            self.pck_itrf = project_dir + 'kernels/pck/naif/earth_000101_180827_180605.bpc'
            self.pck_geo = project_dir + 'kernels/pck/naif/geophysical.ker'
            self.spk_file = project_dir + 'kernels/spk/naif/de432s.bsp'

            # static self-generated files
            self.sclk_file = project_dir + 'kernels/sclk/flp.tsc'
            self.fk_file = project_dir + 'kernels/fk/FLP.tf'
            self.ik_file = project_dir + 'kernels/ik/instruments.ti'

            # spk_ck_creation
            self.TM_path = project_dir + 'TM/'
            self.spk_source = project_dir + 'kernels/spk/source/'
            self.spk_master_mkspk_setup = project_dir + 'kernels/spk/spk_master_mkspk_setup_auto'
            self.spk_master_mkspk_comment = project_dir + 'kernels/spk/spk_master_mkspk_comment'
            self.spk_new = project_dir + 'kernels/spk/new/'
            self.spk_out_name = project_dir + 'kernels/spk/out/spk_master_merge_new.bsp'
            self.spk_master_merge_setup_1 = project_dir + 'kernels/spk/spk_master_spkmerge_setup'
            self.spk_out = project_dir + 'kernels/spk/out/'
            self.ck_source = project_dir + 'kernels/ck/source/'
            self.ck_master_setup = project_dir + 'kernels/ck/msopck.setup'
            self.ck_master_comments = project_dir + 'kernels/ck/msopck.comments'
            self.ck_out = project_dir + 'kernels/ck/out/'
            self.ck_out_name = project_dir + 'kernels/ck/out/ck_master.bc'

            # gmat related pathes (gaps)
            self.gmat = home_dir + 'GMAT/R2017a/bin/'
            self.gmat_default_script = project_dir + 'kernels/spk/spk_gmat_default.script'
            self.spk_gmat_merge_setup = project_dir + 'kernels/spk/spk_gmat_merge_setup'
            self.gmat_storage = project_dir + 'kernels/spk/new/'
            self.gmat_out = project_dir + 'kernels/spk/gmat/'
            self.gmat_out_name = project_dir + 'kernels/spk/gmat/spk_gmat_merge_new.bsp'

            # create target
            self.json_target_old = project_dir + 'json/targets/old/'
            self.json_target_new = project_dir + 'json/targets/new/'
            self.json_scenario = project_dir + 'json/load_flp_scenario.json'
            self.spk_target_setup = project_dir + 'kernels/spk/spk_target_setup'
            self.spk_target_id_code = project_dir + 'kernels/spk/ID_code_critical.txt'
            self.spk_target = project_dir + 'kernels/spk/targets/'
            self.fk_target = project_dir + 'kernels/fk/targets/'

            # create observation
            self.json_obs_old = project_dir + 'json/observations/old/'
            self.json_obs_new = project_dir + 'json/observations/new/'

            # create mk
            self.mk_sample_path = project_dir + 'kernels/'
            self.mk_out = project_dir + 'kernels/mk/out/FLP.tm'

            # create stations
            self.spk_station_spk = project_dir + 'kernels/spk/stations/stations.bsp'
            self.spk_station = project_dir + 'kernels/spk/stations/'
            self.spk_station_setup = project_dir + 'kernels/spk/station_setup'
            self.fk_station_fk = project_dir + 'kernels/fk/stations/stations.tf'
            self.fk_station = project_dir + 'kernels/fk/stations/'

            # create instrument spk
            self.spk_instrument_spk = project_dir + 'kernels/spk/instrument/instrument.bsp'
            self.spk_instrument = project_dir + 'kernels/spk/instrument/'
            self.spk_instrument_setup = project_dir + 'kernels/spk/instrument_setup'

            # cosmographia (open)
            self.cosmographia = home_dir + 'cosmographia-3.0/'
            self.cosmo_script_earth = project_dir + 'json/script/script_earth.py'
            self.cosmo_script_sensor = project_dir + 'json/script/script_sensor.py'
            self.cosmo_json = project_dir + 'json/load_flp_scenario.json'

            # plots
            self.plots = project_dir + 'plots/'

            # produced CK
            self.TM_produced = project_dir + 'TM/produced_quat/'

            # gmat related pathes (future propagation):
            self.gmat_storage_future = project_dir + 'kernels/spk/storage/'
            self.spk_prop = project_dir + 'kernels/spk/propagation/'
            self.spk_prop_name = project_dir + 'kernels/spk/propagation/spk_gmat_prop_new.bsp'
            self.compare_spk_long_term_TM = project_dir + 'TM/long_term_storage/spk_master_merge_new.bsp'
            self.compare_spk_long_term_TM_gaps = project_dir + 'TM/long_term_storage/spk_gmat_merge_new.bsp'

            # predicted CK
            self.ck_prop = project_dir + 'kernels/ck/propagation/'
            self.ck_prop_furnsh = project_dir + 'kernels/ck/ck_prop_furnsh'
            self.ck_prop_idle_ck = project_dir + 'kernels/ck/propagation/ck_prop_0_idle.bc'
            self.ck_prop_idle_spec = project_dir + 'kernels/ck/ck_prop_idle_spec'
            self.ck_prop_nadir_ck = project_dir + 'kernels/ck/propagation/ck_prop_1_nadir.bc'
            self.ck_prop_nadir_spec = project_dir + 'kernels/ck/ck_prop_nadir_spec'
            self.ck_prop_pass_all_ck = project_dir + 'kernels/ck/propagation/ck_prop_2_pass_all.bc'
            self.ck_prop_pass_all_spec = project_dir + 'kernels/ck/ck_prop_pass_all_spec'
            self.ck_prop_moon_pointing_ck = project_dir + 'kernels/ck/propagation/ck_prop_4_moon_pointing.bc'
            self.ck_prop_moon_pointing_spec = project_dir + 'kernels/ck/ck_prop_moon_pointing_spec'
            self.ck_prop_nadir_offset_ck = project_dir + 'kernels/ck/propagation/ck_prop_5_nadir_offset.bc'
            self.ck_prop_nadir_offset_spec = project_dir + 'kernels/ck/ck_prop_nadir_offset_spec'

            # SPK produced by TLE
            self.spk_source_tle = project_dir + 'kernels/spk/tle_input/FLP.tle'
            self.spk_setup_tle = project_dir + 'kernels/spk/spk_setup_tle'
            self.spk_comment_tle = project_dir + 'kernels/spk/spk_tle_comment'
            self.spk_out_name_tle = project_dir + 'kernels/spk/tle_output/spk_tle.bsp'
            self.spk = project_dir + 'kernels/spk/'


    flp_path = flp_path_class()

    return flp_path


def spk_ck_creation():
    '''
    The TM data is stored in /home/flp/H/h/TM . The routine will load all data stored inside this directory. It will
    automatically detect whether it is a GPS or attitude file. The data need to be provided as a csv file using the
    following structure:

    GPS file:
    header(first line): "time";"timestamp";"AYTPOS00";"AYTPOS01";"AYTPOS02";"AYTVEL00";"AYTVEL01";"AYTVEL02"
    data(all other lines): jjjj-mm-dd HR:MN:SC.###;FLP timestamp;pos x;pos y; pos z;vel x;vel y;vel z
    Attitude file:
    header(first line): "time";"timestamp";"AYTFQU00";"AYTFQU01";"AYTFQU02";"AYTFQU03"
    data(all other lines): jjjj-mm-dd HR:MN:SC.###;FLP timestamp;quat 1;quat 2; quat 3; quat 4

    where ### represents  an integer, FLP timestamp represents a float type
    pos x,y,z, vel x,y,z and quat 1,2,3,4 are float type [m] ; [m/s] ; [-]
    quat style: sin(alpha/2), sin(alpha/2), sin(alpha/2) cos(alpha/2) --> no rotation = 0;0;0;1

    data lines with default values (pos = 0,0,0 or quat = 0,0,0,1) are treated as follows:
    GPS data:
    The line will will not be used but if the option gmat_prop is set to "on" the routine will substitute the gap with
    values on an "educated guess" precision.
    Attitude data:
    The line will not be used at all. You will get a "no spice data" error for this epoch.
    :return: Nothing
    '''

    flp_path = path_setup()

    plots = 'off'  # if set to "on" a plot for each input file will be drawn
    figure_size = [20, 5]  # window size of the plots shown
    timesteps_plot = 10000  # how many data points should be calculated for the plot [not more then seconds of the interval]
    step_spk = 1  # define that only each nth-line will be stored for further computations (kernel generation)
    step_ck = 1  # define that only each nth-line will be stored for further computations (kernel generation)

    poly_degree = 2  # Polynomial order of spk data fitting curve
    max_interval_spk = 200  # time interval between two measured GPS data sets [s]   (200)
    max_interval_ck = 600  # time interval between two measured attitude points [s]  (600)

    gmat_prop = 'on'  # if set to "on" TM GPS data gaps will be propagated using GMAT
    SPK_dif_plot = 'on'  # If set to "on" no overlappting TM data is allowed! Moreover, gmat_prop needs to be set to "on"

    if plots != 'on' and plots != 'off':
        print('Invalid option. Allowed options are: \n\n' +
              'on:        enables plots\n')
    if step_ck == 0 or step_spk == 0:
        print('Invalid option. Allowed options are: \n\n' +
              'all positiv integers (excluding 0)\n')
    else:
        delete_inputs_1(flp_path)
        data_utc_sc_data_all_spk, data_utc_sc_data_all_ck = manage_input_data(plots, figure_size, step_spk, step_ck,
                                                                              poly_degree, max_interval_spk, flp_path)
        if data_utc_sc_data_all_spk != []:
            gmat = create_input_spk(poly_degree, flp_path)
            if gmat_prop == 'on' and gmat == "yes":
                prop_spk_gaps(flp_path)
            plot_validation_spk(data_utc_sc_data_all_spk, gmat_prop, figure_size, timesteps_plot, flp_path, gmat)
            if SPK_dif_plot == 'on':
                plot_spk_dif(data_utc_sc_data_all_spk, figure_size, flp_path, gmat_prop, gmat)
        if data_utc_sc_data_all_ck != []:
            create_input_ck(max_interval_ck, flp_path)
            plot_validation_ck(data_utc_sc_data_all_ck, figure_size, timesteps_plot, flp_path)
        create_mk()


def create_mk():
    '''
    This script samples all files in the directories needed to run the FLP mission scenario. It is divided into 5 sections.
    1. static naif files
    2. static FLP files
    3. semi-static files (they don't belong to the FLP SC but shouldn't be changed. So far there's only ground stations)
    4. dynamic FLP files (SPK, CK - both created for the specific mission scenario)
    5. targets (Points of interests on the Earth surface)
    Notes:
    - the "kernel path" value still printed in the MK is not used but might be used in later releases.
    - observations shown in cosmographia do not belong to kernel files. They are only produced out of these kernel
    files for visualisation purposes.
    :param flp_path: all paths needed
    :return: Nothing
    '''

    flp_path = path_setup()

    file = open(flp_path.mk_out, 'w')

    # write opening and static kernels to mk
    path_values = "( '" + flp_path.mk_sample_path + "' )"
    path_symbols = "( 'KERNELS' )"
    kernels_to_load = "(\n\n" \
                      + "'" + flp_path.lsk_file + "', \n" \
                      + "'" + flp_path.spk_file + "', \n" \
                      + "'" + flp_path.pck_normal + "', \n" \
                      + "'" + flp_path.pck_geo + "', \n" \
                      + "'" + flp_path.pck_itrf + "', " \
                      + "\n\n" \
                      + "'" + flp_path.sclk_file + "', \n" \
                      + "'" + flp_path.fk_file + "', \n" \
                      + "'" + flp_path.ik_file + "'," \
                      + "\n\n" \
                      + "'" + flp_path.fk_station_fk + "', \n" \
                      + "'" + flp_path.spk_station_spk + "'," \
                      + "\n\n"

    text_1 = str(
        "\\begindata\n\n"
        "PATH_VALUES = " + path_values + "\n\n"
        "PATH_SYMBOLS = " + path_symbols + "\n\n"
        "KERNELS_TO_LOAD = " + kernels_to_load
    )
    file.write(text_1)

    # write dynamic kernels to mk
    # NOTE: order of for-loops is very important as this defines the order listed in the mk!!
    for gmat_out in sorted(os.listdir(flp_path.gmat_out)):
        if not gmat_out.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.gmat_out + gmat_out + "'", "\n"]))
    for spk_prop in sorted(os.listdir(flp_path.spk_prop)):
        if not spk_prop.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.spk_prop + spk_prop + "'", "\n"]))
    for spk_out in sorted(os.listdir(flp_path.spk_out)):
        if not spk_out.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.spk_out + spk_out + "'", "\n\n"]))

    for ck_prop in sorted(os.listdir(flp_path.ck_prop)):
        if not ck_prop.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.ck_prop + ck_prop + "'", "\n"]))
    for ck_out in sorted(os.listdir(flp_path.ck_out)):
        if not ck_out.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.ck_out + ck_out + "'", "\n"]))

    file.write("\n")
    for fk_target in sorted(os.listdir(flp_path.fk_target)):
        if not fk_target.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.fk_target + fk_target + "'", "\n"]))
    file.write("\n")
    for spk_target in sorted(os.listdir(flp_path.spk_target)):
        if not spk_target.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.spk_target + spk_target + "'", "\n"]))

    file.write("\n")

    file.close()


def stations():
    '''
    This file creates the spk and fk kernels for all ground stations. To add new ground stations, open the spk100_setup file
    and add a new ground station.
    :param flp_path: all paths needed
    :return: Nothing
    '''

    flp_path = path_setup()

    # Development state- has to be changed...right now every sclk and ck files are deleted before a new one is created
    if os.listdir(flp_path.spk_station) != []:
        remove_spk = 'rm ' + flp_path.spk_station + '*'
        subprocess.call(remove_spk, shell=True)
    if os.listdir(flp_path.fk_station) != []:
        remove_fk = 'rm ' + flp_path.fk_station + '*'
        subprocess.call(remove_fk, shell=True)

    # Run the pinpoint utility
    station_command = 'pinpoint ' + '-def ' + flp_path.spk_station_setup + ' -pck ' + flp_path.pck_normal + \
                     ' -spk ' + flp_path.spk_station_spk + ' -fk ' + flp_path.fk_station_fk
    subprocess.call(station_command, shell=True)


def instrument():
    '''
    This file creates the spk kernels for all instruments. To add new instruments, open the instrument_setup file
    and add a new instrument.
    :param flp_path: all paths needed
    :return: Nothing
    '''

    flp_path = path_setup()

    # Development state- has to be changed...right now every sclk and ck files are deleted before a new one is created
    if os.listdir(flp_path.spk_instrument) != []:
        remove_spk = 'rm ' + flp_path.spk_instrument + '*'
        subprocess.call(remove_spk, shell=True)

    # Run the pinpoint utility
    station_command = 'pinpoint ' + '-def ' + flp_path.spk_instrument_setup + ' -spk ' + flp_path.spk_instrument_spk
    subprocess.call(station_command, shell=True)


def spk_propagation():
    '''
    This script should be used to propagate orbits in a timeframe up to 40 days. To achieve the best prediction currently
    possible, the following steps needs to be done:
    1. Place a csv file wiht at least 10 hours of continuous GPS TM data into the TM directory.
    2. Run the spk_ck_creation script to create a SPK kernel for the corresponding time.
    Note that the gmat_prop parameter should be set to "on" and the SPK_dif_plot should be activated to check that the GPS
    and SPK data have good quality.
    3. Run this script the first time. The following key:value parameter should be used:
        - utc_fix: short after the start time of the input file
        - utc_test: time window almost covering the whole input time window
        - utc_prop: time window larger then time window defined for utc_test
        - max_iter: 1 ; minimal value to speed up the process.
        - error_window: 'off' ; we want to see the whole utc_test period in the plots
        - del_i: is not of interest as it will not be reached before max_iter is reached. (leave it at 50)
        - step: 0.000003 ; this value was found to be a good value for the iteration later on. (so just don't change it)
    During this step we are only interested in the output (plots) to adjust the time window and to validate that the input
    data is valid to be used.
    Plot 1: Both x-positions should be overlaying each other
    Plot 2: Most important plot: I. check if the utc_fix time is at a convenient position (doesnt seem strange...)
                            II. spot a time window (if possible between 30-120 min) with good data sampling (you trust)
                           III. change the utc_test window to the spotted time window; change utc_fix if needed...
    Note: if you see a plot with a "red wave" diverging (frequency roughly 90 min) change the input time slightly (10 sec
    might be enough) and repeat step 3.
    Plot 4: Not of interest but should show two lines roughly with the same shape.
    4. Run this script for a second time. Besides the changes of the utc_test and utc_fix parameter, you should change:
    - max_iter: 1000
    - del_i: 50
    - step: 0.000003
    - utc_prop: use a time which lays around 1 month in the future (the time you need to propagate for mission planning
    reasons)
    Note that these values were found through testing. Changing them could have a huge impact on the propagation result.
    Plot 2: Should now show a plot with lines between [-1, 1]... if the are above those values, do not trust the propagation.
    Plot 4: Both lines should be way more similar to each other as without the iteration process.
    5. Hope, that you predicted right :D
    Note that the result should be a propagation with an error of less than 20 km for a period of 1 month. Some predictions
    achieved values of less then 2 km for all times within this period.
    Note that this script was used for more specific testing as multiple input windows and multiple iterations with
    different step sizes etc. but is reduced to its basic version as more complex versions lead to no significant
    improvements but even less unpredicted behaviour. Please check out the documentation of this script for further details
    in terms of "what to believe in" as an good output propagation.
    Note that a drag area used in the underlying GMAT script of around 0.08 was found to fit most propagations. Do not
    change that value too much even if it seems to be unrealistic.
    Possible error debugging:
    - check if your input file does not has TM gaps
    - check if the input time window includes all times defined in this script
    - only one data file should be supplied to make sure no overlapping data is provided...
    :param flp_path: all paths needed
    :return: Nothing
    '''

    flp_path = path_setup()

    # initial start conditions
    utc_fix = '2018 MAY 08 12:00:00'

    # Get utc time for FLP GPS states to test against (for epsilon calculation)
    utc_test = [
        '2018 MAY 09 08:00:00', '2018 MAY 09 10:00:00',
    ]
    # SPK propagation timeframe:
    utc_prop = [utc_fix, '2018 JUN 10 23:00:00']
    # maximal iterations until abort
    max_iter = [5]
    error_window = 'off'

    # condition to break the for loop before max_iter is reached (steps needed to find a better solution)
    del_i = [30]
    # step size; [km] for position; [m/s] for velocity (so the values are in the same order of magnitude)
    step = [0.000003]
    # drag area of the FLP SC for the JacchiaRObersts atmosphere model. Even though this value seems unrealistic it leads
    # to good propagation results.
    drag_area = 0.078

    input = manage_spk_data_input(flp_path)
    create_gmat_spk(input, utc_fix, utc_test, max_iter, del_i, step, utc_prop, error_window, flp_path, drag_area)


def ck_propagation():
    '''
    This script produces 3 CK propagation kernels. The most basic file is the "idle CK" which points towards the Sun. This
    kernel takes over if no other attitude data is loaded. A second "default" kernel is a nadir pointing kernel which has
    priority compared with the idle CK but not for the other two kernels. The third CK file produces pointing data for every
    IRS flyby (or any other GS (target))above a specific topocentric elevation. This makes sure that the FLP points towards
    the GS... .
    Note that it cannot rotate around a specific orientation (no frequency for a defined rotation around an axes can be
    simulated) --> for this, a routine needs to be implemented to produce the quaternions (as quaternion_computation.py) and
    then transfered to a CK using MSOPCK.
    No slew times are implemented which means that the position transfer is done instantaneously.
    User defined orientations with fixed quaternions, offset to directions, etc. can be implemented in later steps.

    :param flp_path: all paths needed
    :return: Nothing
    '''

    flp_path = path_setup()

    # all times in UTC
    idle = 'on'
    nadir = 'on'
    nadir_times = [
        '2018 JUN 01 12:00:00', '2018 JUN 02 12:00:10',
        '2018 JUN 04 12:00:20', '2018 JUN 05 12:00:30'
    ]

    target_pointing = 'on'
    # every GS or target inside the ck_prop_furnsh metakernel. It will point to it for ALL passes, not a specific one!
    target_p = 'IRS'
    degree = 10  # mininlal elevation of the FLP SC in the topcentric target frame to be considered as pass

    moon_pointing = 'on'
    moon_pointing_times = [
        '2018 JUN 01 17:35:53.027', '2018 JUN 01 17:36:07.636',  # times out of occultation_computation.py script
        '2018 JUN 02 04:26:36.216', '2018 JUN 02 04:26:53.775'
    ]

    nadir_offset = 'on'  # offset in left/right direction NOT forward/backwards direction of nadir mode!
    nadir_offset_values = [
        '2018 MAY 19 08:54:38.259', '2018 MAY 19 08:59:38.259', 55.04589357278617,
        # times out of quaternion_angle.py script
        '2018 MAY 19 10:30:09.372', '2018 MAY 19 10:33:09.372', -48.330314368244814  # angle in degree!!
    ]

    cspice.furnsh(flp_path.lsk_file)

    delete_inputs_2(flp_path)
    create_ck_prop_furnsh(flp_path)
    if idle == 'on':
        create_ck_prop_idle(flp_path)
    if nadir == 'on':
        create_ck_prop_nadir(nadir_times, flp_path)
    if target_pointing == 'on':
        create_ck_prop_pass_all(target_p, degree, flp_path)
    if moon_pointing == 'on':
        create_ck_prop_moon_pointing(moon_pointing_times, flp_path)
    if nadir_offset == 'on':
        create_ck_prop_nadir_offset(nadir_offset_values, flp_path)


def create_observations():
    '''
    This script is designed as an easy interface to load observations into cosmographia. It could be used to check OSIRIS
    pointings to the ground station as well as pictures downlinked. Note that this is the 3D interactive version of the
    FOV_basemap script.
    With this script it is possible to also show limb observations.
    Note tha:
    - the end time cannot be the start time.
    - if the fill paremeter is set to true, it might show not exactly the observered area as observing it a second time
    change it to "unobserverd" again.

    :param flp_path: all paths needed
    :return: Nothing
    '''

    flp_path = path_setup()


    # ---------- user input ------------------
    sensor = 'FLP_PAMCAM'
    start_time = "2018-127T21:54:22"
    end_time = "2018-127T21:54:23"
    obs_rate = 0  # if set to 0 it will be a continuous observation
    fill = False  # fill the area with is observed

    exclude_old_obs = 'on'  # set to on if only the newest observation is of interest for you
    exclude_stations = 'off'  # should the stations be shown?
    exclude_targets = 'off'  # should the targests be shown?

    cosmo_sensor = 'off'  # load a cosmographia view centered in the FLP sensor.
    cosmo_earth = 'on'  # load a cosmographia view looking down to the earth north pole.

    # get the right time format
    cspice.furnsh(flp_path.lsk_file)
    style = 'YYYY-MM-DD HR:MN:SC.###'
    start = cspice.timout(cspice.str2et(start_time), style)
    end = cspice.timout(cspice.str2et(end_time), style)
    cspice.unload(flp_path.lsk_file)
    name_time = start_time.split(".")[0].replace("-", "_").replace(":", "")
    observation_name = sensor + '_' + name_time

    move_new_to_old(flp_path)
    create_json_observation(observation_name, sensor, start, end, obs_rate, fill, flp_path)
    update_scenario_json(exclude_targets, exclude_stations, exclude_old_obs, flp_path)
    if cosmo_sensor == 'on':
        cosmo_sensor_func(sensor, exclude_targets, exclude_stations, start, flp_path)
    if cosmo_earth == 'on':
        cosmo_earth_func(end, flp_path)


def create_targets():
    '''
    Geographic data of targets-------------------

    Put in the geographic coordinates of your target of interest and name it.
    Use the "decimal degrees" coordintates (format) specified on this website as input: (West means negativ longitude values)
    http://dateandtime.info/citycoordinates.php
    Note that there are some restrictions:
    Don't use spaces and other special symbols for the target name. To seperate use "_" e.g. NEW_YORK
    Naming the target in capital letters is good practice.
    It will add your target to all existing targets. If you want to see only your target use the "exclude old targets" parameter.
    If you want to exclude the station in cosmographia as well, use the "exclude station" parameter.

    You cannot use the same name twice. Therefore you have to delete the spk/targets/"name".bsp and fk/targets/"name".tf files before.

    :param flp_path: all paths needed
    :return: Nothing
    '''

    flp_path = path_setup()

    target_name = "HOF"
    target_lat = 50.3129700  # [degree]
    target_lon = 11.9126100  # [degree]
    target_alt = 0.491  # [km]

    exclude_old_targets = 'off'  # 'on'/'off' value. On means only your newly specified target will be shown.
    exclude_station = 'off'  # no stations will be shown in the cosmographia view.

    files = os.listdir(flp_path.spk_target)
    if target_name + '.bsp' not in files:
        move_new_to_old_2(flp_path)
        create_tf_setup(target_name, target_lat, target_lon, target_alt, flp_path)
        create_target_pinpoint(target_name, flp_path)
        create_json_target(target_name, flp_path)
        update_scenario_json_2(exclude_old_targets, exclude_station, flp_path)
        create_mk()
    else:
        print('This target name already exists! Change the name or/and check the existing file!')


def create_input_spk_tle():

    flp_path = path_setup()

    create_tle_setup(flp_path)

    delete_old_tle = 'rm -f ' + flp_path.spk_out_name_tle
    subprocess.call(delete_old_tle, shell=True)

    input_path = os.path.relpath(flp_path.spk_source_tle, flp_path.spk)
    output_path = os.path.relpath(flp_path.spk_out_name_tle, flp_path.spk)
    setup_path = os.path.relpath(flp_path.spk_setup_tle, flp_path.spk)

    os.chdir(flp_path.spk)
    subprocess.call('mkspk -setup ' + setup_path + ' -input ' + input_path + ' -output ' + output_path, shell=True)


    # Brief the file (doublecheck coverage)
    spk_brief = 'brief -t -utc ' + flp_path.lsk_file + ' ' + flp_path.spk_out_name_tle
    subprocess.call(spk_brief, shell=True)





# ----------------------------------------------------------------
# Functions
# ----------------------------------------------------------------

def ck_coverage():
    '''
    This script fills the gap which arises from the simplified CK generation routine. Using only one CK file where all input
    data gets added step by step produces different segments in the CK kernel. These different segments will be shown with
    their coverage individually. This means that there is a need to check every segment to be sure that the coverage gap
    found in segment is a true gap in the overall kernel file. This routine is only needed for debug reasons if "no spice
    data" is displayed in Cosmographia or some other routines fail because of transformation issues. If only one CK input
    file is provided, the ckbrief -dump option could be used to get all the coverage with a terminal command.

    :param flp_path: all paths needed
    :return: Nothing
    '''

    flp_path = path_setup()

    # load lsk, sclk and ck_master file
    cspice.furnsh(flp_path.lsk_file)
    cspice.furnsh(flp_path.sclk_file)
    for item in os.listdir(flp_path.ck_out):
        if not item.endswith(".gitkeep"):
            master_name = item
            cspice.furnsh(flp_path.ck_out + master_name)

    # precalculations
    style = 'YYYY-MM-DD-HR-MN-SC.###'
    cover = cspice.ckcov(flp_path.ck_out + master_name, -513000, False, 'INTERVAL', 0.0, 'TDB')
    time_tmp = np.array([cspice.et2utc(cover[i], 'C', 0) for i in range(len(cover))])

    for i in range(0, len(time_tmp) - 1, 2):
        print(time_tmp[i] + " - " + time_tmp[i + 1])


def cosmographia_earth():
    '''
    This script should be used if Cosmographia should be started with a default Earth view (with a camera viewpoint above
    the north pole. This view is useful to check if all newly added targets are loaded or to launch cosmographia after
    new TM data is used.
    Note that you need to change the visualisation start time manually.

    :param flp_path: all paths needed
    :return: Nothing
    '''

    flp_path = path_setup()

    utc = '2018 MAY 09 08:56:38.259'
    cosmo_earth_func(utc, flp_path)


def cosmographia_sensor():
    '''
    This script should be used if Cosmographia should be started with a default sensor view of the specified sensor.
    This view is useful to improve the feeling about what should be shown in taken pictures. This is useful if you want to
    validated pre-caculated scenarios as "moon rising over Earth".
    With this routine it is possible to show the pointing of an instrument even if it does not have intercepting points
    of all FOV rays with Earth as needed for the FOV_basemap script.
    Note that you need to change the visualisation start time manually. The sensor name needs to be sensor name defined in
    "json/sensor_xxxx". You can define if you want to see only the newest target created, all targets and if the GS
    should be shown.

    :param flp_path: all paths needed
    :return: Nothing
    '''

    flp_path = path_setup()

    utc = '2018 MAY 09 10:32:10.026'
    sensor = 'FLP_MICS'  # make sure you use the real sensor name defined in "json/sensor_xxxx"
    exclude_stations = 'off'
    exclude_old_targets = 'off'

    cspice.furnsh(flp_path.mk_out)

    FOV_angle = get_fov_angle(sensor)
    cosmoscript_sensor(sensor, flp_path)
    update_scenario_json_sensor(exclude_old_targets, exclude_stations, sensor, flp_path)
    launch_cosmographia(utc, FOV_angle, sensor, flp_path)


def FOV_basemap():
    '''
    This routine projects the FOV of a specific time epoch (e.g. for a picture) onto a map. It could be used to roughly
    verify the SPK and CK precision as well as the internal attitude calculation mechanism of the FLP. On the other hand
    it helps to indicate landscapes easier.
    On the other hand it could be used for a longer time period to plot the FOV path of an instrument on the ground. As a
    special example it could be used to check for OSIRIS laser downlink pointing.
    Notes:
    The figsize values are high to produce a high resolution plot saved in the "plots" directory.

    :param flp_path: all paths needed
    :return: Nothing
    '''

    flp_path = path_setup()

    start_time = "2018-127T21:46:46"
    end_time = "2018-127T21:54:22"

    instrument_name = 'FLP_PAMCAM'  # sensor of interest
    n_time_points = 7
    fill = True  # [False, True]
    mid_points = 50
    figsize_1 = 70
    figsize_2 = 70
    revert_limb = 'on'

    # load the FLP kernel
    cspice.furnsh(flp_path.mk_out)
    duration = int(cspice.utc2et(end_time) - cspice.utc2et(start_time))

    main_fov_basemap(start_time, instrument_name,n_time_points,duration,mid_points, revert_limb, figsize_1, figsize_2, end_time, fill, flp_path)


def gf_illumination():
    '''
    This script should be used to determine (photo) opportunities if special requirements for the illumination of a target
    exist. Usage scenarios could be the quantification of illuminiation angles on the shutter times of the cameras.
    The minimum and maximum angles to be specified are:
    The overall angle between source-target-FLP (phase angle)
    The angle between surface normal of target and the source (incident angle)
    The angle between surfache normal of the target and the FLP (emission angle)

    :param flp_path: all paths needed
    :return: Nothing
    '''

    flp_path = path_setup()

    # define time interval and step size
    begin = "2018 JUN 01 20:00:00"
    end = "2018 JUN 02 23:00:00"
    step = 200  # stepsize [s]
    # define point (on Earth surface) -> http://dateandtime.info/citycoordinates.php
    alt = 0.4  # [km]
    lon = 10  # [degrees]
    lat = 50  # [degress]
    # define constrains; use default, if this you do not want to put an additional constraint on it
    phase_angle_min = 0  # default = 0 [degree]
    phase_angle_max = 90  # default = 180 [degree]
    incidence_angle_min = 0  # default = 0 [degree]
    incidence_angle_max = 180  # default = 180 [degree]
    emission_angle_min = 0  # default = 0 [degree]
    emission_angle_max = 40  # default = 180 [degree]

    # "Predefined" parameters for gfsntc and gfilum
    method = "Ellipsoid"
    angtyp = 'PHASE'
    target = 'EARTH'
    illumn = 'SUN'
    frame = 'ITRF93'
    abcorr_ilum = 'LT+S'
    obsrvr = 'FLP'
    adjust = 0
    nintvls = 75000
    cnfine = cspice.utils.support_types.SPICEDOUBLE_CELL(2)

    # load metakernel
    cspice.furnsh(flp_path.mk_out)

    # pre-computations
    et_begin = cspice.utc2et(begin)
    et_end = cspice.utc2et(end)
    ET = [et_begin, et_end]
    cspice.appndd(ET, cnfine)
    # convert planetocentric r/lon/lat to Cartesian vector
    radii = cspice.bodvrd('Earth', 'RADII', 3)[1]
    f = (radii[0] - radii[2]) / radii[0]
    lon_rad = lon * cspice.rpd()
    lat_rad = lat * cspice.rpd()
    spoint = cspice.georec(lon_rad, lat_rad, alt, radii[0], f)  # also input for gfilum!

    # compute gf search:

    # phase angle above  x [degrees]
    refval = phase_angle_min * cspice.rpd()  # now in [rad]
    relate = '>'
    result = cspice.utils.support_types.SPICEDOUBLE_CELL(1500000)
    cspice.gfilum(method, angtyp, target, illumn, frame, abcorr_ilum, obsrvr, spoint, relate, refval, adjust, step,
                  nintvls, cnfine, result)
    count = cspice.wncard(result)
    print(count)

    # phase angle below  x [degrees]
    refval = phase_angle_max * cspice.rpd()  # now in [rad]
    relate = '<'
    result_2 = cspice.utils.support_types.SPICEDOUBLE_CELL(1500000)
    cspice.gfilum(method, angtyp, target, illumn, frame, abcorr_ilum, obsrvr, spoint, relate, refval, adjust, step,
                  nintvls, result, result_2)
    count = cspice.wncard(result_2)
    print(count)

    # incidence angle above  x [degrees]
    refval = incidence_angle_min * cspice.rpd()  # now in [rad]
    relate = '>'
    result_3 = cspice.utils.support_types.SPICEDOUBLE_CELL(1500000)
    cspice.gfilum(method, angtyp, target, illumn, frame, abcorr_ilum, obsrvr, spoint, relate, refval, adjust, step,
                  nintvls, result_2, result_3)
    count = cspice.wncard(result_3)
    print(count)

    # incidence angle below x [degrees]
    refval = incidence_angle_max * cspice.rpd()  # now in [rad]
    relate = '<'
    result_4 = cspice.utils.support_types.SPICEDOUBLE_CELL(1500000)
    cspice.gfilum(method, angtyp, target, illumn, frame, abcorr_ilum, obsrvr, spoint, relate, refval, adjust, step,
                  nintvls, result_3, result_4)
    count = cspice.wncard(result_4)
    print(count)

    # emission angle above  x [degrees]
    refval = emission_angle_min * cspice.rpd()  # now in [rad]
    relate = '>'
    result_5 = cspice.utils.support_types.SPICEDOUBLE_CELL(1500000)
    cspice.gfilum(method, angtyp, target, illumn, frame, abcorr_ilum, obsrvr, spoint, relate, refval, adjust, step,
                  nintvls, result_4, result_5)
    count = cspice.wncard(result_5)
    print(count)

    # emission angle below x [degrees]
    refval = emission_angle_max * cspice.rpd()  # now in [rad]
    relate = '<'
    result_6 = cspice.utils.support_types.SPICEDOUBLE_CELL(1500000)
    cspice.gfilum(method, angtyp, target, illumn, frame, abcorr_ilum, obsrvr, spoint, relate, refval, adjust, step,
                  nintvls, result_5, result_6)
    count = cspice.wncard(result_6)
    print(count)

    # print results
    if (count == 0):
        print("Result window is empty.\n\n")
    else:
        for i in range(count):
            # Fetch the endpoints of the Ith interval of the result window.
            left, right = cspice.wnfetd(result_6, i)

            if (left == right):
                time = cspice.et2utc(left, 'C', 3)
                print("Event time: " + time + "\n")
            else:
                time_left = cspice.et2utc(left, 'C', 3)
                time_right = cspice.et2utc(right, 'C', 3)
                print("Interval " + str(i + 1) + "\n")
                print("From : " + time_left + " \n")
                print("To   : " + time_right + " \n")
                print(" \n")


def gf_target_in_FOV():
    '''
    This routine is built around a specific need: Check whether the a target is in the instrument FOV.
    This was done for the FLP_OSIRIS instrument points with its circular FOV and the GS1 as target. This way it could be
    determined if a downlink to the station could be established referring geometric need and according to the CKs and SPKs
    generated before which do have some implementation errors and the overall noise of the GPS data... .
    :param flp_path: all paths needed
    :return: Nothing
    '''

    flp_path = path_setup()

    cspice.furnsh(flp_path.mk_out)

    # define input data
    begin = "2018 MAY 08 10:00:00"
    end = "2018 MAY 08 23:00:00"

    inst = 'FLP_OSIRIS'
    target = 'GS1'
    tshape = 'POINT'
    tframe = ""  # should be ignored and left blank for tshape = POINT
    abcorr = 'NONE'
    obsrvr = 'FLP'

    step = 1  # stepsize [sec]

    result = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
    cnfine = cspice.utils.support_types.SPICEDOUBLE_CELL(2)

    # pre-computations
    et_begin = cspice.utc2et(begin)
    et_end = cspice.utc2et(end)
    ET = [et_begin, et_end]
    cspice.appndd(ET, cnfine)

    # compute gf search 1
    result = cspice.gftfov(inst, target, tshape, tframe, abcorr, obsrvr, step, cnfine)

    # print results
    count = cspice.wncard(result)
    print(count)

    if count == 0:
        print("Result window is empty.\n\n")
    else:
        for i in range(count):
            # Fetch the endpoints of the Ith interval of the result window.
            start, stop = cspice.wnfetd(result, i)

            if (start == stop):
                time = cspice.et2utc(start, 'C', 3)
                print("Event time: " + time + "\n")
            else:
                time_left = cspice.et2utc(start, 'C', 3)
                time_right = cspice.et2utc(stop, 'C', 3)
                print("Interval " + str(i + 1) + "\n")
                print("From : " + time_left + " \n")
                print("To   : " + time_right + " \n")
                print(" \n")


def minimal_distances():
    '''
    This script generates the times of local minima distance between a target and the FLP SC. This might be useful as this
    will be the times with the highest resultion achievable with the onboard cameras. Note that this is an easy routine
    which also serves as geometry-finder implementation demonstration.

    :param flp_path: all paths needed
    :return: Nothing
    '''

    flp_path = path_setup()

    cspice.furnsh(flp_path.mk_out)

    # define input data
    begin = "2018 MAY 08 01:00:00"
    end = "2018 MAY 08 23:00:00"

    target = 'MILAN'
    abcorr = 'NONE'
    obsrvr = 'FLP'
    relate = '<'

    refval = 3000  # seems to be a useful value
    adjust = 0
    step = 20 * 60  # stepsize [min]
    nintvls = 7500

    result = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
    cnfine = cspice.utils.support_types.SPICEDOUBLE_CELL(2)

    # pre-computations
    et_begin = cspice.utc2et(begin)
    et_end = cspice.utc2et(end)
    ET = [et_begin, et_end]
    cspice.appndd(ET, cnfine)

    # compute gf search 1
    cspice.gfdist(target, abcorr, obsrvr, relate, refval, adjust, step, nintvls, cnfine, result)
    # compute gf search 2
    relate_2 = 'LOCMIN'
    result_2 = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
    cspice.gfdist(target, abcorr, obsrvr, relate_2, refval, adjust, step, nintvls, result, result_2)

    # print results
    count = cspice.wncard(result_2)
    print(count)

    if count == 0:
        print("Result window is empty.\n\n")
    else:
        for i in range(count):
            # Fetch the endpoints of the Ith interval of the result window.
            start, stop = cspice.wnfetd(result_2, i)

            if (start == stop):
                time = cspice.et2utc(start, 'C', 3)
                print("Event time: " + time + "\n")
            else:
                time_left = cspice.et2utc(start, 'C', 3)
                time_right = cspice.et2utc(stop, 'C', 3)
                print("Interval " + str(i + 1) + "\n")
                print("From : " + time_left + " \n")
                print("To   : " + time_right + " \n")
                print(" \n")


def occultation_computation():
    '''
    -------------- Mission planning -------------
    This script is an example program on how to costumize constellation searches for specific geometry events. This script
    focus' on a scenario "moon rise behind earth". In order to make use out of this program, the following steps have to
    be performed:
    1. Load SPK kernel into TM directory for the time of interest.
    2. Run the spk_ck_creation program.
    3. Execute this program. (Copy output times if needed)
    4. Copy the produced kernels "TM/produced_quat" to the "TM" directory (this is manually on purpose!)
    5. Run the spk_ck_program again.
    6. Run the cosmographia_sensor program (with on of the output times from step 3)
    7. Go a little bit forwards, backwards in time [max. 1 hour] to see if the scenario of interest really appears.

    :param flp_path:
    :return:
    '''

    flp_path = path_setup()

    cspice.furnsh(flp_path.mk_out)

    # define input data
    begin = "2018 MAY 08 02:00:00.00"
    end = "2018 MAY 09 20:00:00.00"
    inst = 'FLP_MICS'
    target = 'MOON'
    phase_angle_max = 40
    plot = 'off'

    # parameter for gfilum search (done first , to hopefully speed up the search)
    method = "Ellipsoid"
    angtyp = 'PHASE'
    illumn = 'SUN'
    frame = 'IAU_MOON'
    adjust = 0
    nintvls = 75000
    abcorr = 'NONE'
    obsrvr = 'FLP'
    step = 1000
    result_0 = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
    cnfine_0 = cspice.utils.support_types.SPICEDOUBLE_CELL(2)
    # convert planetocentric r/lon/lat to Cartesian vector
    # define point (on Moon surface)
    alt = 0  # [km]
    lon = 0  # [degrees]
    lat = 90  # [degress]
    radii = cspice.bodvrd('Moon', 'RADII', 3)[1]
    f = (radii[0] - radii[2]) / radii[0]
    lon_rad = lon * cspice.rpd()
    lat_rad = lat * cspice.rpd()
    spoint = cspice.georec(lon_rad, lat_rad, alt, radii[0], f)  # also input for gfilum!

    # parameter for gfoclt search
    occtyp = "PARTIAL"
    front = "EARTH"
    fshape = "ELLIPSOID"
    fframe = "IAU_EARTH"
    back = "MOON"
    bshape = "ELLIPSOID"
    bframe = "IAU_MOON"
    step_1 = 100  # stepsize [sec]
    result_1 = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
    cnfine_1 = cspice.utils.support_types.SPICEDOUBLE_CELL(2)

    # pre-computations
    et_begin = cspice.utc2et(begin)
    et_end = cspice.utc2et(end)
    ET = [et_begin, et_end]
    cspice.appndd(ET, cnfine_0)

    # compute gf search 0 (pahse angles to see some parts of the moon)
    # phase angle below  x [degrees]
    refval = phase_angle_max * cspice.rpd()  # now in [rad]
    relate = '<'
    cspice.gfilum(method, angtyp, target, illumn, frame, abcorr, obsrvr, spoint, relate, refval, adjust, step, nintvls,
                  cnfine_0, result_0)
    count_0 = cspice.wncard(result_0)
    print(count_0)

    # compute gf search 1 (full occultations)
    cspice.gfoclt(occtyp, front, fshape, fframe, back, bshape, bframe, abcorr, obsrvr, step_1, result_0, result_1)
    # print results
    count = cspice.wncard(result_1)
    print(count)

    instrument_ID = str(cspice.bodn2c(inst))  # ID code for instrument name
    frame_instrument_pool = 'INS' + instrument_ID + '_FOV_FRAME'
    frame_instrument = cspice.gcpool(frame_instrument_pool, 0, 1)[0]  # frame name of instrument
    boresight_pool = 'INS' + instrument_ID + '_BORESIGHT'
    boresight = cspice.gdpool(boresight_pool, 0, 3)  # boresight vector of sensor

    if count == 0:
        print("Result window is empty.\n\n")
    else:
        for i in range(count):
            # Fetch the endpoints of the Ith interval of the result window.
            start, stop = cspice.wnfetd(result_1, i)

            ET = np.linspace(start - 3600, stop + 3600, (stop + 3600) - (
                        start - 3600))  # the 3600 is included to have CK coverage 1 hour before and after the event

            # vector to target
            vec = np.array([cspice.spkpos(target, ET[i], 'J2000', abcorr, obsrvr)[0] for i in range(len(ET))])

            q = []
            a = []
            b = []
            c = []
            # the boresight_j2000 vector equals the boresight vector in the instrument frame as we define that the instrument frame
            # is orientated as the j2000 frame. This means it needs to rotate about the output quaternion to look at the specified
            # target. But this rotation needs to be "implemented". This is a difference compared with the already existing attitude
            # data where the SC was already rotated!
            for k in range(len(vec)):
                # normal vector of cross product
                H = cspice.ucrss(boresight, vec[k])
                # rotation angle   # vnorm boresight_j2000 = 1; -> not calculated.
                I = cspice.vdot(vec[k], boresight) / (cspice.vnorm(vec[k]))
                J = np.arccos(I)
                K = cspice.axisar(H, J)
                q.append(cspice.m2q(K)[0])  # store the quaternion entries seperated for easier plot
                a.append(cspice.m2q(K)[1])
                b.append(cspice.m2q(K)[2])
                c.append(cspice.m2q(K)[3])

            if plot == 'on':
                # plot the calculated data
                style = 'YYYY-MM-DD-HR-MN-SC.###'
                timeline = []
                for item in ET:
                    time = cspice.timout(item, style, 23)
                    sub = time.replace(".", "-").split("-")
                    timeline.append(datetime.datetime(int(sub[0]), int(sub[1]), int(sub[2]),
                                                      int(sub[3]), int(sub[4]), int(sub[5]), int(sub[6]), tzinfo=None))

                fig, ax = plt.subplots(figsize=(15, 7))
                ax.set_ylabel('Rotation [-]', fontsize=18)
                plt.plot(timeline, q, label='quat q', color='green')
                plt.plot(timeline, a, label='quat a', color='brown')
                plt.plot(timeline, b, label='quat b', color='blue')
                plt.plot(timeline, c, label='quat c', color='red')
                plt.gcf().autofmt_xdate()
                ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=4)
                for tick in ax.xaxis.get_major_ticks():
                    tick.label.set_fontsize(15)
                for tick in ax.yaxis.get_major_ticks():
                    tick.label.set_fontsize(15)
                plt.grid()
                # fig.savefig(flp_path.plots + 'validation_spk_magnitude_' + time_start + '_' + time_end + '.png', dpi=fig.dpi)
                plt.show()

            # write values as TM input file
            style_2 = 'YYYY-MM-DD HR-MN-SC.###'
            name = 'TM_test_output_' + str(i) + '.csv'
            file = open(flp_path.TM_produced + name, 'w')
            text = str(
                '"time";"timestamp";"AYTFQU00";"AYTFQU01";"AYTFQU02";"AYTFQU03"' + '\n'
            )
            file.write(text)
            for k in range(len(ET)):
                file.write(';'.join(
                    [cspice.timout(ET[k], style_2, 23), 'sclk_clock', str(a[k]), str(b[k]), str(c[k]),
                     str(q[k]) + '\n']))
            file.close()

            if (start == stop):
                time = cspice.et2utc(start, 'C', 3)
                print("Event time: " + time + "\n")
            else:
                time_left = cspice.et2utc(start, 'C', 3)
                time_right = cspice.et2utc(stop, 'C', 3)
                print("Interval " + str(i + 1) + "\n")
                print("From : " + time_left + " \n")
                print("To   : " + time_right + " \n")
                print(" \n")


def plot_angle_offset_target():
    '''
    This function plots the angle between the specified instrument boresight vector and the FLP SC->target vector
    plotted over a specified time frame. The time frame is split into shorter time windows fulfilling the conditions
    defined. (Elevation over ground)

    :param flp_path:
    :return:
    '''

    flp_path = path_setup()


    cspice.furnsh(flp_path.mk_out)

    instrument = 'FLP_OSIRIS'
    obsrvr = 'GS1'  # it is called observer because of the gf routine, think of it as "target" [valid: targets or GS generated with pinpoint]
    begin = "2018 MAY 08 12:18:30"
    end = "2018 MAY 08 22:18:30"
    n_point = 2000
    abcorr = 'NONE'
    refval = 3 * cspice.rpd()  # elevation [degree]
    time_offset = 140  # time offset before and after the pass condition is met [s]

    # precomputations to get all parameters
    et_begin = cspice.utc2et(begin)
    et_end = cspice.utc2et(end)
    et = [et_begin, et_end]
    instrument_ID = str(cspice.bodn2c(instrument))  # ID code for sensor name
    frame_instrument_pool = 'INS' + instrument_ID + '_FOV_FRAME'
    frame_instrument = cspice.gcpool(frame_instrument_pool, 0, 1)[0]  # frame name of sensor
    boresight_pool = 'INS' + instrument_ID + '_BORESIGHT'
    boresight = cspice.gdpool(boresight_pool, 0, 3)  # boresight vector of sensor
    target_ID = str(cspice.bodn2c(obsrvr))  # ID code for target (or observer?)
    frame_obsrvr_pool = 'FRAME_1' + target_ID + '_NAME'  # this will work only for pinpoint generated tf files.
    frame_obsrvr = cspice.gcpool(frame_obsrvr_pool, 0, 1)[0]  # frame name of target
    crdsys = 'LATITUDINAL'
    coord = 'LATITUDE'
    relate = '>'
    adjust = 0
    step = 20 * 60  # stepsize [min]
    nintvls = 7500
    result = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
    cnfine = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
    cspice.appndd(et, cnfine)

    # compute gf search
    # elevation above x [degrees]
    cspice.gfposc('FLP', frame_obsrvr, abcorr, obsrvr, crdsys, coord, relate, refval, adjust, step, nintvls, cnfine,
                  result)
    count = cspice.wncard(result)
    print("Number of result windows: " + str(count))

    if (count == 0):
        print("Result window is empty.\n\n")
    else:

        # actual angle_offset_target calculations:
        for i in range(count):

            # get pass time
            et_start, et_stop = cspice.wnfetd(result, i)
            ET = np.linspace(et_start - time_offset, et_stop + time_offset, n_point)

            # FLP SC -> target vector
            vec = np.array([cspice.spkpos('FLP', ET[i], frame_instrument, abcorr, obsrvr)[0] for i in range(len(ET))])

            # angle in degree derived from dot product; the boresight norm is already and therefore does not need to be calculated
            # the minus sign is included to change the vector orientation ...
            angle = np.array(
                [cspice.dpr() * np.arccos(cspice.vdot(-vec[i, :], boresight) / cspice.vnorm(vec[i, :])) for i in
                 range(len(ET))])

            # get nice timeline
            style = 'YYYY-MM-DD-HR-MN-SC.###'
            timeline = []
            for i in range(len(ET)):
                time = cspice.timout(ET[i], style, 23)
                sub = time.replace(".", "-").split("-")
                timeline.append(
                    datetime.datetime(int(sub[0]), int(sub[1]), int(sub[2]), int(sub[3]), int(sub[4]), int(sub[5]),
                                      int(sub[6]), tzinfo=None))

            # plot the data
            fig, ax = plt.subplots(figsize=(15, 8))
            plt.plot(timeline, angle, label='angle [degrees]')
            plt.gcf().autofmt_xdate()
            ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=1)
            for tick in ax.xaxis.get_major_ticks():
                tick.label.set_fontsize(15)
            for tick in ax.yaxis.get_major_ticks():
                tick.label.set_fontsize(15)
            plt.grid()
            plt.title(
                str(obsrvr) + " " + str(cspice.et2utc(et_start, 'C', 0)) + " - " + str(cspice.et2utc(et_stop, 'C', 0)),
                fontsize=18)
            plot_name = cspice.et2utc(et_start, 'ISOC', 0).replace("-", "_").replace(":", "")
            fig.subplots_adjust(top=0.95)  # adjust the shown plot
            fig.subplots_adjust(bottom=0.2)
            fig.subplots_adjust(left=0.08)
            fig.savefig(flp_path.plots + str(obsrvr) + '_angular_offset_' + plot_name + '.png', dpi=fig.dpi)
            plt.show()


def plot_pass():
    '''
    This function plots the angle between the specified instrument boresight vector and the FLP SC->target vector
    plotted over a specified time frame. The time frame is split into shorter time windows fulfilling the conditions
    defined. (Elevation over ground)
    The times the conditions for the paths are met will be printed to the output.

    :param flp_path:
    :return:
    '''

    flp_path = path_setup()

    cspice.furnsh(flp_path.mk_out)

    obsrvr = 'AALEN'  # it is called observer because of the gf routine, think of it as "target" [valid: targets or GS generated with pinpoint]
    begin = "2018 MAY 09 01:00:00.000"
    end = "2018 JUN 01 01:00:00.000"
    abcorr = 'NONE'
    refval = 2 * cspice.rpd()  # elevation [degree]
    time_offset = 0  # time offset before and after the pass condition is met [s]
    plot = 'off'

    # precomputations to get all parameters
    et_begin = cspice.utc2et(begin)
    et_end = cspice.utc2et(end)
    et = [et_begin, et_end]
    target_ID = str(cspice.bodn2c(obsrvr))  # ID code for target
    frame_obsrvr_pool = 'FRAME_1' + target_ID + '_NAME'  # this will work only for pinpoint generated tf files.
    frame_obsrvr = cspice.gcpool(frame_obsrvr_pool, 0, 1)[0]  # frame name of target
    crdsys = 'LATITUDINAL'
    coord = 'LATITUDE'
    relate = '>'
    adjust = 0
    step = 20 * 60  # stepsize [min]
    nintvls = 7500
    result = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
    cnfine = cspice.utils.support_types.SPICEDOUBLE_CELL(2)
    cspice.appndd(et, cnfine)

    # compute gf search
    # elevation above x [degrees]
    cspice.gfposc('FLP', frame_obsrvr, abcorr, obsrvr, crdsys, coord, relate, refval, adjust, step, nintvls, cnfine,
                  result)
    count = cspice.wncard(result)
    print("Number of result windows: " + str(count))
    # max. elevation time
    relate_2 = 'LOCMAX'
    result_2 = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
    cspice.gfposc('FLP', frame_obsrvr, abcorr, obsrvr, crdsys, coord, relate_2, refval, adjust, step, nintvls, result,
                  result_2)

    if (count == 0):
        print("Result window is empty.\n\n")
    else:

        # actual plot_pass calculations:
        for k in range(count):

            # get maximal elevation for output
            max_el_l, max_el_r = cspice.wnfetd(result_2, k)

            # get pass time
            et_start, et_stop = cspice.wnfetd(result, k)
            ET = np.linspace(et_start - time_offset, et_stop + time_offset,
                             (et_stop + time_offset) - (et_start - time_offset))

            # target-> FLP SC vector
            vec = np.array([cspice.spkpos('FLP', ET[i], frame_obsrvr, abcorr, obsrvr)[0] for i in range(len(ET))])

            # convert from rectangular coordinates to spherical coordinates
            r = np.array([cspice.recsph(-vec[i, :])[0] for i in range(len(ET))])  # km
            # Angle between the point and the positive z-axis [degrees] ; the z axis is the zenit direction of the topocentric frame.
            colat = np.array([cspice.dpr() * cspice.recsph(-vec[i, :])[1] for i in range(len(ET))])
            # angle between the positive X-axis and the orthogonal projection of the point onto the XY plane. [degrees]
            # the x axis is pointing north [topocentric frame].
            lon = np.array([cspice.dpr() * cspice.recsph(-vec[i, :])[2] for i in range(len(ET))])

            if plot == 'on':
                # get nice timeline
                style = 'YYYY-MM-DD-HR-MN-SC.###'
                timeline = []
                for i in range(len(ET)):
                    time = cspice.timout(ET[i], style, 23)
                    sub = time.replace(".", "-").split("-")
                    timeline.append(
                        datetime.datetime(int(sub[0]), int(sub[1]), int(sub[2]), int(sub[3]), int(sub[4]), int(sub[5]),
                                          int(sub[6]), tzinfo=None))

                # plot the data
                fig, ax1 = plt.subplots(figsize=(15, 8))

                ax1.set_ylabel('distance [km]', fontsize=18)
                line_1 = ax1.plot(timeline, r, label='distance', linestyle=':', color='red', marker='x')
                ax2 = ax1.twinx()
                ax2.set_ylabel('angle [degree]', fontsize=18)
                line_2 = ax2.plot(timeline, colat, label='colat', color='blue', marker='.')
                line_3 = ax2.plot(timeline, lon, label='lon', color='green', marker='o')
                ax2.set_ylim(-190, 190)

                fig.tight_layout()
                plt.gcf().autofmt_xdate()
                lines = line_1 + line_2 + line_3
                labels = [item.get_label() for item in lines]
                plt.legend(lines, labels, fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=3)
                for tick in ax1.xaxis.get_major_ticks():
                    tick.label.set_fontsize(15)
                for tick in ax1.yaxis.get_major_ticks():
                    tick.label.set_fontsize(15)
                for tick in ax2.yaxis.get_major_ticks():
                    tick.label.set_fontsize(15)
                plt.grid()
                plt.title(str(obsrvr) + " " + str(cspice.et2utc(et_start, 'C', 0)) + " - " + str(
                    cspice.et2utc(et_stop, 'C', 0)), fontsize=18)
                plot_name = cspice.et2utc(et_start, 'ISOC', 0).replace("-", "_").replace(":", "")
                fig.subplots_adjust(top=0.95)  # adjust the shown plot
                fig.subplots_adjust(bottom=0.2)
                fig.subplots_adjust(left=0.08)
                fig.savefig(flp_path.plots + str(obsrvr) + '_pass_' + plot_name + '.png', dpi=fig.dpi)
                plt.show()

            # Print characteristic output values
            # Fetch the endpoints of the Ith interval of the result window.
            if (max_el_l == max_el_r):
                max_el = max_el_l

                if (et_start == et_stop):
                    time = cspice.et2utc(et_start, 'C', 3)
                    print("Event time: " + time + "\n")
                else:
                    time_left = cspice.et2utc(et_start, 'C', 3)
                    time_right = cspice.et2utc(et_stop, 'C', 3)
                    time_max = cspice.et2utc(max_el, 'C', 3)
                    print("Interval " + str(k + 1) + "\n")
                    print("From : " + time_left + " \n")
                    print("To   : " + time_right + " \n")
                    print("Max  : " + time_max + " \n")
                    print(" \n")


def quaternion_angle():
    '''
    This script computes the minimal angle (appearing at the highest elevation of the SC in the topocentric target frame)
    between the SC subobserver point and a target. This routine is not validated yet.

    :return: Nothing
    '''

    flp_path = path_setup()

    # load metakernel
    cspice.furnsh(flp_path.mk_out)

    # define input data
    begin = "2018 MAY 10 12:21:30"
    end = "2018 MAY 20 12:26:00"

    target = 'IRS'
    abcorr = 'NONE'
    obsrvr = 'FLP'
    relate = '<'

    refval = 3000  # seems to be a useful value
    adjust = 0
    step = 20 * 60  # stepsize [min]
    nintvls = 7500

    result = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
    cnfine = cspice.utils.support_types.SPICEDOUBLE_CELL(2)

    # pre-computations
    et_begin = cspice.utc2et(begin)
    et_end = cspice.utc2et(end)
    ET = [et_begin, et_end]
    cspice.appndd(ET, cnfine)

    # compute gf search 1
    cspice.gfdist(target, abcorr, obsrvr, relate, refval, adjust, step, nintvls, cnfine, result)
    # compute gf search 2
    relate_2 = 'LOCMIN'
    result_2 = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
    cspice.gfdist(target, abcorr, obsrvr, relate_2, refval, adjust, step, nintvls, result, result_2)

    # print results and store the time of interest (ET)
    count_2 = cspice.wncard(result_2)
    print(count_2)

    ET = []
    time_max = []
    for i in range(count_2):
        time_et_1, time_et_2 = cspice.wnfetd(result_2, i)
        if time_et_1 == time_et_2:  # this should always be the case
            time_max.append(cspice.et2utc(time_et_1, 'C', 3))
            ET.append(time_et_1)

    # calculate the FLP-(subobserver)surface vectors
    flp_spoint = np.array([cspice.subpnt("INTERCEPT/ELLIPSOID", "EARTH", ET[i], "ITRF93", "NONE", "FLP")[2]
                           for i in range(len(ET))])
    # calculate the FLP-target vectors
    flp_target = np.array([cspice.spkpos('-513', ET[i], 'ITRF93', 'NONE', target)[0] for i in range(len(ET))])
    # calculate the target-(subobserver)surface
    spoint = np.array([cspice.subpnt("INTERCEPT/ELLIPSOID", "EARTH", ET[i], "ITRF93", "NONE", "FLP")[0]
                       for i in range(len(ET))])
    target = np.array([cspice.spkpos(target, ET[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET))])
    target_spoint = target - spoint

    rot_matrix = np.array([cspice.pxform("ITRF93", "FLP_ANGLE_SIGN", ET[i]) for i in range(len(ET))])
    target_spoint_flp_angle_sign = np.array([cspice.mxv(rot_matrix[i], target_spoint[i]) for i in range(len(ET))])

    flp_spoint_distance = []
    flp_target_distance = []
    target_spoint_distance = []
    for i in range(len(ET)):
        flp_spoint_distance.append(
            np.sqrt(np.square(flp_spoint[i, 0]) + np.square(flp_spoint[i, 1]) + np.square(flp_spoint[i, 2])))
        flp_target_distance.append(
            np.sqrt(np.square(flp_target[i, 0]) + np.square(flp_target[i, 1]) + np.square(flp_target[i, 2])))
        target_spoint_distance.append(
            np.sqrt(np.square(target_spoint[i, 0]) + np.square(target_spoint[i, 1]) + np.square(target_spoint[i, 2])))

    angle_rad = []
    for i in range(len(ET)):
        angle_rad.append(
            np.arccos((np.square(flp_target_distance[i]) +
                       np.square(flp_spoint_distance[i]) -
                       np.square(target_spoint_distance[i])) /
                      (2 * flp_target_distance[i] * flp_spoint_distance[i])))

    angle = []
    for i in range(len(ET)):
        if target_spoint_flp_angle_sign[i, 1] > 0:
            angle.append(
                (cspice.dpr() * angle_rad[i]) * (-1))  # here we have to revert the sign for the angle! to point right
        else:
            angle.append(cspice.dpr() * angle_rad[i])  # this is automatically pointing right

    quaternion = []
    for i in range(len(ET)):
        quaternion.append([np.sin(angle_rad[i] / 2), 0, 0, np.cos(angle_rad[i] / 2)])

    for i in range(len(time_max)):
        print("Minimum distance at: " + time_max[i] + '\n'
                                                      "Minimum resulting angle [degree]: " + str(angle[i]) + '\n'
              # "Resulting quaternion [q, a, b, c]: " + str(quaternion[i]) + '\n' +  # NOT CHECKED YET
              )


def quaternion_computation():
    '''
    This script computes, plots and stores quaternions. These quaternions represent rotations needed to aim a specific
    ephemeris object or target on Earth. To validate the output of this routine in cosmographia make sure you do not load
    any other TM quaternion files as the newly produced ones, labeled: TM_test_output . Otherwise the planned data could be
    overwritten by other data (especially if you calculate these quaternions for the past). This routine is meant to serve
    as a Mission planning script. If further constraints e.g. constellations have to be met one or many geometry finder
    routines will be executed beforehand as shown in the occultation_computation script.

    :return:
    '''

    flp_path = path_setup()

    cspice.furnsh(flp_path.mk_out)

    start = '2018 MAY 19 01:34:55.426'
    end = '2018 MAY 19 01:37:03.640'
    instrument = 'FLP_MICS'
    target = 'IRS'

    # precalculations
    et_start = cspice.str2et(start)
    et_end = cspice.str2et(end)
    ET = np.linspace(et_start, et_end, et_end - et_start)
    instrument_ID = str(cspice.bodn2c(instrument))  # ID code for instrument name
    frame_instrument_pool = 'INS' + instrument_ID + '_FOV_FRAME'
    frame_instrument = cspice.gcpool(frame_instrument_pool, 0, 1)[0]  # frame name of instrument
    boresight_pool = 'INS' + instrument_ID + '_BORESIGHT'
    boresight = cspice.gdpool(boresight_pool, 0, 3)  # boresight vector of sensor
    abcorr = 'NONE'
    obsrvr = 'FLP'

    # vector to target
    vec = np.array([cspice.spkpos(target, ET[i], 'J2000', abcorr, obsrvr)[0] for i in range(len(ET))])

    q = []
    a = []
    b = []
    c = []
    # the boresight_j2000 vector equals the boresight vector in the instrument frame as we define that the instrument frame
    # is orientated as the j2000 frame. This means it needs to rotate about the output quaternion to look at the specified
    # target. But this rotation needs to be "implemented". This is a difference compared with the already existing attitude
    # data where the SC was already rotated!
    for i in range(len(vec)):
        # normal vector of cross product
        H = cspice.ucrss(boresight, vec[i])
        # rotation angle   # vnorm boresight_j2000 = 1; -> not calculated.
        I = cspice.vdot(vec[i], boresight) / (cspice.vnorm(vec[i]))
        J = np.arccos(I)
        K = cspice.axisar(H, J)
        q.append(cspice.m2q(K)[0])  # store the quaternion entries seperated for easier plot
        a.append(cspice.m2q(K)[1])
        b.append(cspice.m2q(K)[2])
        c.append(cspice.m2q(K)[3])

    # plot the calculated data
    style = 'YYYY-MM-DD-HR-MN-SC.###'
    timeline = []
    for item in ET:
        time = cspice.timout(item, style, 23)
        sub = time.replace(".", "-").split("-")
        timeline.append(datetime.datetime(int(sub[0]), int(sub[1]), int(sub[2]),
                                          int(sub[3]), int(sub[4]), int(sub[5]), int(sub[6]), tzinfo=None))

    fig, ax = plt.subplots(figsize=(15, 7))
    ax.set_ylabel('Rotation [-]', fontsize=18)
    plt.plot(timeline, q, label='quat q', color='green')
    plt.plot(timeline, a, label='quat a', color='brown')
    plt.plot(timeline, b, label='quat b', color='blue')
    plt.plot(timeline, c, label='quat c', color='red')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=4)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.savefig(flp_path.plots + 'quaternion_computation.png', dpi=fig.dpi)
    plt.show()

    # write values as TM input file
    style_2 = 'YYYY-MM-DD HR-MN-SC.###'
    name = 'TM_test_output.csv'
    file = open(flp_path.TM_produced + name, 'w')
    text = str(
        '"time";"timestamp";"AYTFQU00";"AYTFQU01";"AYTFQU02";"AYTFQU03"' + '\n'
    )
    file.write(text)
    for i in range(len(ET)):
        file.write(';'.join(
            [cspice.timout(ET[i], style_2, 23), 'sclk_clock', str(a[i]), str(b[i]), str(c[i]), str(q[i]) + '\n']))
    file.close()


# ----------------------------------------------------------------
# ----------------------------------------------------------------
# ----------------------------------------------------------------




def manage_input_data(plots, figure_size, step_spk, step_ck, poly_degree, max_interval_spk, flp_path):

    # Get all TM input files
    print('Searching for input files ...')
    files_tmp = []
    for file in os.listdir(flp_path.TM_path):
        if file.endswith(".csv"):
            files_tmp.append(file)
    marker = '~'
    files = []
    for i in range(len(files_tmp)):
        if marker not in files_tmp[i]:
            files.append(files_tmp[i])
    print('Amount of input files detected: ' + str(len(files)))

    # Create new spk and ck source files
    cspice.furnsh(flp_path.lsk_file)  # needed to convert between times using cspice functions

    print('Started TM import ...')
    data_utc_sc_data_all_spk = []
    data_utc_sc_data_all_ck = []
    for i in range(len(files)):
        with open(flp_path.TM_path + files[i]) as file:

            # read file content
            reader = csv.reader(file, delimiter=';')
            rows = [r for r in reader]
            print('File ' + str(i+1) + ' (' + files[i] + ') has ' + str(len(rows)) + ' rows')
            header = rows[0]
            print('Computations ongoing ...')
            input_data = rows[1:]

            # check for default lines and store data in lists
            utc = []
            sc_data = []
            data_utc_sc_data = []
            cspice.furnsh(flp_path.lsk_file)
            for k in range(len(input_data)):
                if input_data[k][2] != '0':
                    utc.append(input_data[k][0])
                    sc_data.append(input_data[k][2:])
                    data_utc_sc_data.append(input_data[k])

            if 'AYTPOS00' in header[2]:

                # find GPS data gaps inside the actual input file
                ET = np.array([cspice.utc2et(utc[i]) for i in range(len(utc))])
                index_cut = [0]
                for i in range(len(ET)-1):
                    if ET[i+1] - ET[i] > max_interval_spk:
                        index_cut.append(i)
                index_cut.append(len(ET)-1)  # (second) last element ... really needed. otherwise the propagation etc gets weird. !!!!
                u = 0

                # divide input file data into data frames for each continous periode.
                for i in range(len(index_cut)-1):
                    if index_cut[i+1] - index_cut[i] > poly_degree*step_spk:  # avoid "windows" with too less data points
                        utc_cut = []
                        sc_data_cut = []
                        for k in range(index_cut[i]+1,index_cut[i+1]):  # +1 because of better propagation fit. this number is adjustable.
                            utc_cut.append(utc[k])
                            sc_data_cut.append(sc_data[k])
                        sc_data_cut = np.asarray(sc_data_cut).astype(np.float) / 1000  # change to an array with [km] instead of list with [m]

                        if plots == 'on':
                            plot_input_data(utc_cut, sc_data_cut, header, figure_size)

                        # store the file
                        tmp_start = utc_cut[0].split(".")[0].replace("-", "").replace(" ", "").replace(":", "")
                        tmp_end = utc_cut[-1].split(".")[0].replace("-", "").replace(" ", "").replace(":", "")

                        name = 'TMsplit_spk_' + tmp_start + '_' + tmp_end
                        # Store utc, sc_data in new file
                        file = open(flp_path.spk_source + name, 'w')
                        for i in range(0, len(utc_cut), step_spk):
                            file.write(','.join([utc_cut[i], str(sc_data_cut[i, 0]), str(sc_data_cut[i, 1]), str(sc_data_cut[i, 2]),
                                                     str(sc_data_cut[i, 3]), str(sc_data_cut[i, 4]), str(sc_data_cut[i, 5]) + '\n']))
                        # always include the last input row (but not twice)
                        if (len(utc_cut) - 1) % step_spk != 0:
                            file.write(','.join([utc_cut[-1], str(sc_data_cut[-1, 0]), str(sc_data_cut[-1, 1]), str(sc_data_cut[-1, 2]),
                                                     str(sc_data_cut[-1, 3]), str(sc_data_cut[-1, 4]), str(sc_data_cut[-1, 5]) + '\n']))
                        file.close()
                    else:
                        u = u+1
                        print(str(u) + ' files have not enough data rows')

                # append data from this file to the overall GPS data
                data_utc_sc_data_all_spk.append(data_utc_sc_data)

            if 'AYTFQU00' in header[2]:

                sc_data = np.asarray(sc_data).astype(np.float)

                if plots == 'on':
                    plot_input_data(utc, sc_data, header, figure_size)

                tmp_start = utc[0].split(".")[0].replace("-", "").replace(" ", "").replace(":", "")
                tmp_end = utc[-1].split(".")[0].replace("-", "").replace(" ", "").replace(":", "")

                name = 'TM_ck_' + tmp_start + '_' + tmp_end
                # Store utc, sc_data in new file
                file = open(flp_path.ck_source + name, 'w')
                for i in range(0, len(utc), step_ck):
                    file.write(' '.join([cspice.et2utc(cspice.utc2et(utc[i]), 'ISOC', 3),
                                         str(sc_data[i, 3]), str(sc_data[i, 0]*(-1)), str(sc_data[i, 1]*(-1)),
                                         str(sc_data[i, 2]*(-1)) + '\n']))
                # always include the last input row (but not twice)
                if (len(utc) - 1) % step_ck != 0:
                    # changes the input file to the spice quaternion style. this is not mandatory, but if you change back
                    # make sure you change the "quaternion style" argument in the msopck routine too!
                    file.write(
                        ' '.join([cspice.et2utc(cspice.utc2et(utc[-1]), 'ISOC', 3), str(sc_data[-1, 3]), str(sc_data[-1, 0]*(-1)),
                                  str(sc_data[-1, 1]*(-1)),
                                  str(sc_data[-1, 2]*(-1)) + '\n']))
                file.close()

                # append data from this file to the overall QUAT data
                data_utc_sc_data_all_ck.append(data_utc_sc_data)

    data_utc_sc_data_all_spk.sort()  # needed to be sorted for a nice timeline spk - gps data comparsion plot
    data_utc_sc_data_all_ck.sort()  # needed to be sorted for a nice timeline ck - TM data comparsion plot ##maybe does not work correctly

    print('Step 1 completed \n\n')
    cspice.unload(flp_path.lsk_file)

    return data_utc_sc_data_all_spk, data_utc_sc_data_all_ck


def plot_input_data(utc, sc_data, header, figure_size, flp_path):

    # general input to get nice labels
    cspice.furnsh(flp_path.lsk_file)
    et = cspice.str2et(utc)
    timeline = []
    style = 'YYYY-MM-DD-HR-MN-SC.######'
    time = cspice.timout(et, style, 26)
    for i in range(len(et)):
        sub = time[i].replace(".", "-").split("-")
        timeline.append(datetime.datetime(int(sub[0]), int(sub[1]), int(sub[2]),
                                   int(sub[3]), int(sub[4]), int(sub[5]), int(sub[6]), tzinfo=None))

    # check for ck/spk file (only works with the right input format)
    if 'AYTPOS00' in header[2]:

        # coordinate plot
        fig, ax = plt.subplots(figsize=(figure_size[0], figure_size[1]))
        plt.plot(timeline, sc_data[:,0], label='GPS x')
        plt.plot(timeline, sc_data[:,1], label='GPS y')
        plt.plot(timeline, sc_data[:,2], label='GPS z')
        plt.gcf().autofmt_xdate()
        ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=3)
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(15)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(15)
        plt.grid()
        fig.subplots_adjust(bottom=0.28)
        plt.show()

        # coordinate magnitude plot
        vec_len = np.array([np.sqrt(np.square(sc_data[k,0]) +
                                    np.square(sc_data[k,1]) +
                                    np.square(sc_data[k,2]))
                                    for k in range(len(sc_data))])

        fig, ax = plt.subplots(figsize=(figure_size[0], figure_size[1]))
        plt.plot(timeline, vec_len, label='magnitude coordinate vector')
        plt.gcf().autofmt_xdate()
        ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=1)
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(15)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(15)
        plt.grid()
        fig.subplots_adjust(bottom=0.28)
        plt.show()

    if 'AYTFQU00' in header[2]:

        # quaternion plot
        fig, ax = plt.subplots(figsize=(figure_size[0], figure_size[1]))
        plt.plot(timeline, sc_data[:, 0], label='quat x')
        plt.plot(timeline, sc_data[:, 1], label='quat y')
        plt.plot(timeline, sc_data[:, 2], label='quat z')
        plt.plot(timeline, sc_data[:, 3], label='quat q')
        plt.gcf().autofmt_xdate()
        ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=4)
        for tick in ax.xaxis.get_major_ticks():
            tick.label.set_fontsize(15)
        for tick in ax.yaxis.get_major_ticks():
            tick.label.set_fontsize(15)
        plt.grid()
        fig.subplots_adjust(bottom=0.28)
        plt.show()


def create_input_spk(poly_degree, flp_path):
    spk_master_creation(poly_degree,flp_path)
    create_spk_master_spkmerge_setup(flp_path)
    run_spkmerge_master(flp_path)

    cover = cspice.utils.support_types.SPICEDOUBLE_CELL(1000)
    cspice.spkcov(flp_path.spk_out_name, -513, cover)
    if len(cover) == 2:
        gmat = "no"
    else:
        gmat = "yes"

    return gmat


def spk_master_creation(poly_degree, flp_path):

    create_mkspk_master_setup(poly_degree,flp_path)

    # get all input data files
    source_files = []
    for file in os.listdir(flp_path.spk_source):
        if not file.endswith(".gitkeep"):
            source_files.append(file)
    print('Amount of GPS data files detected: ' + str(len(source_files)))

    for i in range(len(source_files)):

        # Create new spk files
        name_tmp = 'spk_new_' + str(i) + '.bsp'

        spk_command = 'mkspk -setup ' + flp_path.spk_master_mkspk_setup + ' -input ' + flp_path.spk_source + source_files[i] + \
                  ' -output ' + flp_path.spk_new + name_tmp + ' > /dev/null'
        subprocess.call(spk_command, shell=True)

        # name spk file related to its coverage
        spk = flp_path.spk_new + name_tmp
        sc_id = -513
        print(source_files[i])
        time_tmp = spk_window(spk,sc_id, flp_path)
        time_start = time_tmp[0].split(".")[0].replace("-", "").replace(" ", "").replace(":", "")
        time_end = time_tmp[1].split(".")[0].replace("-", "").replace(" ", "").replace(":", "")
        spk_name_new = flp_path.spk_new + 'spk_input_' + time_start + '_' + time_end + '.bsp'

        # name the newly gernerated file appropriate
        rename = 'mv ' + flp_path.spk_new + name_tmp + ' ' + spk_name_new
        subprocess.call(rename, shell=True)

    # Brief the files (doublecheck coverage) -- developing mode
    spk_brief = 'brief -t -a -utc ' + flp_path.lsk_file + ' ' + flp_path.spk_new + '*.bsp'
    subprocess.call(spk_brief, shell=True)


def create_mkspk_master_setup(poly_degree, flp_path):
    # write mkspk setup file
    file = open(flp_path.spk_master_mkspk_setup, 'w')
    text = str(
        "\\begindata\n\n"
        "INPUT_DATA_TYPE = 'STATES'" + "\n" +
        "OUTPUT_SPK_TYPE = 9" + "\n" +
        "OBJECT_ID = -513" + "\n" +
        "CENTER_ID = 399" + "\n" +
        "REF_FRAME_NAME = 'ITRF93'" + "\n" +
        "PRODUCER_ID = 'IRS'" + "\n" +
        "DATA_ORDER = 'epoch x y z vx vy vz'" + "\n" +
        "DATA_DELIMITER = ','" + "\n" +
        "LEAPSECONDS_FILE = '" + flp_path.lsk_file + "'\n" +
        "PCK_FILE = '" + flp_path.pck_normal + "'\n" +
        "COMMENT_FILE = '" + flp_path.spk_master_mkspk_comment + "'\n" +
        "INPUT_DATA_UNITS = 'DISTANCES= KM'" + "\n" +
        "LINES_PER_RECORD = 1" + "\n" +
        "POLYNOM_DEGREE = " + str(poly_degree) + "\n" +
        "SEGMENT_ID = 'SEGMENT X'" + "\n" +  # how should i name it?
        "APPEND_TO_OUTPUT = 'NO'" + "\n\n" +
        "\\begintext\n")
    file.write(text)


def spk_window(spk,sc_id, flp_path):
    cover = cspice.utils.support_types.SPICEDOUBLE_CELL(1000)
    cspice.spkcov(spk, sc_id, cover)
    intervals = cspice.wncard(cover)
    print(intervals)
    # check if it is one continuous interval or many seperated ones (in this case a
    # error message is thrown as I don't want to include not_continous intervalls
    if intervals >= 2:
        print("Attention: The spk coverage is not continous!!!")

    cspice.furnsh(flp_path.lsk_file)
    tmp_begin = cover[0]
    tmp_end = cover[-1]
    time_0 = cspice.et2utc(tmp_begin, 'ISOC', 0)
    time_1 = cspice.et2utc(tmp_end, 'ISOC', 0)
    time_tmp = [time_0, time_1]
    cspice.unload(flp_path.lsk_file)

    return time_tmp


def create_spk_master_spkmerge_setup(flp_path):

    # get a list of all new spks to merge inside "master spk 111"
    spk_source = []
    for file in os.listdir(flp_path.spk_new):
        if not file.endswith(".gitkeep"):
           spk_source.append(file)
    # NOTE: the spkmerge routine superseeds input files exactly the
    # other way around as the mk furnsh routine. That's why the for loop later on is inverted!
    # write spk_111_merge_setup_1 file
    file = open(flp_path.spk_master_merge_setup_1, 'w')

    text_1 = str(
        "LEAPSECONDS_KERNEL = " + flp_path.lsk_file + "\n" + \
        "SPK_KERNEL = " + flp_path.spk_out_name + "\n" )
    file.write(text_1)

    for i in range(len(spk_source)-1, -1, -1):
        file.write(','.join(["SOURCE_SPK_KERNEL = " + flp_path.spk_new + spk_source[i] + "\n"]))

    file.close()


def run_spkmerge_master(flp_path):

    # run the actual utility program
    run_spk_master_merge = "spkmerge " + flp_path.spk_master_merge_setup_1 + ' > /dev/null'
    subprocess.call(run_spk_master_merge, shell=True)


def prop_spk_gaps(flp_path):

    drag_area = 0.078

    print('Start gmat propagation of input spk gaps')
    # load all SPICE kernels needed for spice function usage (not the SPK file, this will be loaded individually)
    cspice.furnsh(flp_path.fk_file)
    cspice.furnsh(flp_path.pck_itrf)
    spk_111_file = flp_path.spk_out_name
    cspice.furnsh(spk_111_file)
    print('Used input spk file: ' + str(spk_111_file))

    cover = cspice.utils.support_types.SPICEDOUBLE_CELL(1000)
    cspice.spkcov(spk_111_file, -513, cover)
    time = np.array([cspice.et2utc(cover[i], 'C', 3) for i in range(len(cover))])

    print('If nothing is happening, maybe GMAT got a problem, delete the  > /dev/null in the code to debug... but wait a while before!!')

    for i in range(1,len(time)-1,2):
        # it is important to transform the (ITRF93 data) frame to the J2000Eq frame as this is the frame used for GMAT propagations!!!
        et_fix = cspice.str2et(time[i])
        duration = cspice.str2et(time[i+1]) - cspice.str2et(time[i])
        state = cspice.spkezr('-513', et_fix, 'J2000', 'NONE', 'EARTH')[0]

        # start iterations
        utc_fix_start = cspice.et2utc(et_fix, 'ISOC', 0)
        utc_fix_end = cspice.et2utc(et_fix + duration, 'ISOC', 0)
        gmat_sim_start = utc_fix_start.replace("-", "").replace(" ", "").replace(":", "")
        gmat_sim_stop = utc_fix_end.replace("-", "").replace(" ", "").replace(":", "")
        gmat_sim_spk_name = 'spk_gmat_' + gmat_sim_start + '_' + gmat_sim_stop + "_gmat.bsp"
        gmat_sim_script_name = 'spk_gmat_' + gmat_sim_start + '_' + gmat_sim_stop + ".script"

        default_script_name = flp_path.gmat_default_script
        storage_script_name = flp_path.gmat_storage + gmat_sim_script_name
        storage_spk_file = flp_path.gmat_storage + gmat_sim_spk_name
        gmat_console_path = flp_path.gmat

        # copy GMAT default_script
        copy = "cp " + default_script_name + " " + storage_script_name
        subprocess.call(copy, shell=True)

        # compute needed values for usage in GMAT script
        style = 'DD Mon YYYY HR:MN:SC.###'
        state_utc = cspice.timout(et_fix, style, 25)

        # insert state_new (and utc_fix) parameters in GMAT script
        file = open(storage_script_name)
        input = file.read()
        file.close()
        file = open(storage_script_name, 'w')
        output = re.sub(r'FLP_SC\.Epoch\s*=\s*\'Start_Time\'', "FLP_SC.Epoch = '" + state_utc + "'", input)
        output = re.sub(r'FLP_SC\.ElapsedSecs\s*=\s*\'Duration\'', "FLP_SC.ElapsedSecs = " + str(duration), output)
        output = re.sub(r'FLP_SPK\.Filename\s*=\s*\'Output_File_Path_Name\'',
                        "FLP_SPK.Filename = '" + storage_spk_file + "'", output)
        output = re.sub(r'FLP_SC\.X\s*=\s*\'X\'', "FLP_SC.X = " + str(state[0]), output)
        output = re.sub(r'FLP_SC\.Y\s*=\s*\'Y\'', "FLP_SC.Y = " + str(state[1]), output)
        output = re.sub(r'FLP_SC\.Z\s*=\s*\'Z\'', "FLP_SC.Z = " + str(state[2]), output)
        output = re.sub(r'FLP_SC\.VX\s*=\s*\'VX\'', "FLP_SC.VX = " + str(state[3]), output)
        output = re.sub(r'FLP_SC\.VY\s*=\s*\'VY\'', "FLP_SC.VY = " + str(state[4]), output)
        output = re.sub(r'FLP_SC\.VZ\s*=\s*\'VZ\'', "FLP_SC.VZ = " + str(state[5]), output)
        output = re.sub(r'FLP_SC\.DragArea\s*=\s*\'DRAG_AREA\'', "FLP_SC.DragArea = " + str(drag_area), output)
        file.write(output)
        file.close()

        # Run GMAT script
        os.chdir(gmat_console_path)
        gmat_script_command = './GmatConsole ' + storage_script_name + ' > /dev/null'
        subprocess.call(gmat_script_command, shell=True)

    cspice.unload(flp_path.fk_file)
    cspice.unload(flp_path.pck_itrf)
    cspice.unload(spk_111_file)

    create_spk_gmat_spkmerge_setup()
    run_spkmerge_gmat()


def create_spk_gmat_spkmerge_setup(flp_path):

    # get a list of all new spks to merge inside "master spk 111"
    gmat_files = []
    for file in os.listdir(flp_path.spk_new):
        if file.endswith("_gmat.bsp"):
            gmat_files.append(file)

    # NOTE: the spkmerge routine superseeds input files exactly the
    # other way around as the mk furnsh routine. That's why the for loop later on is inverted!
    # write spk_111_merge_setup_1 file
    file = open(flp_path.spk_gmat_merge_setup, 'w')

    text_1 = str(
        "LEAPSECONDS_KERNEL = " + flp_path.lsk_file + "\n" + \
        "SPK_KERNEL = " + flp_path.gmat_out_name + "\n" )
    file.write(text_1)

    for i in range(len(gmat_files)-1, -1, -1):
        file.write(','.join(["SOURCE_SPK_KERNEL = " + flp_path.spk_new + gmat_files[i] + "\n"]))

    file.close()


def run_spkmerge_gmat(flp_path):

    # run the actual utility program
    run_spk_gmat_merge = "spkmerge " + flp_path.spk_gmat_merge_setup + ' > /dev/null'
    subprocess.call(run_spk_gmat_merge, shell=True)


def plot_validation_spk(data_utc_sc_data_all_spk, gmat_prop, figure_size, timesteps_plot, flp_path, gmat):

    # iterate over all created spks
    cspice.furnsh(flp_path.lsk_file)
    spk = flp_path.spk_out_name
    cspice.furnsh(spk)

    # precalculations
    style = 'YYYY-MM-DD-HR-MN-SC.###'
    cover = cspice.utils.support_types.SPICEDOUBLE_CELL(1000)
    cspice.spkcov(spk, -513, cover)
    time_tmp = np.array([cspice.et2utc(cover[i], 'ISOC', 3) for i in range(len(cover))])
    time_start = cspice.timout(cover[0], style, 23).split(".")[0].replace("-", "")  # plot naming
    time_end = cspice.timout(cover[-1], style, 23).split(".")[0].replace("-", "")  # plot naming

    ET = []
    for i in range(0,len(time_tmp),2):
        et1 = cspice.utc2et(time_tmp[i])
        et2 = cspice.utc2et(time_tmp[i+1])
        ET_tmp = np.linspace(et1, et2, timesteps_plot)
        ET.append(ET_tmp)

    spk_data = []
    time = []
    for item in ET:
        for subitem in item:
            spk_data.append(cspice.spkezr('-513', subitem, 'ITRF93', 'NONE', 'EARTH')[0])
            time.append(cspice.timout(subitem, style, 23))
    cspice.unload(spk)

    # coordinate magnitude plot
    # timeline for ET
    timeline = []
    for i in range(len(ET)*timesteps_plot):
        sub = time[i].replace(".", "-").split("-")
        timeline.append(datetime.datetime(int(sub[0]), int(sub[1]), int(sub[2]),
                                              int(sub[3]), int(sub[4]), int(sub[5]), int(sub[6]), tzinfo=None))
    # timeline for ET_2
    ET_2 = []
    for item in data_utc_sc_data_all_spk:
        for subitem in item:
            ET_2.append(cspice.utc2et(subitem[0]))

    timeline_2 = []
    time_2 = cspice.timout(ET_2, style, 23)

    for i in range(len(ET_2)):
        sub_2 = time_2[i].replace(".", "-").split("-")
        timeline_2.append(datetime.datetime(int(sub_2[0]), int(sub_2[1]), int(sub_2[2]),
                                              int(sub_2[3]), int(sub_2[4]), int(sub_2[5]), int(sub_2[6]), tzinfo=None))


    if gmat_prop == 'on' and gmat == "yes":
        # timeline for combined view (spk TM and spk prop for TM gaps)
        spk_2 = flp_path.gmat_out_name
        ET_3 = np.linspace(cover[0], cover[-1], timesteps_plot)
        timeline_3 = []
        time_3 = cspice.timout(ET_3, style, 23)
        for i in range(len(ET_3)):
            sub_3 = time_3[i].replace(".", "-").split("-")
            timeline_3.append(datetime.datetime(int(sub_3[0]), int(sub_3[1]), int(sub_3[2]),
                                                int(sub_3[3]), int(sub_3[4]), int(sub_3[5]), int(sub_3[6]), tzinfo=None))

        cspice.furnsh(spk_2)
        cspice.furnsh(spk)
        cspice.furnsh(flp_path.pck_itrf)
        spk_data_3 = np.array([cspice.spkezr('-513', ET_3[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET_3))])
        cspice.unload(flp_path.pck_itrf)
        cspice.unload(spk_2)
        cspice.unload(spk)

        vec_len_3 = np.array([np.sqrt(np.square(spk_data_3[k][0]) + np.square(spk_data_3[k][1]) + np.square(spk_data_3[k][2]))
                              for k in range(len(spk_data_3))])


    vec_len_1 = np.array([np.sqrt(np.square(spk_data[k][0]) + np.square(spk_data[k][1]) + np.square(spk_data[k][2]))
                              for k in range(len(spk_data))])

    vec_len_2 = []
    x = []
    y = []
    z = []
    for item in data_utc_sc_data_all_spk:
        for subitem in item:
            # subitem[0] = utc; subitem[1] = sc clock time
            x_tmp = float(subitem[2]) / 1000
            y_tmp = float(subitem[3]) / 1000
            z_tmp = float(subitem[4]) / 1000
            vec_len_2.append(np.sqrt(np.square(x_tmp) + np.square(y_tmp) + np.square(z_tmp)))
            x.append(x_tmp)
            y.append(y_tmp)
            z.append(z_tmp)

    fig, ax = plt.subplots(figsize=(figure_size[0], figure_size[1]))
    ax.set_ylabel('distance [km]', fontsize=18)
    if gmat_prop == 'on' and gmat == "yes":
        plt.plot(timeline_3, spk_data_3[:,0], label='x spk gmat', color='red')
        plt.plot(timeline_3, spk_data_3[:,1], label='y spk gmat', color='blue')
        plt.plot(timeline_3, spk_data_3[:,2], label='z spk gmat', color='green')
    else:
        spk_data = np.asarray(spk_data).astype(np.float)
        plt.plot(timeline, spk_data[:,0], label='x spk', color='red')
        plt.plot(timeline, spk_data[:,1], label='y spk', color='blue')
        plt.plot(timeline, spk_data[:,2], label='z spk', color='green')

    plt.plot(timeline_2, x, label='x TM', linestyle='--', color='red')
    plt.plot(timeline_2, y, label='y TM', linestyle='--', color='blue')
    plt.plot(timeline_2, z, label='z TM', linestyle='--', color='green')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=6)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.28)
    fig.savefig(flp_path.plots + 'validation_spk_coordinates_' + time_start + '_' + time_end + '.png', dpi=fig.dpi)
    plt.show()

    fig, ax = plt.subplots(figsize=(figure_size[0], figure_size[1]))
    ax.set_ylabel('distance [km]', fontsize=18)
    if gmat_prop == 'on' and gmat == "yes":
        plt.plot(timeline_3, vec_len_3, label='propagation', color='green')
    plt.plot(timeline_2, vec_len_2, label='input data', color='black', marker='x')
    plt.plot(timeline, vec_len_1, label='spk input data', color='brown', marker='o')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=3)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.28)
    fig.savefig(flp_path.plots + 'validation_spk_magnitude_' + time_start + '_' + time_end + '.png', dpi=fig.dpi)
    plt.show()


def plot_spk_dif(data_utc_sc_data_all_spk, figure_size, flp_path, gmat_prop, gmat):
    # load the needed kernels
    cspice.furnsh(flp_path.lsk_file)
    cspice.furnsh(flp_path.pck_itrf)
    spk = flp_path.spk_out_name
    cspice.furnsh(spk)
    if gmat_prop == 'on' and gmat == "yes":
        spk_2 = flp_path.gmat_out_name
        cspice.furnsh(spk_2)
    # get nice timeline for plot
    ET = []
    style = 'YYYY-MM-DD-HR-MN-SC.###'
    for item in data_utc_sc_data_all_spk:
        for subitem in item:
            et_tmp = cspice.utc2et(subitem[0])
            ET.append(et_tmp)
    ET = ET[1:-1]  # needed to align with produced SPK file
    timeline = []
    time = cspice.timout(ET, style, 23)
    for i in range(len(ET)):
        sub_2 = time[i].replace(".", "-").split("-")
        timeline.append(datetime.datetime(int(sub_2[0]), int(sub_2[1]), int(sub_2[2]),
                                            int(sub_2[3]), int(sub_2[4]), int(sub_2[5]), int(sub_2[6]), tzinfo=None))
    # position of the FLP SC according to GPS:
    x = []
    y = []
    z = []
    for item in data_utc_sc_data_all_spk:
        for subitem in item:
            # subitem[0] = utc; subitem[1] = sc clock time
            x.append(float(subitem[2]) / 1000)
            y.append(float(subitem[3]) / 1000)
            z.append(float(subitem[4]) / 1000)
    x = x[1:-1]  # because of index shift for ET!
    y = y[1:-1]
    z = z[1:-1]
    # position of the FLP SC according to the SPK kernels produced:
    spk_data = np.array([cspice.spkezr('-513', ET[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET))])
    # difference between both positions:
    vec_diff_x = np.array([x[i] - spk_data[i, 0] for i in range(len(ET))])
    vec_diff_y = np.array([y[i] - spk_data[i, 1] for i in range(len(ET))])
    vec_diff_z = np.array([z[i] - spk_data[i, 2] for i in range(len(ET))])

    # plot to check SPK against input GPS at GPS data point times.
    fig, ax = plt.subplots(figsize=(figure_size[0], figure_size[1]))
    ax.set_ylabel('difference [km]', fontsize=18)
    plt.plot(timeline, vec_diff_x, label='x', color='green')
    plt.plot(timeline, vec_diff_y, label='y', color='black')
    plt.plot(timeline, vec_diff_z, label='z', color='brown')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=3)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.28)
    plt.show()

    # check SPK kernel with only GPS data points times (and linearly fit) with same SPK kernel with more
    # data points
    # new timeline

    if ET[-1] - ET[0] < 30000:
        points = ET[-1] - ET[0]
    else:
        points = 30000
    ET_2 = np.linspace(ET[0],ET[-1],points)
    timeline_2 = []
    time_2 = cspice.timout(ET_2, style, 23)
    for i in range(0,len(ET_2)-1):
        sub_2 = time_2[i].replace(".", "-").split("-")
        timeline_2.append(datetime.datetime(int(sub_2[0]), int(sub_2[1]), int(sub_2[2]),
                                          int(sub_2[3]), int(sub_2[4]), int(sub_2[5]), int(sub_2[6]), tzinfo=None))
    # coordinate interpolation of GPS data
    x_inter = []
    y_inter = []
    z_inter = []
    m = 0
    for i in range(len(ET_2)):
        for k in range(m,len(ET)-1):
            if ET[k] <= ET_2[i] and ET[k+1] > ET_2[i]:
            # this leads to huge differences if ET is not sampled regularly.
            # this is the reason the gmat propagation is off by 1000th of kilometers.
            # the "best" way to check the precision of the gmat propagation is its last value always shwon in the plot
            # before. (more or less by accident :D)
                # value from closest time before ET_2[i] + difference of x position * time delta / time delta GPS
                x_inter_tmp = x[k] + (x[k+1]-x[k])*((ET_2[i]-ET[k])/(ET[k+1]-ET[k]))
                y_inter_tmp = y[k] + (y[k + 1] - y[k]) * ((ET_2[i]-ET[k]) / (ET[k + 1] - ET[k]))
                z_inter_tmp = z[k] + (z[k + 1] - z[k]) * ((ET_2[i]-ET[k]) / (ET[k + 1] - ET[k]))
                if x_inter == []:
                    x_inter.append(x_inter_tmp)
                    y_inter.append(y_inter_tmp)
                    z_inter.append(z_inter_tmp)
                if x_inter[-1] != x_inter_tmp:
                    x_inter.append(x_inter_tmp)
                    y_inter.append(y_inter_tmp)
                    z_inter.append(z_inter_tmp)
                break
        m = k

    # position of FLP SC using SPK kernels
    spk_data_2 = np.array([cspice.spkezr('-513', ET_2[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET_2))])
    # difference of the new positions
    vec_diff_x_2 = np.array([x_inter[i] - spk_data_2[i, 0] for i in range(len(x_inter))])
    vec_diff_y_2 = np.array([y_inter[i] - spk_data_2[i, 1] for i in range(len(x_inter))])
    vec_diff_z_2 = np.array([z_inter[i] - spk_data_2[i, 2] for i in range(len(x_inter))])

    # get the absolut from the values and the sum of all absolut values
    vec_abs_diff_x = np.array([np.abs(vec_diff_x_2[i]) for i in range(len(vec_diff_x_2))])
    vec_abs_diff_y = np.array([np.abs(vec_diff_y_2[i]) for i in range(len(vec_diff_y_2))])
    vec_abs_diff_z = np.array([np.abs(vec_diff_z_2[i]) for i in range(len(vec_diff_z_2))])
    vec_abs_diff = np.array([vec_abs_diff_x[i] + vec_abs_diff_y[i] + vec_abs_diff_z[i] for i in range(len(vec_diff_x_2))])

    fig, ax = plt.subplots(figsize=(figure_size[0], figure_size[1]))
    ax.set_ylabel('difference [km]', fontsize=18)
    plt.plot(timeline_2, vec_diff_x_2, label='x', color='green')
    plt.plot(timeline_2, vec_diff_y_2, label='y', color='black')
    plt.plot(timeline_2, vec_diff_z_2, label='z', color='brown')
    plt.plot(timeline_2, vec_abs_diff, label='abs', color='red', linestyle='--')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=4)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.28)
    plt.show()


def create_input_ck(max_interval, flp_path):
    run_msopck(max_interval,flp_path)

    # get the ck_master coverage
    cspice.furnsh(flp_path.ck_out_name)
    cspice.furnsh(flp_path.sclk_file)
    cover = cspice.ckcov(flp_path.ck_out_name, -513000, False, 'INTERVAL', 0.0, 'TDB')
    time_tmp = np.array([cspice.et2utc(cover[i], 'C', 3) for i in range(len(cover))])
    print("\n CK coverage intervals: " + str(int(len(cover)/2)) + "\n")
    for i in range(0, len(time_tmp) - 1, 2):
        print(time_tmp[i] + " - " + time_tmp[i + 1])

    # Brief the files (doublecheck coverage) -- developing mode
    #ck_brief = 'ckbrief -t -a -utc ' + flp_path.lsk_file + ' ' + flp_path.sclk_file + ' ' + flp_path.ck_out + '*.bc'
    #subprocess.call(ck_brief, shell=True)


def run_msopck(max_interval, flp_path):

    # No deletion of master ck_111 file on purpose!

    # Create the script needed for the prediCkt utility program
    create_ck_master_setup(max_interval,flp_path)
    ck_source = []

    for file in os.listdir(flp_path.ck_source):
        if not file.endswith(".gitkeep"):
           ck_source.append(file)

    if ck_source != []:
        for i in range(len(ck_source)):
            # Create new ck file
            ck_command = 'msopck ' + flp_path.ck_master_setup + ' ' + flp_path.ck_source + ck_source[i] + \
                         ' ' + flp_path.ck_out_name + ' > /dev/null'
            subprocess.call(ck_command, shell=True)
    else:
        print('\n\nNo attitude input data provided!')


def create_ck_master_setup(max_interval, flp_path):

    file = open(flp_path.ck_master_setup, 'w')

    # write flp.furnsh file
    text = str(
        "\\begindata\n\n" + \
 \
        "LSK_FILE_NAME           = '" + flp_path.lsk_file + "'\n" + \
        "SCLK_FILE_NAME           = '" + flp_path.sclk_file + "'\n" + \
        "COMMENTS_FILE_NAME           = '" + flp_path.ck_master_comments + "'\n" + \
        "FRAMES_FILE_NAME           = '" + flp_path.fk_file + "'\n" + \
        "INTERNAL_FILE_NAME      = 'NAME 1'\n" + \
        "CK_TYPE                 = 3\n" + \
        "CK_SEGMENT_ID           = 'SEGMENT NAME 1'\n" + \
        "INSTRUMENT_ID           = -513000\n" + \
        "REFERENCE_FRAME_NAME    = 'J2000'\n" + \
        "ANGULAR_RATE_PRESENT    = 'MAKE UP'\n" + \
        "INPUT_DATA_TYPE         = 'SPICE QUATERNIONS'\n" + \
        "INPUT_TIME_TYPE         = 'UTC'\n" + \
        "MAXIMUM_VALID_INTERVAL  = " + str(max_interval) + "\n" + \
        "QUATERNION_NORM_ERROR   = 1.0E-3\n" + \
        "PRODUCER_ID             = 'IRS'\n" + \
        "DOWN_SAMPLE_TOLERANCE   = 0.001\n\n" + \
 \
        "\\begintext\n\n"
    )
    file.write(text)
    file.close()


def plot_validation_ck(data_utc_sc_data_all_ck, figure_size, timesteps_plot, flp_path):

    # iterate over all created spks
    cspice.furnsh(flp_path.lsk_file)
    cspice.furnsh(flp_path.sclk_file)
    ck = flp_path.ck_out_name
    cspice.furnsh(ck)

    # precalculations
    style = 'YYYY-MM-DD-HR-MN-SC.###'
    cover = cspice.ckcov(flp_path.ck_out_name, -513000, False, 'INTERVAL', 0.0, 'TDB')
    time_tmp = np.array([cspice.et2utc(cover[i], 'ISOC', 0) for i in range(len(cover))])
    time_start = cspice.timout(cover[0], style, 26).split(".")[0].replace("-", "")  # plot naming
    time_end = cspice.timout(cover[-1], style, 26).split(".")[0].replace("-", "")  # plot naming

    ET = []
    for i in range(0,len(time_tmp),2):
        if cover[i] != cover[i+1]:  # needed to not fail the ckgp routine later on ...
            et1 = cspice.utc2et(time_tmp[i])+1  # a small margin towards the ends is needed... (spice routine)
            et2 = cspice.utc2et(time_tmp[i+1])-1  # a small margin towards the ends is needed... (spice routine)
            ET_tmp = np.linspace(et1, et2, timesteps_plot)
            ET.append(ET_tmp)

    ck_data = []
    time = []
    for item in ET:
        for subitem in item:
            matrix, t= cspice.ckgp(-513000, cspice.sce2c(-513,subitem), 0, 'J2000')
            ck_data.append(cspice.m2q(matrix))
            time.append(cspice.timout(subitem, style, 23))


    ck_data_0 = []
    ck_data_1 = []
    ck_data_2 = []
    ck_data_3 = []
    for item in ck_data:
        ck_data_0.append(item[0])
        ck_data_1.append(item[1])
        ck_data_2.append(item[2])
        ck_data_3.append(item[3])

    # coordinate magnitude plot
    # timeline for ET
    timeline = []
    for i in range(len(ET)*timesteps_plot):
        sub = time[i].replace(".", "-").split("-")
        timeline.append(datetime.datetime(int(sub[0]), int(sub[1]), int(sub[2]),
                                              int(sub[3]), int(sub[4]), int(sub[5]), int(sub[6]), tzinfo=None))

    # timeline for ET_2
    ET_2 = []
    data_0 = []
    data_1 = []
    data_2 = []
    data_3 = []
    for item in data_utc_sc_data_all_ck:
        for subitem in item:
            ET_2.append(cspice.utc2et(subitem[0]))
            data_0.append(subitem[2])
            data_1.append(subitem[3])
            data_2.append(subitem[4])
            data_3.append(subitem[5])
    data_0 = np.asarray(data_0).astype(np.float)
    data_1 = np.asarray(data_1).astype(np.float)
    data_2 = np.asarray(data_2).astype(np.float)
    data_3 = np.asarray(data_3).astype(np.float)

    timeline_2 = []
    time_2 = cspice.timout(ET_2, style, 23)
    for i in range(len(ET_2)):
        sub_2 = time_2[i].replace(".", "-").split("-")
        timeline_2.append(datetime.datetime(int(sub_2[0]), int(sub_2[1]), int(sub_2[2]),
                                              int(sub_2[3]), int(sub_2[4]), int(sub_2[5]), int(sub_2[6]), tzinfo=None))


    fig, ax = plt.subplots(figsize=(figure_size[0], figure_size[1]))
    ax.set_ylabel('value [-]', fontsize=18)
    plt.plot(timeline, ck_data_0, label='ck quat q', color='red')
    plt.plot(timeline, ck_data_1, label='ck quat x', color='blue')
    plt.plot(timeline, ck_data_2, label='ck quat y', color='green')
    plt.plot(timeline, ck_data_3, label='ck quat z', color='brown')
    plt.plot(timeline_2, data_3[:], label='quat q', color='red', linestyle='--')
    plt.plot(timeline_2, data_0, label='quat x', color='blue', linestyle='--')
    plt.plot(timeline_2, data_1, label='quat y', color='green', linestyle='--')
    plt.plot(timeline_2, data_2, label='quat z', color='brown', linestyle='--')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=4)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.35)
    fig.savefig(flp_path.plots + 'validation_ck_quaternions_' + time_start + '_' + time_end + '.png', dpi=fig.dpi)
    plt.show()

    cspice.unload(flp_path.sclk_file)
    cspice.unload(ck)


def delete_inputs_1(flp_path):

    remove_old_spk_source = "rm -f " + flp_path.spk_source + "*"  # delete kernels/spk/source/ files...
    subprocess.call(remove_old_spk_source, shell=True)

    remove_old_spk_new = "rm -f " + flp_path.spk_new + "*"  # delete kernels/spk/new/ files...
    subprocess.call(remove_old_spk_new, shell=True)

    remove_old_spk_out = "rm -f " + flp_path.spk_out + "*"  # delete kernels/spk/out/ files...
    subprocess.call(remove_old_spk_out, shell=True)

    remove_old_gmat_out = "rm -f " + flp_path.gmat_out + "*"  # delete kernels/spk/gmat/ files...
    subprocess.call(remove_old_gmat_out, shell=True)

    remove_old_ck_source = "rm -f " + flp_path.ck_source + "*"  # delete kernels/ck/source/ files...
    subprocess.call(remove_old_ck_source, shell=True)

    remove_old_ck_out = "rm -f " + flp_path.ck_out + "*"  # delete kernels/ck/out/ files...
    subprocess.call(remove_old_ck_out, shell=True)

    remove_prop_ck = "rm -f " + flp_path.ck_prop + "*"  # delete kernels/ck/propagation/ files...
    subprocess.call(remove_prop_ck, shell=True)

    remove_prop_spk = "rm -f " + flp_path.spk_prop + "*"  # delete kernels/spk/propagation/ files...
    subprocess.call(remove_prop_spk, shell=True)


def manage_spk_data_input(flp_path):

    # Get all TM input files
    print('Searching for input files ...')
    files_tmp = []
    for file in os.listdir(flp_path.TM_path):
        if file.endswith(".csv"):
            files_tmp.append(file)
    marker = '~'
    files = []
    for i in range(len(files_tmp)):
        if marker not in files_tmp[i]:
            files.append(files_tmp[i])
    print('Amount of input files detected: ' + str(len(files)))

    # Create new spk and ck source files
    cspice.furnsh(flp_path.lsk_file)  # needed to convert between times using cspice functions

    print('Started TM import ...')
    input = []
    for i in range(len(files)):
        with open(flp_path.TM_path + files[i]) as file:

            # read file content
            reader = csv.reader(file, delimiter=';')
            rows = [r for r in reader]
            print('File ' + str(i+1) + ' (' + files[i] + ') has ' + str(len(rows)) + ' rows')
            header = rows[0]
            print('Computations ongoing ...')
            input_data = rows[1:]

            # check for default lines and store data in lists
            utc = []
            sc_data = []
            data_utc_sc_data = []
            cspice.furnsh(flp_path.lsk_file)
            for k in range(len(input_data)):
                if input_data[k][2] != '0':
                    utc.append(input_data[k][0])
                    sc_data.append(input_data[k][2:])
                    data_utc_sc_data.append(input_data[k])

            if 'AYTPOS00' in header[2]:
                # append data from this file to the overall GPS data
                input.append(data_utc_sc_data)

    input.sort()  # needed to be sorted for a nice timeline spk - gps data comparsion plot
    cspice.unload(flp_path.lsk_file)
    print('Step 1 completed \n\n')
    return input


def create_gmat_spk(input, utc_fix, utc_test, max_iter, del_i, step, utc_prop, error_window, flp_path, drag_area):
    # load all SPICE kernels needed for spice function usage (not the SPK file, this will be loaded individually)
    cspice.furnsh(flp_path.lsk_file)
    cspice.furnsh(flp_path.fk_file)
    cspice.furnsh(flp_path.pck_itrf)
    # if there is no continuous GPS data available this routine will fail!
    # this is due to the fact that using GMAT propagated gaps might be not precise enough and would yield to an even
    # more unpredictable propgagation.
    cspice.furnsh(flp_path.spk_out_name)

    eps = [100000000, 100000000, 100000000]  # set a high epsilon as start error
    et_fix = cspice.str2et(utc_fix)
    et_test = []
    for i in range(len(utc_test)):
        et_test.append(cspice.str2et(utc_test[i]))

    ET_input = []
    for item in input:
        for subitem in item:
            et_tmp = cspice.utc2et(subitem[0])
            ET_input.append(et_tmp)

    cut = []
    for i in range(len(et_test)):  # iterate through all utc test windows
        for k in range(0,len(ET_input)-1):
            if ET_input[k] <= et_test[i] and ET_input[k+1] > et_test[i]:
                if cut == []:
                    cut.append(k + 1)
                    break
                if cut[-1] != k + 1:
                    cut.append(k + 1)
                    break

    if len(cut)%2 != 0:
        print('Please check your input. It needs to be at least 10 hour of continous data.')
        quit()

    # get the test times within the utc_test windows for which GPS data was available:
    ET= []
    for i in range(int(np.floor(len(cut)/2))):
        et = []
        for k in range(cut[2*i],cut[2*i+1]):
            et.append(ET_input[k])
        ET.append(et)

    # it is important to transform the (ITRF93 data) frame to the J2000Eq frame as this is the frame used for GMAT propagations!!!
    state = cspice.spkezr('-513', et_fix, 'J2000', 'NONE', 'EARTH')[0]

    progress_storage_list = []

    # start iterations
    print('If nothing is happening, maybe GMAT got a proplem, delete the  > /dev/null in the code to debug...')

    for loop in range(len(step)):
        # GMAT script calculation duration: # the 100[s] are used as buffer
        duration = np.max(ET[loop]) - et_fix + 100
        progress, eps, gmat_sim_spk_name, state = comp_state(duration, max_iter[loop], del_i[loop], ET[loop],
                                                                  state, utc_fix, step[loop], eps, loop, drag_area, flp_path)
        progress_storage_list.append(progress)
        #eps = [i * 1000 for i in eps] # because a longer timespan is simulated (and the points to check are getting more
        print(loop)
        # (because i set it like that)...which increases normally the difference error

    # remove all GMAT created test files...
    remove = "rm " + flp_path.gmat_storage_future + "*"
    subprocess.call(remove, shell=True)

    print('DATA review: \n')
    for item in progress_storage_list:
        for subitem in item:
            print(subitem)

    # compute the long term GMAT SPK:
    gmat_SPK_long_term(utc_prop, progress_storage_list[-1][-1], drag_area, flp_path)  # progress should have stored the latest (and therefore best) SC state

    # show the results of the best fit
    plot_gmat_comparison(flp_path.spk_prop_name, ET, error_window)


def comp_state(duration, max_iter, del_i, et_test, state_fix, utc_fix, step, eps, loop, drag_area, flp_path):
    # Values for for loop
    i_old = 0
    progress = []
    check_points = len(et_test)
    print(check_points)

    # iterate over start conditions to get the "true" values
    for i in range(max_iter):

        state_new = [state_fix[k] + step * np.tan(np.pi * random.uniform(0, 1)) for k in range(len(state_fix))]

        storage_spk_file, gmat_sim_spk_name = gmat_propagator(utc_fix, state_new, duration, i, loop, drag_area, flp_path)

        eps_new = error_calculator(et_test, storage_spk_file)

        print("[" + str(i) + "/" + str(max_iter) + "]")

        if sum(eps_new) <= sum(eps):
            state_fix = state_new
            print([i, i_old, state_fix, sum(eps_new)/check_points])
            eps = eps_new
            # progress.append([i, i_old[p], p, state_new, eps_new[p]/check_points])  #-> the state the error is calculated for
            progress.append([i, i_old, state_fix, sum(eps_new)/check_points])  # -> the state used as input for next iteration
            if i - i_old <= del_i:
                i_old = i

        if i - i_old > del_i:
            print("got it" + str(i) , str(i_old))
            break

    return progress, eps, gmat_sim_spk_name, state_fix


def gmat_propagator(utc_fix, state_new, duration, i, loop, drag_area, flp_path):
    # file locations and names
    utc_fix_start = cspice.et2utc(cspice.str2et(utc_fix), 'ISOC', 0)
    utc_fix_end = cspice.et2utc(cspice.str2et(utc_fix) + duration, 'ISOC', 0)
    gmat_sim_start = utc_fix_start.replace("-", "").replace(" ", "").replace(":", "")
    gmat_sim_stop = utc_fix_end.replace("-", "").replace(" ", "").replace(":", "")
    gmat_sim_spk_name = 'spk_prop_' + gmat_sim_start + '_' + gmat_sim_stop + "_v" + str(loop) + "_t" + str(i) + ".bsp"
    gmat_sim_script_name = 'spk_prop_' + gmat_sim_start + '_' + gmat_sim_stop + "_v" + str(loop) + "_t" + str(i) + ".script"

    default_script_name = flp_path.gmat_default_script
    storage_script_name = flp_path.gmat_storage_future + gmat_sim_script_name
    storage_spk_file = flp_path.gmat_storage_future + gmat_sim_spk_name
    gmat_console_path = flp_path.gmat

    # copy GMAT default_script
    copy = "cp " + default_script_name + " " + storage_script_name
    subprocess.call(copy, shell=True)

    # compute needed values for usage in GMAT script
    style = 'DD Mon YYYY HR:MN:SC.###'
    et_fix = cspice.str2et(utc_fix)
    state_utc = cspice.timout(et_fix, style, 25)

    # insert state_new (and utc_fix) parameters in GMAT script
    file = open(storage_script_name)
    input = file.read()
    file.close()
    file = open(storage_script_name, 'w')
    output = re.sub(r'FLP_SC\.Epoch\s*=\s*\'Start_Time\'', "FLP_SC.Epoch = '" + state_utc + "'", input)
    output = re.sub(r'FLP_SC\.ElapsedSecs\s*=\s*\'Duration\'', "FLP_SC.ElapsedSecs = " + str(duration), output)
    output = re.sub(r'FLP_SPK\.Filename\s*=\s*\'Output_File_Path_Name\'',
                    "FLP_SPK.Filename = '" + storage_spk_file + "'", output)
    output = re.sub(r'FLP_SC\.X\s*=\s*\'X\'', "FLP_SC.X = " + str(state_new[0]), output)
    output = re.sub(r'FLP_SC\.Y\s*=\s*\'Y\'', "FLP_SC.Y = " + str(state_new[1]), output)
    output = re.sub(r'FLP_SC\.Z\s*=\s*\'Z\'', "FLP_SC.Z = " + str(state_new[2]), output)
    output = re.sub(r'FLP_SC\.VX\s*=\s*\'VX\'', "FLP_SC.VX = " + str(state_new[3]), output)
    output = re.sub(r'FLP_SC\.VY\s*=\s*\'VY\'', "FLP_SC.VY = " + str(state_new[4]), output)
    output = re.sub(r'FLP_SC\.VZ\s*=\s*\'VZ\'', "FLP_SC.VZ = " + str(state_new[5]), output)
    output = re.sub(r'FLP_SC\.DragArea\s*=\s*\'DRAG_AREA\'', "FLP_SC.DragArea = " + str(drag_area), output)
    file.write(output)
    file.close()

    # Run GMAT script
    os.chdir(gmat_console_path)
    gmat_script_command = './GmatConsole ' + storage_script_name + ' > /dev/null'
    subprocess.call(gmat_script_command, shell=True)

    return storage_spk_file, gmat_sim_spk_name


def error_calculator(et_test, storage_spk_file):

    state_gps_test = np.array(
        [cspice.spkezr('-513', et_test[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(et_test))])
    cspice.furnsh(storage_spk_file)  # loading order is important!!
    state_gmat_test = np.array(
        [cspice.spkezr('-513', et_test[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(et_test))])
    cspice.unload(storage_spk_file)

    # calculate the error (epsilon)
    square = []
    eps_new_tmp_x = []
    eps_new_tmp_y = []
    eps_new_tmp_z = []
    for k in range(len(et_test)):
        eps_new_tmp_x.append(np.abs(state_gmat_test[k, 0] - state_gps_test[k, 0]))
        eps_new_tmp_y.append(np.abs(state_gmat_test[k, 1] - state_gps_test[k, 1]))
        eps_new_tmp_z.append(np.abs(state_gmat_test[k, 2] - state_gps_test[k, 2]))

    eps_new_x = sum(eps_new_tmp_x)
    eps_new_y = sum(eps_new_tmp_y)
    eps_new_z = sum(eps_new_tmp_z)

    eps_new = [eps_new_x, eps_new_y, eps_new_z]

    return eps_new


def plot_gmat_comparison(spk2, ET, error_window):

    if error_window == 'on':
        ET_plot = []
        for item in ET:
            for subitem in item:
                ET_plot.append(subitem)
    else:
        if ET[-1][-1]-ET[0][0] < 100000:
            points = ET[-1][-1]-ET[0][0]
        else:
            points = 100000
        ET_plot = np.linspace(ET[0][0],ET[-1][-1], points)



    print('Start date plot: ' + cspice.et2utc(ET_plot[0], 'C', 0))
    print('End date plot: ' + cspice.et2utc(ET_plot[-1], 'C', 0))

    # load needed kernels and calculate the states
    flp_gps_plot = np.array(
        [cspice.spkezr('-513', ET_plot[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET_plot))])

    # Note: loading order is very important!!!!
    cspice.furnsh(spk2)
    flp_gmat_plot = np.array(
        [cspice.spkezr('-513', ET_plot[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET_plot))])
    cspice.unload(spk2)

    diff_pos = np.array([np.abs(flp_gps_plot[i, 0] - flp_gmat_plot[i, 0]) \
                                 + np.abs(flp_gps_plot[i, 1] - flp_gmat_plot[i, 1]) \
                                 + np.abs(flp_gps_plot[i, 2] - flp_gmat_plot[i, 2]) \
                         for i in range(len(flp_gps_plot))])

    diff_vel = np.array([np.abs(flp_gps_plot[i, 3] - flp_gmat_plot[i, 3]) \
                                 + np.abs(flp_gps_plot[i, 4] - flp_gmat_plot[i, 4]) \
                                 + np.abs(flp_gps_plot[i, 5] - flp_gmat_plot[i, 5]) \
                         for i in range(len(flp_gps_plot))])

    # general input to get nice labels
    style = 'YYYY-MM-DD-HR-MN-SC.###'
    time = []
    for item in ET_plot:
        time.append(cspice.timout(item, style, 23))
    timeline = []
    for i in range(len(time)):
        sub = time[i].replace(".", "-").split("-")
        timeline.append(datetime.datetime(int(sub[0]), int(sub[1]), int(sub[2]),
                                          int(sub[3]), int(sub[4]), int(sub[5]), int(sub[6]), tzinfo=None))

    # plot x position
    fig, ax = plt.subplots(figsize=(10, 7))
    plt.plot(timeline, flp_gps_plot[:, 0], label='gps spk x')
    plt.plot(timeline, flp_gmat_plot[:, 0], label='gmat spk x')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=2)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.28)
    plt.show()

    # plot x,y,z position difference and sart(sqaured) difference
    fig, ax = plt.subplots(figsize=(10, 7))
    plt.plot(timeline, flp_gps_plot[:, 0] - flp_gmat_plot[:, 0], label='diff x pos')
    plt.plot(timeline, flp_gps_plot[:, 1] - flp_gmat_plot[:, 1], label='diff y pos')
    plt.plot(timeline, flp_gps_plot[:, 2] - flp_gmat_plot[:, 2], label='diff z pos')
    plt.plot(timeline, diff_pos, '--', label='diff sum pos')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=4)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.28)
    plt.show()

    # plot x,y,z velocity difference
    fig, ax = plt.subplots(figsize=(10, 7))
    plt.plot(timeline, flp_gps_plot[:, 3] - flp_gmat_plot[:, 3], label='diff x vel')
    plt.plot(timeline, flp_gps_plot[:, 4] - flp_gmat_plot[:, 4], label='diff y vel')
    plt.plot(timeline, flp_gps_plot[:, 5] - flp_gmat_plot[:, 5], label='diff z vel')
    plt.plot(timeline, diff_vel, '--', label='diff sum vel')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=4)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.28)
    plt.show()


    # calculate vector length
    vec_len_gps = np.array(
        [np.sqrt(np.square(flp_gps_plot[k][0]) + np.square(flp_gps_plot[k][1]) + np.square(flp_gps_plot[k][2]))
         for k in range(len(flp_gps_plot))])

    vec_len_gmat = np.array(
        [np.sqrt(np.square(flp_gmat_plot[k][0]) + np.square(flp_gmat_plot[k][1]) + np.square(flp_gmat_plot[k][2]))
         for k in range(len(flp_gmat_plot))])

    fig, ax = plt.subplots(figsize=(10, 7))
    ax.set_ylabel('distance [km]', fontsize=18)
    plt.plot(timeline, vec_len_gps, label='spk gps', color='green')
    plt.plot(timeline, vec_len_gmat, label='spk gmat', color='red')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=3)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.28)
    plt.show()


def gmat_SPK_long_term(utc_prop, progress_end, drag_area, flp_path):

    print('\n GMAT SPK propagation for final file started... \n')

    # input paramter for GMAT script
    mod_state = progress_end[2]
    duration = cspice.str2et(utc_prop[1]) - cspice.str2et(utc_prop[0])

    # pathes
    gmat_sim_script_name = 'spk_prop_long_duration.script'
    default_script_name = flp_path.gmat_default_script
    storage_script_name = flp_path.gmat_storage_future + gmat_sim_script_name
    gmat_console_path = flp_path.gmat

    # copy GMAT default_script
    copy = "cp " + default_script_name + " " + storage_script_name
    subprocess.call(copy, shell=True)

    # compute needed values for usage in GMAT script
    style = 'DD Mon YYYY HR:MN:SC.###'
    state_utc = cspice.timout(cspice.str2et(utc_prop[0]), style, 25)

    # insert state_new (and utc_fix) parameters in GMAT script
    file = open(storage_script_name)
    input = file.read()
    file.close()
    file = open(storage_script_name, 'w')
    output = re.sub(r'FLP_SC\.Epoch\s*=\s*\'Start_Time\'', "FLP_SC.Epoch = '" + state_utc + "'", input)
    output = re.sub(r'FLP_SC\.ElapsedSecs\s*=\s*\'Duration\'', "FLP_SC.ElapsedSecs = " + str(duration), output)
    output = re.sub(r'FLP_SPK\.Filename\s*=\s*\'Output_File_Path_Name\'',
                    "FLP_SPK.Filename = '" + flp_path.spk_prop_name + "'", output)
    output = re.sub(r'FLP_SC\.X\s*=\s*\'X\'', "FLP_SC.X = " + str(mod_state[0]), output)
    output = re.sub(r'FLP_SC\.Y\s*=\s*\'Y\'', "FLP_SC.Y = " + str(mod_state[1]), output)
    output = re.sub(r'FLP_SC\.Z\s*=\s*\'Z\'', "FLP_SC.Z = " + str(mod_state[2]), output)
    output = re.sub(r'FLP_SC\.VX\s*=\s*\'VX\'', "FLP_SC.VX = " + str(mod_state[3]), output)
    output = re.sub(r'FLP_SC\.VY\s*=\s*\'VY\'', "FLP_SC.VY = " + str(mod_state[4]), output)
    output = re.sub(r'FLP_SC\.VZ\s*=\s*\'VZ\'', "FLP_SC.VZ = " + str(mod_state[5]), output)
    output = re.sub(r'FLP_SC\.DragArea\s*=\s*\'DRAG_AREA\'', "FLP_SC.DragArea = " + str(drag_area), output)
    file.write(output)
    file.close()

    # Run GMAT script
    os.chdir(gmat_console_path)
    gmat_script_command = './GmatConsole ' + storage_script_name + ' > /dev/null'
    subprocess.call(gmat_script_command, shell=True)


def create_ck_prop_furnsh(flp_path):
    kernels_to_load = "(\n\n" \
                      + "'" + flp_path.lsk_file + "', \n" \
                      + "'" + flp_path.spk_file + "', \n" \
                      + "'" + flp_path.pck_normal + "', \n" \
                      + "'" + flp_path.pck_itrf + "', " \
                      + "\n\n" \
                      + "'" + flp_path.sclk_file + "', \n" \
                      + "'" + flp_path.fk_file + "', \n" \
                      + "'" + flp_path.ik_file + "'," \
                      + "\n\n" \
                      + "'" + flp_path.fk_station_fk + "', \n" \
                      + "'" + flp_path.spk_station_spk + "'," \
                      + "\n\n"

    file = open(flp_path.ck_prop_furnsh, 'w')

    text = str(
        "\\begindata\n\n"
        "KERNELS_TO_LOAD = " + kernels_to_load
    )
    file.write(text)

    for spk_prop in sorted(os.listdir(flp_path.spk_prop)):
        if not spk_prop.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.spk_prop + spk_prop + "'", "\n\n"]))
    for fk_target in sorted(os.listdir(flp_path.fk_target)):
        if not fk_target.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.fk_target + fk_target + "'", "\n"]))
    for spk_target in sorted(os.listdir(flp_path.spk_target)):
        if not spk_target.endswith(".gitkeep"):
            file.write(','.join(["'" + flp_path.spk_target + spk_target + "'", "\n"]))

    file.write(")\n\n")
    file.write("\\begintext\n\n")
    file.close()


def create_ck_prop_idle(flp_path):
    # the function below is needed after we know where we want to point (for near future ck generation)
    create_ck_prop_idle_spec(flp_path)

    # Run the prediCkt utility
    ck_command_GPS = 'prediCkt -furnish ' + flp_path.ck_prop_furnsh + ' -spec ' + flp_path.ck_prop_idle_spec + \
                     ' -ck ' + flp_path.ck_prop_idle_ck + ' -tol 0.5 degrees -sclk ' + flp_path.sclk_file
    subprocess.call(ck_command_GPS, shell=True)


def create_ck_prop_idle_spec(flp_path):
    cover = cspice.utils.support_types.SPICEDOUBLE_CELL(1000)
    cspice.spkcov(flp_path.spk_prop_name, -513, cover)
    style = 'DD-MON-YYYY-HR:MN:SC.### ::TDB'  # TDB needed to convert to "right" time. Otherwise there is an offset of approx. 70 sec.
    time_start = cspice.timout(np.ceil(cover[0]), style, 30)
    time_end = cspice.timout(np.floor(cover[-1]), style, 30)

    file = open(flp_path.ck_prop_idle_spec, 'w')

    text = str(
        "\\begindata\n\n" +

        "DIRECTION_SPECS += ('TO_EARTH = POSITION OF EARTH -', \n" +
        "' FROM -513 -', \n" +
        "' CORRECTION NONE')\n\n"

        "DIRECTION_SPECS += ('TO_SUN = POSITION OF SUN -', \n" +
        "' FROM -513 -', \n" +
        "' CORRECTION NONE')\n\n\n"

        "ORIENTATION_NAME += 'IDLE'\n" +
        "PRIMARY += '-Z = TO_SUN'\n" +
        "SECONDARY += 'Y = TO_EARTH'\n" +
        "BASE_FRAME += 'J2000'\n\n\n" +

        "CK-513000ORIENTATION += 'IDLE'\n" +
        "CK-513000START +=@" + time_start + "\n" +
        "CK-513000STOP +=@" + time_end + "\n\n\n" +

        "CK-SCLK = 513\n" +
        "CK-SPK = -513\n" +
        "CK-FRAMES = -513000\n\n" +

        "\\begintext\n\n"
    )
    file.write(text)
    file.close()


def create_ck_prop_nadir(nadir_times, flp_path):
    # the function below is needed after we know where we want to point (for near future ck generation)
    create_ck_prop_nadir_spec(nadir_times, flp_path)

    # Run the prediCkt utility
    ck_command_GPS = 'prediCkt -furnish ' + flp_path.ck_prop_furnsh + ' -spec ' + flp_path.ck_prop_nadir_spec + \
                     ' -ck ' + flp_path.ck_prop_nadir_ck + ' -tol 0.001 degrees -sclk ' + flp_path.sclk_file
    subprocess.call(ck_command_GPS, shell=True)


def create_ck_prop_nadir_spec(nadir_times, flp_path):
    nadir_times_et = np.array([cspice.utc2et(nadir_times[i]) for i in range(len(nadir_times))])
    style = 'DD-MON-YYYY-HR:MN:SC.### ::TDB'
    time = np.array([cspice.timout(nadir_times_et[i], style, 30) for i in range(len(nadir_times_et))])

    file = open(flp_path.ck_prop_nadir_spec, 'w')

    text = str(
        "\\begindata\n\n" +

        "DIRECTION_SPECS += ('TO_EARTH = POSITION OF EARTH -', \n" +
        "' FROM -513 -', \n" +
        "' CORRECTION NONE')\n\n"

        "DIRECTION_SPECS += ('FLP_VEL = VELOCITY OF -513 -', \n" +
        "' FROM EARTH -', \n" +
        "' CORRECTION NONE')\n\n\n"

        "ORIENTATION_NAME += 'NADIR'\n" +
        "PRIMARY += 'Z = TO_EARTH'\n" +
        "SECONDARY += 'X = FLP_VEL'\n" +
        "BASE_FRAME += 'J2000'\n\n" +

        "CK-SCLK = 513\n" +
        "CK-SPK = -513\n" +
        "CK-FRAMES = -513000\n\n"
    )
    file.write(text)

    for i in range(0, len(time), 2):
        time_window = str(
            "CK-513000ORIENTATION += 'GOTO NADIR'\n" +
            "CK-513000START +=@" + time[i] + "\n" +
            "CK-513000STOP +=@" + time[i + 1] + "\n\n"
        )

        file.write(time_window)

    file.write("\\begintext\n\n")

    file.close()


def create_ck_prop_pass_all(target_p, degree, flp_path):
    # the function below is needed after we know where we want to point (for near future ck generation)
    create_ck_prop_pass_all_spec(target_p, degree, flp_path)

    # Run the prediCkt utility
    ck_command_GPS = 'prediCkt -furnish ' + flp_path.ck_prop_furnsh + ' -spec ' + flp_path.ck_prop_pass_all_spec + \
                     ' -ck ' + flp_path.ck_prop_pass_all_ck + ' -tol 0.01 degrees -sclk ' + flp_path.sclk_file
    subprocess.call(ck_command_GPS, shell=True)


def create_ck_prop_pass_all_spec(target_p, degree, flp_path):
    cspice.furnsh(flp_path.ck_prop_furnsh)  # needed to be loaded here for GF and other SPICE routines to work...

    # precomputations to get all parameters
    obsrvr = target_p  # it is called observer because of the gf routine, think of it as "target" [valid: targets or GS generated with pinpoint]
    abcorr = 'NONE'
    refval = degree * cspice.rpd()  # elevation [degree] (after conversion)
    target_ID = str(cspice.bodn2c(obsrvr))  # ID code for target
    frame_obsrvr_pool = 'FRAME_1' + target_ID + '_NAME'  # this will work only for pinpoint generated tf files.
    frame_obsrvr = cspice.gcpool(frame_obsrvr_pool, 0, 1)[0]  # frame name of target
    crdsys = 'LATITUDINAL'
    coord = 'LATITUDE'
    relate = '>'
    adjust = 0
    step = 20 * 60  # stepsize [min]
    nintvls = 7500
    result = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
    cnfine = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
    # get the start and stop times of the SPK propagation kernel
    cover = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
    cspice.spkcov(flp_path.spk_prop_name, -513, cover)
    et = [cover[0] + 1, cover[-1] - 1]  # 1 second margin as it somehow did not work without that margin...
    cspice.appndd(et, cnfine)

    # compute gf search
    # elevation above x [degrees]
    cspice.gfposc('FLP', frame_obsrvr, abcorr, obsrvr, crdsys, coord, relate, refval, adjust, step, nintvls, cnfine,
                  result)
    count = cspice.wncard(result)
    print("Number of result windows: " + str(count))

    style = 'DD-MON-YYYY-HR:MN:SC.### ::TDB'  # TDB needed to convert to "right" time. Otherwise there is an offset of approx. 70 sec.
    time = []
    for k in range(count):
        # get pass time
        et_start, et_stop = cspice.wnfetd(result, k)
        time.append(cspice.timout(et_start, style, 30))
        time.append(cspice.timout(et_stop, style, 30))

    file = open(flp_path.ck_prop_pass_all_spec, 'w')

    text = str(
        "\\begindata\n\n" +

        "DIRECTION_SPECS += ('TO_" + target_p + " = POSITION OF " + target_p + " -', \n" +
        "' FROM -513 -', \n" +
        "' CORRECTION NONE')\n\n"

        "DIRECTION_SPECS += ('FLP_VEL = VELOCITY OF -513 -', \n" +
        "' FROM EARTH -', \n" +
        "' CORRECTION NONE')\n\n"

        "ORIENTATION_NAME += '" + target_p + "'\n" +
        "PRIMARY += 'Z = TO_" + target_p + "'\n" +
        "SECONDARY += 'X = FLP_VEL'\n" +
        "BASE_FRAME += 'J2000'\n\n\n" +

        "CK-SCLK = 513\n" +
        "CK-SPK = -513\n" +
        "CK-FRAMES = -513000\n\n"
    )
    file.write(text)

    for i in range(0, len(time), 2):
        time_window = str(
            "CK-513000ORIENTATION += 'GOTO " + target_p + "'\n" +
            "CK-513000START +=@" + time[i] + "\n" +
            "CK-513000STOP +=@" + time[i + 1] + "\n\n"
        )

        file.write(time_window)

    file.write("\\begintext\n\n")
    file.close()


def create_ck_prop_moon_pointing(moon_pointing_times, flp_path):
    # the function below is needed after we know where we want to point (for near future ck generation)
    create_ck_prop_moon_pointing_spec(moon_pointing_times, flp_path)

    # Run the prediCkt utility
    ck_command_GPS = 'prediCkt -furnish ' + flp_path.ck_prop_furnsh + ' -spec ' + flp_path.ck_prop_moon_pointing_spec + \
                     ' -ck ' + flp_path.ck_prop_moon_pointing_ck + ' -tol 0.001 degrees -sclk ' + flp_path.sclk_file
    subprocess.call(ck_command_GPS, shell=True)


def create_ck_prop_moon_pointing_spec(moon_pointing_times, flp_path):
    moon_pointing_times_et = np.array([cspice.utc2et(moon_pointing_times[i]) for i in range(len(moon_pointing_times))])
    style = 'DD-MON-YYYY-HR:MN:SC.### ::TDB'
    time = np.array([cspice.timout(moon_pointing_times_et[i], style, 30) for i in range(len(moon_pointing_times_et))])

    file = open(flp_path.ck_prop_moon_pointing_spec, 'w')

    text = str(
        "\\begindata\n\n" +

        "DIRECTION_SPECS += ('TO_MOON = POSITION OF MOON -', \n" +
        "' FROM -513 -', \n" +
        "' CORRECTION NONE')\n\n"

        "DIRECTION_SPECS += ('TO_SUN = POSITION OF SUN -', \n" +
        "' FROM -513 -', \n" +
        "' CORRECTION NONE')\n\n\n"

        "ORIENTATION_NAME += 'MOON_POINTING'\n" +
        "PRIMARY += 'Z = TO_MOON'\n" +
        "SECONDARY += 'Y = TO_SUN'\n" +
        "BASE_FRAME += 'J2000'\n\n" +

        "CK-SCLK = 513\n" +
        "CK-SPK = -513\n" +
        "CK-FRAMES = -513000\n\n"
    )
    file.write(text)

    for i in range(0, len(time), 2):
        time_window = str(
            "CK-513000ORIENTATION += 'GOTO MOON_POINTING'\n" +
            "CK-513000START +=@" + time[i] + "\n" +
            "CK-513000STOP +=@" + time[i + 1] + "\n\n"
        )

        file.write(time_window)

    file.write("\\begintext\n\n")

    file.close()


def create_ck_prop_nadir_offset(nadir_offset_values, flp_path):
    # the function below is needed after we know where we want to point (for near future ck generation)
    create_ck_prop_nadir_offset_spec(nadir_offset_values, flp_path)

    # Run the prediCkt utility
    ck_command_GPS = 'prediCkt -furnish ' + flp_path.ck_prop_furnsh + ' -spec ' + flp_path.ck_prop_nadir_offset_spec + \
                     ' -ck ' + flp_path.ck_prop_nadir_offset_ck + ' -tol 0.001 degrees -sclk ' + flp_path.sclk_file
    subprocess.call(ck_command_GPS, shell=True)


def create_ck_prop_nadir_offset_spec(nadir_offset_values, flp_path):
    nadir_offset_times_et = []
    nadir_offset_angles = []
    for i in range(len(nadir_offset_values)):
        if (i + 1) % 3 != 0:
            nadir_offset_times_et.append(cspice.utc2et(nadir_offset_values[i]))
        else:
            nadir_offset_angles.append(nadir_offset_values[i])

    style = 'DD-MON-YYYY-HR:MN:SC.### ::TDB'
    time = np.array([cspice.timout(nadir_offset_times_et[i], style, 30) for i in range(len(nadir_offset_times_et))])

    file = open(flp_path.ck_prop_nadir_offset_spec, 'w')

    text = str(
        "\\begindata\n\n" +

        "DIRECTION_SPECS += ('TO_EARTH = POSITION OF EARTH -', \n" +
        "' FROM -513 -', \n" +
        "' CORRECTION NONE')\n\n"

        "DIRECTION_SPECS += ('FLP_VEL = VELOCITY OF -513 -', \n" +
        "' FROM EARTH -', \n" +
        "' CORRECTION NONE')\n\n\n"
    )
    file.write(text)

    for i in range(len(nadir_offset_angles)):
        dir_spec = str(
            "DIRECTION_SPECS += ('PositionAngle_" + str(i) + " = ROTATE TO_EARTH -', \n" +
            "' " + str(nadir_offset_angles[i]) + " degrees ABOUT - ', \n" +
            "' FLP_VEL' ) \n\n" +

            "ORIENTATION_NAME += 'NADIR_OFFSET_" + str(i) + "'\n" +
            "PRIMARY += 'Z = PositionAngle_" + str(i) + "'\n" +
            "SECONDARY += 'Y = FLP_VEL'\n" +
            "BASE_FRAME += 'J2000'\n\n"

            "CK-513000ORIENTATION += 'GOTO NADIR_OFFSET_" + str(i) + "'\n" +
            "CK-513000START +=@" + time[i * 2] + "\n" +
            "CK-513000STOP +=@" + time[i * 2 + 1] + "\n\n\n"
        )
        file.write(dir_spec)

    text_2 = str(
        "CK-SCLK = 513\n" +
        "CK-SPK = -513\n" +
        "CK-FRAMES = -513000\n\n"
    )
    file.write(text_2)

    file.write("\\begintext\n\n")

    file.close()


def delete_inputs_2(flp_path):
    remove_old_spk_source = "rm -f " + flp_path.ck_prop + "*"  # delete kernels/ck/propagation/ files...
    subprocess.call(remove_old_spk_source, shell=True)


def move_new_to_old(flp_path):
    file = os.listdir(flp_path.json_obs_new)
    # there should be only 1 file
    if file != []:
        move_json_obs = 'mv ' + flp_path.json_obs_new + file[0] + ' ' + flp_path.json_obs_old + file[0]
        subprocess.call(move_json_obs, shell=True)


def create_json_observation(observation_name, sensor, start, end, obs_rate, fill, flp_path):

    footprint_color = [1, 0.5, 0]

    groups = OrderedDict([('startTime', start + ' UTC'),
                            ('endTime', end + ' UTC'),
                            ('obsRate', obs_rate)])
    geometry = OrderedDict([('type', 'Observations'),
                            ('sensor', sensor),
                            ('groups', [groups]),
                            ('footprintColor', footprint_color),
                            ('footprintOpacity', 0.4),
                            ('showResWithColor', True),
                            ('sideDivisions', 125),
                            ('alongTrackDivisions', 500),
                            ('shadowVolumeScaleFactor', 1.75),
                            ('fillInObservations', fill)])
    bodyFrame = OrderedDict([('type', 'BodyFixed'),
                             ('body', "Earth")])
    trajectoryFrame = OrderedDict([('type', 'BodyFixed'),
                                   ('body', "Earth")])
    items = OrderedDict([('class', "observation"),
                         ('name', observation_name),
                         ('center', "Earth"),
                         ('trajectoryFrame', trajectoryFrame),
                         ('bodyFrame', bodyFrame),
                         ('geometry', geometry)])
    json_data = OrderedDict([('version', '1.0'),
                             ('name', observation_name),
                             ('items', [items])])

    json_path = flp_path.json_obs_new + observation_name + ".json"
    with open(json_path, 'w') as outfile:
        json.dump(json_data, outfile, indent=3)


def update_scenario_json(exclude_targets, exclude_stations, exclude_old_obs, flp_path):

    items = [
        "./metakernels.json",
        "./spacecraft_FLP.json",
        "./sensors/FLP_PAMCAM.json",
        "./sensors/FLP_OSIRIS.json"
    ]

    if exclude_stations != 'on':
        items.append("./stations.json")

    if exclude_targets != 'on':
        target_item_new = os.listdir(flp_path.json_target_new)
        items.append("./targets/new/" + target_item_new[0])
        target_items_old = os.listdir(flp_path.json_target_old)
        for i in range(len(target_items_old)):
            items.append("./targets/old/" + target_items_old[i])

    if exclude_old_obs != 'on':
        obs_items_old = os.listdir(flp_path.json_obs_old)
        for i in range(len(obs_items_old)):
            items.append("./observations/old/" + obs_items_old[i])

    obs_item_new = os.listdir(flp_path.json_obs_new)
    items.append("./observations/new/" + obs_item_new[0])  # there should only be 1 new item

    json_data = OrderedDict([('version', '1.0'),
                        ('name', 'load FLP'),
                        ('require',
                            items
                        )])

    json_path = flp_path.json_scenario
    with open(json_path, 'w') as outfile:
        json.dump(json_data, outfile, indent=3)


def cosmo_earth_func(utc, flp_path):
    cspice.furnsh(flp_path.mk_out)
    et = cspice.str2et(utc)
    jd = str(cspice.j2000() + et / cspice.spd())
    cosmo_launch = "./Cosmographia.sh"+" -style=gtk"

    # center, select, frame, jd, cam_x, cam_y, cam_z, cam_qw, cam_qx, cam_qy, cam_qz, ts, cam_fov
    val = ['Earth', 'Earth', 'itrf', jd , '0.0', '0.0', '20000', '1.0', '0.0', '0.0', '0.0', '0', '40']
    cosmo_state = "'cosmo:" + val[0] + "?&select=" + val[1] + "&frame=" + val[2] + "&jd=" + val[3] + "&x=" + \
                        val[4] + "&y=" + val[5] + "&z=" + val[6] + "&qw=" + val[7] + "&qx=" + val[8] + "&qy=" + val[9] + \
                        "&qz=" + val[10] + "&ts=" + val[11] + "&fov=" + val[12] + "'"

    cosmographia_command = cosmo_launch + " -u " + cosmo_state + " -p " + flp_path.cosmo_script_earth + " " + flp_path.cosmo_json
    os.chdir(flp_path.cosmographia)
    subprocess.call(cosmographia_command, shell=True)


def get_fov_angle(sensor):
    # always use DEGREE
    sensor_ID = str(cspice.bodn2c(sensor))
    in_ref_angle = 'INS' + sensor_ID + '_FOV_REF_ANGLE'
    in_cross_angle = 'INS' + sensor_ID + '_FOV_CROSS_ANGLE'
    ref_angle_temp = cspice.gdpool(in_ref_angle, 0, 1)
    cross_angle_temp = cspice.gdpool(in_cross_angle, 0, 1)
    ref_angle = ref_angle_temp[0]
    cross_angle = cross_angle_temp[0]
    if ref_angle >= cross_angle:
        FOV_angle = ref_angle * 2.5  # multiply by 2 to get a nice fit of the FOV in the cosmographia screenshot
    else:
        FOV_angle = cross_angle * 2.5

    return FOV_angle


def cosmoscript_sensor(sensor,flp_path):

    file = open(flp_path.cosmo_script_sensor, 'w')

    text = str( 'import cosmoscripting' + '\n\n' + \
                'cosmo = cosmoscripting.Cosmo()' + '\n\n' + \
                'cosmo.hideTrajectory("Moon")' + '\n' + \
                'cosmo.hideTrajectory("Mercury")' + '\n' + \
                'cosmo.hideTrajectory("Venus")' + '\n' + \
                'cosmo.hideTrajectory("Earth")' + '\n' + \
                'cosmo.hideTrajectory("Mars")' + '\n' + \
                'cosmo.hideTrajectory("Jupiter")' + '\n' + \
                'cosmo.hideTrajectory("Saturn")' + '\n' + \
                'cosmo.hideTrajectory("Uranus")' + '\n' + \
                'cosmo.hideTrajectory("Neptune")' + '\n\n' + \
                'cosmo.showObject("' + sensor + '")'
                )
    file.write(text)


def update_scenario_json_sensor(exclude_old_targets, exclude_stations, sensor, flp_path):

    items = [
        "./metakernels.json",
        "./spacecraft_FLP.json",
        "./sensors/" + sensor + ".json"
    ]

    if exclude_stations != 'on':
        items.append("./stations.json")

    if exclude_old_targets != 'on':
        target_items_old = os.listdir(flp_path.json_target_old)
        for i in range(len(target_items_old)):
            items.append("./targets/old/" + target_items_old[i])


    obs_item_new = os.listdir(flp_path.json_target_new)
    items.append("./targets/new/" + obs_item_new[0])  # there should only be 1 new item

    json_data = OrderedDict([('version', '1.0'),
                        ('name', 'load FLP'),
                        ('require',
                            items
                        )])

    json_path = flp_path.json_scenario
    with open(json_path, 'w') as outfile:
        json.dump(json_data, outfile, indent=3)


def launch_cosmographia(start, FOV_angle, sensor, flp_path):

    et = cspice.str2et(start)
    jd = str(cspice.j2000() + et / cspice.spd())

    cosmo_launch = "./Cosmographia.sh"+" -style=gtk"

    # center, select, frame, jd, cam_x, cam_y, cam_z, cam_qw, cam_qx, cam_qy, cam_qz, ts, cam_fov
    val = [sensor, 'FLP', 'bfix', jd , '0.0', '0.0', '0.1', '0.0', '0.0', '1.0', '0.0', '0', str(FOV_angle)]

    cosmo_state = "'cosmo:" + val[0] + "?&select=" + val[1] + "&frame=" + val[2] + "&jd=" + val[3] + "&x=" + \
                        val[4] + "&y=" + val[5] + "&z=" + val[6] + "&qw=" + val[7] + "&qx=" + val[8] + "&qy=" + val[9] + \
                        "&qz=" + val[10] + "&ts=" + val[11] + "&fov=" + val[12] + "'"

    cosmographia_command = cosmo_launch + " -u " + cosmo_state + " -p " + flp_path.cosmo_script_sensor + " " + flp_path.cosmo_json

    os.chdir(flp_path.cosmographia)
    subprocess.call(cosmographia_command, shell=True)


def cosmo_sensor_func(sensor,exclude_targets, exclude_stations, start, flp_path):
    FOV_angle = get_fov_angle(sensor)
    cosmoscript_sensor(sensor)
    update_scenario_json_sensor(exclude_targets, exclude_stations, sensor)
    launch_cosmographia(start, FOV_angle, sensor)


def move_new_to_old_2(flp_path):
    file = os.listdir(flp_path.json_target_new)
    # there should be only 1 file
    if file != []:
        move_json_target = 'mv ' + flp_path.json_target_new + file[0] + ' ' + flp_path.json_target_old + file[0]
        subprocess.call(move_json_target, shell=True)


def create_tf_setup(target_name,target_lat,target_lon,target_alt, flp_path):

    # get the next free naif ID code. This is critical!
    ID_code_file = open(flp_path.spk_target_id_code, 'r')
    ID_code_old = ID_code_file.read()
    ID_code_new = int(ID_code_old)+1
    ID_code_file.close()
    ID_code_file = open(flp_path.spk_target_id_code, 'w')
    ID_code_file.write(str(ID_code_new))
    ID_code_file.close()

    # write pinpoint input file
    file = open(flp_path.spk_target_setup, 'w')

    text = str(
        "\\begindata\n\n" +

        "SITES                      += ( '" + target_name + "' )\n" +
        target_name + "_CENTER       = 399 \n" +
        target_name + "_FRAME        = 'ITRF93' \n" +
        target_name + "_IDCODE       = " + str(ID_code_new) + " \n" +
        target_name + "_LATLON       = ( " + str(target_lat) + ' ' + str(target_lon) + ' ' + str(target_alt) + " ) \n" +
        target_name + "_BOUNDS       = (@2017-JAN-1, @2020-JAN-1) \n" +
        target_name + "_UP       = 'Z' \n" +
        target_name + "_NORTH      = 'X' \n\n" +

        "\\begintext\n\n"
    )
    file.write(text)
    file.close()


def create_target_pinpoint(target_name, flp_path):
    # Run the pinpoint utility
    station_command = 'pinpoint ' + '-def ' + flp_path.spk_target_setup + ' -pck ' + flp_path.pck_normal + \
                 ' -spk ' + flp_path.spk_target + target_name + '.bsp' + ' -fk ' + flp_path.fk_target + target_name + '.tf'
    subprocess.call(station_command, shell=True)


def create_json_target(target_name, flp_path):

    trajectory = OrderedDict([('type', 'Spice'),
                              ('target', target_name),
                              ('center', 'Earth')])
    items = OrderedDict([('name', target_name),
                         ('center', 'Earth'),
                         ('trajectory', trajectory)])
    json_data = OrderedDict([('version', '1.0'),
                             ('name', target_name),
                             ('items', [items])])

    json_path = flp_path.json_target_new + target_name + ".json"
    with open(json_path, 'w') as outfile:
        json.dump(json_data, outfile, indent=3)


def update_scenario_json_2(exclude_old_targets, exclude_station, flp_path):

    items = [
        "./metakernels.json",
        "./spacecraft_FLP.json",
        "./sensors/FLP_PAMCAM.json",
        "./sensors/FLP_OSIRIS.json"
    ]
    if exclude_station != 'on':
        items.append("./stations.json")

    if exclude_old_targets != 'on':
        target_items_old = os.listdir(flp_path.json_target_old)
        for i in range(len(target_items_old)):
            items.append("./targets/old/" + target_items_old[i])

    target_item_new = os.listdir(flp_path.json_target_new)
    items.append("./targets/new/" + target_item_new[0])  # there should only be 1 new item

    json_data = OrderedDict([('version', '1.0'),
                        ('name', 'load FLP'),
                        ('require',
                            items
                        )])

    json_path = flp_path.json_scenario
    with open(json_path, 'w') as outfile:
        json.dump(json_data, outfile, indent=3)


def get_FOV_edge_vectors(FLP_INS_id, mid_points):
    """
    :param instrument_id: NAIF id of instrument
    :param mid_points: Number of points sampled on each side of rectangular FOV between the corners.
    :return: Ordered list of vectors which lie along the FOV rectangle edge.
    """
    if mid_points < 0:
        raise RuntimeError("Number of mid points must be positive.")
    # get the FOV corner vectors from SPICE
    ik_output = cspice.getfov(FLP_INS_id, 10)
    fov_shape = ik_output[0]

    if fov_shape != "RECTANGLE" and fov_shape != "CIRCLE":
        raise RuntimeError("Non-rectangular and non-circular FOVs not supported.")

    if fov_shape == "CIRCLE":

        vec = ik_output[4][0]
        circle_radius = vec[1]
        circle_boresight_len = vec[2]
        fov_edge_vectors = []
        n_fractions = mid_points + 1  # how precise you want to get the circle
        angle = np.linspace(0, 2*np.pi, n_fractions)

        for i in range(n_fractions):
            fov_edge_vectors.append((np.sin(angle[i])*circle_radius, np.cos(angle[i])*circle_radius, circle_boresight_len))
        return fov_edge_vectors, fov_shape

    if fov_shape == "RECTANGLE":
        corner_vector = ik_output[4]

        # compute mid-point vectors between each two adjacent corner pair
        fov_edge_vectors = []

        for i in range(mid_points):  # first side
            vec_1 = corner_vector[0][0]
            vec_2 = corner_vector[0][1] - (corner_vector[0][1] - corner_vector[1][1])*i/mid_points
            vec_3 = corner_vector[0][2]
            fov_edge_vectors.append((vec_1, vec_2, vec_3))
        for i in range(mid_points):  # second side
            vec_1 = corner_vector[1][0] - (corner_vector[1][0] - corner_vector[2][0])*i/mid_points
            vec_2 = corner_vector[1][1]
            vec_3 = corner_vector[1][2]
            fov_edge_vectors.append((vec_1, vec_2, vec_3))
        for i in range(mid_points):  # third side
            vec_1 = corner_vector[2][0]
            vec_2 = corner_vector[2][1] - (corner_vector[2][1] - corner_vector[3][1])*i/mid_points
            vec_3 = corner_vector[2][2]
            fov_edge_vectors.append((vec_1, vec_2, vec_3))
        for i in range(mid_points):  # fourth side
            vec_1 = corner_vector[3][0] - (corner_vector[3][0] - corner_vector[0][0])*i/mid_points
            vec_2 = corner_vector[3][1]
            vec_3 = corner_vector[3][2]
            fov_edge_vectors.append((vec_1, vec_2, vec_3))

        return fov_edge_vectors, fov_shape


def main_fov_basemap(start_time, instrument_name,n_time_points,duration,mid_points, revert_limb, figsize_1, figsize_2, end_time, fill, flp_path):

    FLP_INS_id = cspice.bodn2c(instrument_name)

    start_et = cspice.str2et(start_time)
    # linearly sample the desired time window
    ets = np.linspace(start_et, start_et + duration, num=n_time_points)

    FOV_edge_vectors, fov_shape = get_FOV_edge_vectors(FLP_INS_id, mid_points=mid_points)
    # now we will project the FOV at each time point onto Earth's surface in CEA projection

    lon_points = []
    lat_points = []
    n, radii = cspice.bodvrd("EARTH", "RADII", 3)
    flat = (radii[0]-radii[2])/radii[0]

    for et in ets:
        # intercept each FOV edge vector with Earth's surface
        # sincpt returns multiple outputs, we care about the first one which is coordinates,
        # of the intercepted surface point in ITRF93 frame, hence the [0]
        # This will throw a SpiceError if any of the vectors fails to intercept the surface

        #---- this needs to be changed to a proper error check----
        projected_FOV_edge_vectors = []

        try:
            for i in range(len(FOV_edge_vectors)):
                projected_FOV_edge_vectors.append(cspice.sincpt("ELLIPSOID", "EARTH", et, "ITRF93",
                                                 "NONE", "FLP", cspice.bodc2n(FLP_INS_id), FOV_edge_vectors[i])[0])
        except:

            projected_FOV_edge_vectors = exception_calculation(FOV_edge_vectors, et, FLP_INS_id, revert_limb, instrument_name)


        #---------------------------------------------------------

        # transform the coordinates to planetographic coordinates (0, 2 pi)
        surface_lon_lat_radian = [cspice.recpgr('EARTH',rectan,radii[0],flat) for rectan in projected_FOV_edge_vectors]
        # change from radians to degrees and omit the altitude (which is normally zero) (0-360)
        surface_lon_degree = [cspice.dpr()*lon_lat_radian[0] for lon_lat_radian in surface_lon_lat_radian]
        surface_lat_degree = [cspice.dpr() * lon_lat_radian[1] for lon_lat_radian in surface_lon_lat_radian]
        lon_points.append(surface_lon_degree)
        lat_points.append(surface_lat_degree)

    plot_1(lon_points, lat_points, figsize_1, figsize_2, start_time, end_time, instrument_name, n_time_points, fill, flp_path)


def plot_1(lon_points, lat_points, figsize_1, figsize_2, start_time, end_time, instrument_name, n_time_points, fill, flp_path):
    # now we have all the projections, we plot them one by one in blue
    fig, ax = plt.subplots(figsize=(figsize_1, figsize_2))

    # this is all done to automatically know the size of the basemap to include...
    # 1. get the min and max values for longitude and latitude
    # 2. plot them
    # 3. get the default axis limit values from matplotlib as input for the basemap...
    for i in range(len(lon_points)):
        for k in range(len(lon_points[i])):
            if lon_points[i][k] > 180:
                lon_points[i][k] = lon_points[i][k] - 360

    x_min = min(lon_points[0])
    x_max = max(lon_points[0])
    y_min = min(lat_points[0])
    y_max = max(lat_points[0])
    for i in range(len(lon_points)):
        if max(lon_points[i]) > x_max:
            x_max = max(lon_points[i])
        if min(lon_points[i]) < x_min:
            x_min = min(lon_points[i])
        if max(lat_points[i]) > y_max:
            y_max = max(lat_points[i])
        if min(lat_points[i]) < y_min:
            y_min = min(lat_points[i])
    plt.plot(x_min,y_min)
    plt.plot(x_max,y_max)
    ymin, ymax = ax.get_ylim()
    xmin, xmax = ax.get_xlim()

    # generate the basemap
    map = Basemap(projection='merc', resolution='h',
                  llcrnrlon=xmin, llcrnrlat=ymin,
                  urcrnrlon=xmax, urcrnrlat=ymax)
    map.drawcoastlines()
    map.drawcountries()
    map.drawmapboundary(fill_color='aqua')
    map.fillcontinents(color='coral',lake_color='aqua')

    # plot the FOV intersection into the Basemap
    for i in range(len(lon_points)):
        FOV = []
        for k in range(len(lon_points[i])):
            FOV.append(map(lon_points[i][k], lat_points[i][k]))
        poly = Polygon(FOV, color='blue', alpha=0.6, fill=fill)
        plt.gca().add_patch(poly)

    # draw stations
    x_station, y_station = map(343.4882, 28.3009)  # GS1
    map.plot(x_station, y_station, marker='.', color='red')
    x_station, y_station = map(210.506, 64.2008)  # HAM_Radio, HAM
    map.plot(x_station, y_station, marker='.', color='red')
    x_station, y_station = map(226.456, 68.3183)  # Inuvik, INU
    map.plot(x_station, y_station, marker='.', color='red')
    x_station, y_station = map(9.10384, 48.7495)  # Stuttgart, IRS
    map.plot(x_station, y_station, marker='.', color='red')
    x_station, y_station = map(10.1818, 54.2629)  # Kiel, KIE
    map.plot(x_station, y_station, marker='.', color='red')
    x_station, y_station = map(101.719, 2.9906)  # Malaysia, MAL
    map.plot(x_station, y_station, marker='.', color='red')
    x_station, y_station = map(11.8835, 78.9277)  # NyAlesund, NYA
    map.plot(x_station, y_station, marker='.', color='red')
    x_station, y_station = map(302.0992, -63.3211)  # OHiggins, OHG
    map.plot(x_station, y_station, marker='.', color='red')
    x_station, y_station = map(11.2781, 48.0847)  # Oberpfaffenhofen, OPH
    map.plot(x_station, y_station, marker='.', color='red')
    x_station, y_station = map(11.0853, 47.8801)  # Weilheim, WHM
    map.plot(x_station, y_station, marker='.', color='red')

    # draw parallels and meridians.
    # label parallels on right and top
    # meridians on bottom and left
    parallels = np.arange(-90, 90, 1)
    # labels = [left,right,top,bottom]
    map.drawparallels(parallels, labels=[False, True, True, False])
    meridians = np.arange(0, 360, 1)
    map.drawmeridians(meridians, labels=[True, False, False, True])

    # Title and plot
    plt.title(str(start_time) + " - " + str(end_time) + " , " + str(n_time_points) + " points")
    plot_name = start_time.split(".")[0].replace("-", "_").replace(":", "")
    fig.savefig(flp_path.plots + 'basemap_' + instrument_name + '_' + plot_name + '.png', dpi=fig.dpi)
    plt.show()


def exception_calculation(FOV_edge_vectors, et, FLP_INS_id, revert_limb, instrument_name):

    counter = 0
    for p in range(len(FOV_edge_vectors)):
        try:
            cspice.sincpt("ELLIPSOID", "EARTH", et, "ITRF93", "NONE", "FLP", cspice.bodc2n(FLP_INS_id), FOV_edge_vectors[p])[0]
        except:
            continue
        else:
            counter = 1
            print(p)
            break

    if counter == 1:
        print('Some piece of the Earth is inside the instrument FOV')
    else:
        print('The Earth is not inside the FOV of the specified instrument')
        quit()

    fov_edge_1 = []
    fov_edge_2 = []
    try:
        for cut_forward in range(p,len(FOV_edge_vectors)):
            fov_edge_1.append(cspice.sincpt("ELLIPSOID", "EARTH", et, "ITRF93",
                                            "NONE", "FLP", cspice.bodc2n(FLP_INS_id), FOV_edge_vectors[cut_forward])[0])
    except:
        print(cut_forward)

    if p == 0:
        try:
            for cut_backward in reversed(range(p,len(FOV_edge_vectors))):
                fov_edge_2.append(cspice.sincpt("ELLIPSOID", "EARTH", et, "ITRF93",
                                                "NONE", "FLP", cspice.bodc2n(FLP_INS_id), FOV_edge_vectors[cut_backward])[0])
        except:
            print(cut_backward)

        fov_2_1 = FOV_edge_vectors[cut_backward + 1]  # last working one
        fov_2_2 = FOV_edge_vectors[cut_backward]  # first not working one

    fov_1_1 = FOV_edge_vectors[cut_forward-1]  # last working one
    fov_1_2 = FOV_edge_vectors[cut_forward]  # first not working one


    for g in range(10):

        n = 20
        fov_x_1 = np.linspace(fov_1_1[0], fov_1_2[0], n)
        fov_y_1 = np.linspace(fov_1_1[1], fov_1_2[1], n)
        fov_z_1 = np.linspace(fov_1_1[2], fov_1_2[2], n)
        vec_1 = []
        for i in range(n):
            vec_1.append([fov_x_1[i], fov_y_1[i], fov_z_1[i]])
        try:
            for m in range(n):
                fov_edge_1.append(cspice.sincpt("ELLIPSOID", "EARTH", et, "ITRF93",
                                                "NONE", "FLP", cspice.bodc2n(FLP_INS_id), vec_1[m])[0])
        except:
            fov_1_1 = vec_1[m - 1]
            fov_1_2 = vec_1[m]

        if p == 0:
            fov_x_2 = np.linspace(fov_2_2[0], fov_2_1[0], n)
            fov_y_2 = np.linspace(fov_2_2[1], fov_2_1[1], n)
            fov_z_2 = np.linspace(fov_2_2[2], fov_2_1[2], n)
            vec_2 = []
            for i in range(n):
                vec_2.append([fov_x_2[i], fov_y_2[i], fov_z_2[i]])
            try:
                for f in reversed(range(n)):
                    fov_edge_2.append(cspice.sincpt("ELLIPSOID", "EARTH", et, "ITRF93",
                                                   "NONE", "FLP", cspice.bodc2n(FLP_INS_id), vec_2[f])[0])
            except:
                fov_2_1 = vec_2[f + 1]
                fov_2_2 = vec_2[f]


    limb_fov_list = limb_fov(et, instrument_name)

    if revert_limb == 'on':
        projected_FOV_edge_vectors = fov_edge_1 + list(reversed(limb_fov_list)) + list(reversed(fov_edge_2))
    else:
        projected_FOV_edge_vectors = fov_edge_1 + list(limb_fov_list) + list(reversed(fov_edge_2))

    return projected_FOV_edge_vectors


def limb_fov(et, instrument_name):
    limb_fov_list = []

    method = 'TANGENT/ELLIPSOID'
    target = 'EARTH'
    fixref = 'ITRF93'
    abcorr = 'NONE'
    corloc = 'CENTER'
    obsrvr = 'FLP'
    refvec = [0, 0, 1]
    rolstp = 0.01 * cspice.rpd()
    ncuts = 36000
    schstp = 10
    soltol = 1
    maxn = 100000
    points = \
        cspice.limbpt(method, target, et, fixref, abcorr, corloc, obsrvr, refvec, rolstp, ncuts, schstp, soltol, maxn)[
            1]

    pos = cspice.spkpos('FLP', et, 'J2000', 'NONE', 'EARTH')[0]
    raydir = np.array([item - pos for item in points])
    inst = instrument_name
    rframe = 'J2000'
    abcorr = 'NONE'
    observer = 'FLP'
    visible = np.array([cspice.fovray(inst, item, rframe, abcorr, observer, et) for item in raydir])
    for k in range(len(raydir)):
        if visible[k] == True:
            limb_fov_list.append(raydir[k])

    return limb_fov_list


def create_tle_setup(flp_path):
    # write mkspk setup file
    file = open(flp_path.spk_setup_tle, 'w')

    text = str(
        "\\begindata\n\n"
        "INPUT_DATA_TYPE = 'TL_ELEMENTS'" + "\n" +
        "OUTPUT_SPK_TYPE = 10" + "\n" +
        "TLE_INPUT_OBJ_ID = 20580" + "\n" +
        "TLE_SPK_OBJ_ID = -48" + "\n" +
        "CENTER_ID = 399" + "\n" +
        "REF_FRAME_NAME = 'J2000'" + "\n" +
        "TLE_START_PAD = '2 days'" + "\n" +
        "TLE_STOP_PAD = '2 days'" + "\n" +
        "LEAPSECONDS_FILE = '" + flp_path.lsk_file + "'\n" +
        #"INPUT_DATA_FILE = './tle_input/FLP.tle'\n" +
        #"OUTPUT_SPK_FILE = '" + flp_path.spk_out_name_tle + "'\n" +
        "PCK_FILE = '" + flp_path.pck_geo + "' \n" +
        "SEGMENT_ID = 'SEGMENT X'" + "\n" +  # how should i name it?
        "PRODUCER_ID = 'IRS'" + "\n\n" +
        "\\begintext\n")
    file.write(text)


def test_all():

    # ---- programs ------------
     spk_ck_creation()
     create_mk()
     stations()
     instrument()
     spk_propagation()
     ck_propagation()
     create_observations()
     create_targets()
     create_input_spk_tle()

    # ---- functions ------------
     ck_coverage()
     cosmographia_earth()
     cosmographia_sensor()
     FOV_basemap()
     gf_illumination()
     gf_target_in_FOV()
     minimal_distances()
     occultation_computation()
     plot_angle_offset_target()
     plot_angle_offset_target()
     plot_pass()
     quaternion_angle()
     quaternion_computation()

     print('Everything worked as expected!')


#test_all()

# ---- programs ------------
# spk_ck_creation()
# create_mk()
# stations()
# instrument()
# spk_propagation()
# ck_propagation()
# create_observations()
# create_targets()
# create_input_spk_tle()

    # ---- functions ------------
# ck_coverage()
# cosmographia_earth()
# cosmographia_sensor()
# FOV_basemap()
# gf_illumination()
# gf_target_in_FOV()
# minimal_distances()
# occultation_computation()
# plot_angle_offset_target()
# plot_angle_offset_target()
# plot_pass()
# quaternion_angle()
# quaternion_computation()


# ------ storage --------
# -----------------------------------------------
# -----------------------------------------------


def storage_elevation():
    '''
    This script is designed to find every pass for values above a minimal elevation for a topocentric target system. It can
    be used to crosscheck the spk files with external routines to provide the same output. It should be used to find the
    pass times for GS passes of interest. Furthermore it can be used to determine times when the FLP could theoretically see
    a specific target.
    Note that there is already a better script (plot_pass) available to check for pass times.

    :return: Nothing
    '''

    flp_path = path_setup()

    # load metakernel
    cspice.furnsh(flp_path.mk_out)

    # define input data
    begin = "2018 MAY 08 12:00:00"
    end = "2018 MAY 09 12:00:00"

    target = 'FLP'
    frame = 'IRS_TOPO'
    abcorr = 'NONE'
    obsrvr = 'IRS'
    crdsys = 'LATITUDINAL'
    coord = 'LATITUDE'
    relate = '>'

    refval = 80 * cspice.rpd()  # elevation [degree]
    adjust = 0
    step = 20 * 60  # stepsize [min]
    nintvls = 7500

    result = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
    cnfine = cspice.utils.support_types.SPICEDOUBLE_CELL(2)

    # pre-computations
    et_begin = cspice.utc2et(begin)
    et_end = cspice.utc2et(end)
    ET = [et_begin, et_end]
    cspice.appndd(ET, cnfine)

    # compute gf search
    # elevation above x [degrees]
    cspice.gfposc(target, frame, abcorr, obsrvr, crdsys, coord, relate, refval, adjust, step, nintvls, cnfine, result)

    print(target, frame, abcorr, obsrvr, crdsys, coord, relate, refval, adjust, step, nintvls, cnfine, result)

    # max. elevation time
    relate_2 = 'LOCMAX'
    result_2 = cspice.utils.support_types.SPICEDOUBLE_CELL(15000)
    cspice.gfposc(target, frame, abcorr, obsrvr, crdsys, coord, relate_2, refval, adjust, step, nintvls, result,
                  result_2)

    # print results
    count = cspice.wncard(result)
    count_2 = cspice.wncard(result_2)
    print(count)
    print(count_2)

    if (count == 0):
        print("Result window is empty.\n\n")
    else:
        for i in range(count):
            # Fetch the endpoints of the Ith interval of the result window.
            left, right = cspice.wnfetd(result, i)
            max_el_l, max_el_r = cspice.wnfetd(result_2, i)

            if (max_el_l == max_el_r):
                max_el = max_el_l

                if (left == right):
                    time = cspice.et2utc(left, 'C', 3)
                    print("Event time: " + time + "\n")
                else:
                    time_left = cspice.et2utc(left, 'C', 3)
                    time_right = cspice.et2utc(right, 'C', 3)
                    time_max = cspice.et2utc(max_el, 'C', 3)
                    print("Interval " + str(i + 1) + "\n")
                    print("From : " + time_left + " \n")
                    print("To   : " + time_right + " \n")
                    print("Max  : " + time_max + " \n")
                    print(" \n")


def storage_long_lat_box():
    '''
    This script needs documentation...

    :return: Nothing
    '''

    flp_path = path_setup()

    # use GFSNTC four times to define search box
    # define input data
    begin = "2018 May 08 10:00:00"
    end = "2018 MAY 08 16:00:00"
    step = 200  # stepsize [s]

    latitude_min = 45  # default = -90
    latitude_max = 55  # default = 90
    longitude_min = 5  # default = -179.99 (because -180 is not allowed)
    longitude_max = 15  # default = 180

    # "Predefined" parameters for gfsntc and gfilum
    target = 'EARTH'  #
    frame = 'ITRF93'
    method = "Ellipsoid"
    obsrvr = 'FLP'
    abcorr_sntc = 'NONE'
    dref = 'FLP_PAMCAM'
    dvec = [0, 0, 1]
    crdsys = 'GEODETIC'
    adjust = 0
    nintvls = 750
    cnfine = cspice.utils.support_types.SPICEDOUBLE_CELL(2)

    # load metakernel
    cspice.furnsh(flp_path.mk_out)

    # pre-computations
    et_begin = cspice.utc2et(begin)
    et_end = cspice.utc2et(end)
    ET = [et_begin, et_end]
    cspice.appndd(ET, cnfine)

    # compute gf search:

    # latitude above  x [degrees]
    coord = 'LATITUDE'
    refval = latitude_min * cspice.rpd()  # now in [rad]
    relate = '>'
    result = cspice.utils.support_types.SPICEDOUBLE_CELL(1500)
    cspice.gfsntc(target, frame, method, abcorr_sntc, obsrvr, dref, dvec, crdsys, coord, relate, refval, adjust, step,
                  nintvls, cnfine, result)
    count = cspice.wncard(result)
    print(count)

    # latitude below  x [degrees]
    coord = 'LATITUDE'
    refval = latitude_max * cspice.rpd()  # now in [rad]
    relate = '<'
    result_2 = cspice.utils.support_types.SPICEDOUBLE_CELL(1500)
    cspice.gfsntc(target, frame, method, abcorr_sntc, obsrvr, dref, dvec, crdsys, coord, relate, refval, adjust, step,
                  nintvls, result, result_2)
    count = cspice.wncard(result_2)
    print(count)

    # latitude above  x [degrees]
    coord = 'LONGITUDE'
    refval = longitude_min * cspice.rpd()  # now in [rad]
    relate = '>'
    result_3 = cspice.utils.support_types.SPICEDOUBLE_CELL(1500)
    cspice.gfsntc(target, frame, method, abcorr_sntc, obsrvr, dref, dvec, crdsys, coord, relate, refval, adjust, step,
                  nintvls, result_2, result_3)
    count = cspice.wncard(result_3)
    print(count)

    # latitude below  x [degrees]
    coord = 'LONGITUDE'
    refval = longitude_max * cspice.rpd()  # now in [rad]
    relate = '<'
    result_4 = cspice.utils.support_types.SPICEDOUBLE_CELL(1500)
    cspice.gfsntc(target, frame, method, abcorr_sntc, obsrvr, dref, dvec, crdsys, coord, relate, refval, adjust, step,
                  nintvls, result_3, result_4)
    count = cspice.wncard(result_4)
    print(count)

    # print results
    if (count == 0):
        print("Result window is empty.\n\n")
    else:
        for i in range(count):
            # Fetch the endpoints of the Ith interval of the result window.
            left, right = cspice.wnfetd(result_4, i)

            if (left == right):
                time = cspice.et2utc(left, 'C', 3)
                print("Event time: " + time + "\n")
            else:
                time_left = cspice.et2utc(left, 'C', 3)
                time_right = cspice.et2utc(right, 'C', 3)
                print("Interval " + str(i + 1) + "\n")
                print("From : " + time_left + " \n")
                print("To   : " + time_right + " \n")
                print(" \n")


def storage_illumination_simple():
    '''
    This routine might be useful to check the illumination condition for photos done in the past.

    :return:
    '''

    flp_path = path_setup()

    # load kernels
    cspice.furnsh(flp_path.mk_out)
    radii = cspice.bodvrd('Earth', 'RADII', 3)[1]
    f = (radii[0] - radii[2]) / radii[0]

    # define point (roughly on Earth surface)
    alt = 0.4  # [km]
    lon = 10  # [degrees]
    lat = 50  # [degress]

    # convert planetocentric r/lon/lat to Cartesian vector
    lon_rad = lon * cspice.rpd()
    lat_rad = lat * cspice.rpd()
    point = cspice.georec(lon_rad, lat_rad, alt, radii[0], f)

    # convert UTC to ET
    et = cspice.str2et('2018 MAY 08 01:20:00')

    # compute illumination angles modeling Earth as an ellipsoid
    trgepc, srfvec, phase, solar, emissn = cspice.ilumin('Ellipsoid', 'Earth', et, 'ITRF93', 'LT+S', 'FLP', point)

    # distance between target point and FLP: [km]
    dist = cspice.vnorm(srfvec)
    print(dist)

    # angle between the spoint-obsrvr vector and the spoint-sun vector (Sun-spoint-FLP): [degrees]
    angle_1 = phase * cspice.dpr()
    print(angle_1)

    # angle between the surface normal vector at 'spoint' and the spoint-sun vector: [degrees]
    angle_2 = solar * cspice.dpr()
    print(angle_2)

    # angle between the surface normal vector at 'spoint' and the spoint-observer vector: [degrees]
    angle_3 = emissn * cspice.dpr()
    print(angle_3)


def storage_spk_comparison():
    '''
    This script needs documentation.
    :return: Nothing
    '''

    flp_path = path_setup()

    time_start = '2018 MAY 08 16:30:00'
    time_end = '2018 MAY 08 20:30:00'

    # first number = drag area *1000
    # second number = flux area *10
    # third number = order of gravitational potential
    # fourth: gravity field model
    # fifth: kg
    # sixth + seventh: date + time
    name = 'docu'

    # load standard kernels
    cspice.furnsh(flp_path.lsk_file)
    cspice.furnsh(flp_path.pck_itrf)
    # get ET timeline and plot timeline
    et1 = cspice.utc2et(time_start)
    et2 = cspice.utc2et(time_end)
    if et2 - et1 < 100000:
        points = et2 - et1
    else:
        points = 100000
    ET = np.linspace(et1, et2, points)
    style = 'YYYY-MM-DD-HR-MN-SC.###'
    time = cspice.timout(ET, style, 23)
    timeline = []
    for i in range(len(ET)):
        sub_2 = time[i].replace(".", "-").split("-")
        timeline.append(datetime.datetime(int(sub_2[0]), int(sub_2[1]), int(sub_2[2]),
                                          int(sub_2[3]), int(sub_2[4]), int(sub_2[5]), int(sub_2[6]), tzinfo=None))

    # SPK data GPS (including GMAT gap fixes!)
    cspice.furnsh(flp_path.compare_spk_long_term_TM)
    cspice.furnsh(flp_path.compare_spk_long_term_TM_gaps)
    spk_data_gps = np.array([cspice.spkezr('-513', ET[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET))])
    cspice.furnsh(flp_path.compare_spk_long_term_TM_gaps)
    cspice.unload(flp_path.compare_spk_long_term_TM)

    # SPK data GMAT
    cspice.furnsh(flp_path.spk_prop_name)
    spk_data_gmat = np.array([cspice.spkezr('-513', ET[i], 'ITRF93', 'NONE', 'EARTH')[0] for i in range(len(ET))])
    cspice.unload(flp_path.spk_prop_name)

    diff_pos = np.array([np.abs(spk_data_gps[i, 0] - spk_data_gmat[i, 0]) \
                         + np.abs(spk_data_gps[i, 1] - spk_data_gmat[i, 1]) \
                         + np.abs(spk_data_gps[i, 2] - spk_data_gmat[i, 2]) \
                         for i in range(len(spk_data_gps))])

    diff_vel = np.array([np.abs(spk_data_gps[i, 3] - spk_data_gmat[i, 3]) \
                         + np.abs(spk_data_gps[i, 4] - spk_data_gmat[i, 4]) \
                         + np.abs(spk_data_gps[i, 5] - spk_data_gmat[i, 5]) \
                         for i in range(len(spk_data_gps))])

    # plot x position
    fig, ax = plt.subplots(figsize=(10, 7))
    plt.plot(timeline, spk_data_gps[:, 0], label='gps spk x')
    plt.plot(timeline, spk_data_gmat[:, 0], label='gmat spk x')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=3)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.25)
    plt.show()

    # plot x,y,z position difference and sart(sqaured) difference
    fig, ax = plt.subplots(figsize=(10, 7))
    plt.plot(timeline, spk_data_gps[:, 0] - spk_data_gmat[:, 0], label='diff x pos')
    plt.plot(timeline, spk_data_gps[:, 1] - spk_data_gmat[:, 1], label='diff x pos')
    plt.plot(timeline, spk_data_gps[:, 2] - spk_data_gmat[:, 2], label='diff x pos')
    plt.plot(timeline, diff_pos, '--', label='diff sum pos')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=4)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.25)
    fig.savefig(flp_path.plots + 'spk_prop_long_term_2_' + name + '.png', dpi=fig.dpi)
    plt.show()

    # calculate vector length
    vec_len_gps = np.array(
        [np.sqrt(np.square(spk_data_gps[k][0]) + np.square(spk_data_gps[k][1]) + np.square(spk_data_gps[k][2]))
         for k in range(len(spk_data_gps))])

    vec_len_gmat = np.array(
        [np.sqrt(np.square(spk_data_gmat[k][0]) + np.square(spk_data_gmat[k][1]) + np.square(spk_data_gmat[k][2]))
         for k in range(len(spk_data_gmat))])

    fig, ax = plt.subplots(figsize=(10, 7))
    ax.set_ylabel('distance [km]', fontsize=18)
    plt.plot(timeline, vec_len_gps, label='spk gps', color='green')
    plt.plot(timeline, vec_len_gmat, label='spk gmat', color='red')
    plt.gcf().autofmt_xdate()
    ax.legend(fontsize=18, loc='upper center', bbox_to_anchor=(0.5, -0.22), ncol=2)
    for tick in ax.xaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    for tick in ax.yaxis.get_major_ticks():
        tick.label.set_fontsize(15)
    plt.grid()
    fig.subplots_adjust(bottom=0.25)
    plt.show()


#storage_elevation()
#storage_long_lat_box()
#storage_illumination_simple()
#storage_spk_comparison()
