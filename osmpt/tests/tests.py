import os
import subprocess
import spiceypy as cspice


def path_setup():
    '''
    This script stores all hard coded paths used within this project. It therefore defines the folder structure of the
    project. The paths philosophy follows the below mentioned ideas:
    - keep the folder structure simple and intuitive.
    - create new folders where needed to remove old files easily without affecting other parts of the project
    - keep names during development phase to not produce new errors even it more intuitive folder names/combinations might
    be useful until you do a general update...
    Nevertheless the paths are grouped dependent on there first script usage (following a "normal" user flow).
    So far, no hard naming conventions were defined.
    :return: All paths needed
    '''

    class flp_path_class:

        # the paths are ordered the way they are first used except naif input files, which are listed first
        def __init__(self):
            # define path beginnings:
            home_dir = '/home/flp/'

            # directory for git: (development)
            project_dir = '/home/flp/mpt/mpt/mpt/'
            # directory for pypi:
            #project_dir = '/home/flp/mpt/'

            # naif input files
            self.lsk_file = project_dir + 'kernels/lsk/naif/naif0012.tls'
            self.pck_normal = project_dir + 'kernels/pck/naif/pck00010.tpc'
            self.pck_itrf = project_dir + 'kernels/pck/naif/earth_000101_180827_180605.bpc'
            self.pck_geo = project_dir + 'kernels/pck/naif/geophysical.ker'
            self.spk_file = project_dir + 'kernels/spk/naif/de432s.bsp'

            # static self-generated files
            self.sclk_file = project_dir + 'kernels/sclk/flp.tsc'
            self.fk_file = project_dir + 'kernels/fk/FLP.tf'
            self.ik_file = project_dir + 'kernels/ik/instruments.ti'

            # spk_ck_creation
            self.TM_path = project_dir + 'TM/'
            self.spk_source = project_dir + 'kernels/spk/source/'
            self.spk_master_mkspk_setup = project_dir + 'kernels/spk/spk_master_mkspk_setup_auto'
            self.spk_master_mkspk_comment = project_dir + 'kernels/spk/spk_master_mkspk_comment'
            self.spk_new = project_dir + 'kernels/spk/new/'
            self.spk_out_name = project_dir + 'kernels/spk/out/spk_master_merge_new.bsp'
            self.spk_master_merge_setup_1 = project_dir + 'kernels/spk/spk_master_spkmerge_setup'
            self.spk_out = project_dir + 'kernels/spk/out/'
            self.ck_source = project_dir + 'kernels/ck/source/'
            self.ck_master_setup = project_dir + 'kernels/ck/msopck.setup'
            self.ck_master_comments = project_dir + 'kernels/ck/msopck.comments'
            self.ck_out = project_dir + 'kernels/ck/out/'
            self.ck_out_name = project_dir + 'kernels/ck/out/ck_master.bc'

            # gmat related pathes (gaps)
            self.gmat = home_dir + 'GMAT/R2017a/bin/'
            self.gmat_default_script = project_dir + 'kernels/spk/spk_gmat_default.script'
            self.spk_gmat_merge_setup = project_dir + 'kernels/spk/spk_gmat_merge_setup'
            self.gmat_storage = project_dir + 'kernels/spk/new/'
            self.gmat_out = project_dir + 'kernels/spk/gmat/'
            self.gmat_out_name = project_dir + 'kernels/spk/gmat/spk_gmat_merge_new.bsp'

            # create target
            self.json_target_old = project_dir + 'json/targets/old/'
            self.json_target_new = project_dir + 'json/targets/new/'
            self.json_scenario = project_dir + 'json/load_flp_scenario.json'
            self.spk_target_setup = project_dir + 'kernels/spk/spk_target_setup'
            self.spk_target_id_code = project_dir + 'kernels/spk/ID_code_critical.txt'
            self.spk_target = project_dir + 'kernels/spk/targets/'
            self.fk_target = project_dir + 'kernels/fk/targets/'

            # create observation
            self.json_obs_old = project_dir + 'json/observations/old/'
            self.json_obs_new = project_dir + 'json/observations/new/'

            # create mk
            self.mk_sample_path = project_dir + 'kernels/'
            self.mk_out = project_dir + 'kernels/mk/out/FLP.tm'

            # create stations
            self.spk_station_spk = project_dir + 'kernels/spk/stations/stations.bsp'
            self.spk_station = project_dir + 'kernels/spk/stations/'
            self.spk_station_setup = project_dir + 'kernels/spk/station_setup'
            self.fk_station_fk = project_dir + 'kernels/fk/stations/stations.tf'
            self.fk_station = project_dir + 'kernels/fk/stations/'

            # create instrument spk
            self.spk_instrument_spk = project_dir + 'kernels/spk/instrument/instrument.bsp'
            self.spk_instrument = project_dir + 'kernels/spk/instrument/'
            self.spk_instrument_setup = project_dir + 'kernels/spk/instrument_setup'

            # cosmographia (open)
            self.cosmographia = home_dir + 'cosmographia-3.0/'
            self.cosmo_script_earth = project_dir + 'json/script/script_earth.py'
            self.cosmo_script_sensor = project_dir + 'json/script/script_sensor.py'
            self.cosmo_json = project_dir + 'json/load_flp_scenario.json'

            # plots
            self.plots = project_dir + 'plots/'

            # produced CK
            self.TM_produced = project_dir + 'TM/produced_quat/'

            # gmat related pathes (future propagation):
            self.gmat_storage_future = project_dir + 'kernels/spk/storage/'
            self.spk_prop = project_dir + 'kernels/spk/propagation/'
            self.spk_prop_name = project_dir + 'kernels/spk/propagation/spk_gmat_prop_new.bsp'
            self.compare_spk_long_term_TM = project_dir + 'TM/long_term_storage/spk_master_merge_new.bsp'
            self.compare_spk_long_term_TM_gaps = project_dir + 'TM/long_term_storage/spk_gmat_merge_new.bsp'

            # predicted CK
            self.ck_prop = project_dir + 'kernels/ck/propagation/'
            self.ck_prop_furnsh = project_dir + 'kernels/ck/ck_prop_furnsh'
            self.ck_prop_idle_ck = project_dir + 'kernels/ck/propagation/ck_prop_0_idle.bc'
            self.ck_prop_idle_spec = project_dir + 'kernels/ck/ck_prop_idle_spec'
            self.ck_prop_nadir_ck = project_dir + 'kernels/ck/propagation/ck_prop_1_nadir.bc'
            self.ck_prop_nadir_spec = project_dir + 'kernels/ck/ck_prop_nadir_spec'
            self.ck_prop_pass_all_ck = project_dir + 'kernels/ck/propagation/ck_prop_2_pass_all.bc'
            self.ck_prop_pass_all_spec = project_dir + 'kernels/ck/ck_prop_pass_all_spec'
            self.ck_prop_moon_pointing_ck = project_dir + 'kernels/ck/propagation/ck_prop_4_moon_pointing.bc'
            self.ck_prop_moon_pointing_spec = project_dir + 'kernels/ck/ck_prop_moon_pointing_spec'
            self.ck_prop_nadir_offset_ck = project_dir + 'kernels/ck/propagation/ck_prop_5_nadir_offset.bc'
            self.ck_prop_nadir_offset_spec = project_dir + 'kernels/ck/ck_prop_nadir_offset_spec'

            # SPK produced by TLE
            self.spk_source_tle = project_dir + 'kernels/spk/tle_input/FLP.tle'
            self.spk_setup_tle = project_dir + 'kernels/spk/spk_setup_tle'
            self.spk_comment_tle = project_dir + 'kernels/spk/spk_tle_comment'
            self.spk_out_name_tle = project_dir + 'kernels/spk/tle_output/spk_tle.bsp'
            self.spk = project_dir + 'kernels/spk/'


    flp_path = flp_path_class()

    return flp_path

