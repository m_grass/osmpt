def test_1(a=5, b=10):
    '''
    This function calculates the rectangle area for the 2 side parameters specified.

    :param a: first side
     :type float
    :param b: second side
     :type float
    :return: nothing

    '''

    area = a*b
    print(area)

    return area


def test_2(a=5, b=10):
    '''
    This function calculates the rectangle area for the 2 side parameters specified.

    :param a: first side
     :type float
    :param b: second side
     :type float
    :return: nothing

    '''

    area = a*b
    print(area)

    return area
