import cosmoscripting

cosmo = cosmoscripting.Cosmo()

cosmo.hideTrajectory('Moon')
cosmo.hideTrajectory('Mercury')
cosmo.hideTrajectory('Venus')
cosmo.hideTrajectory('Earth')
cosmo.hideTrajectory('Mars')
cosmo.hideTrajectory('Jupiter')
cosmo.hideTrajectory('Saturn')
cosmo.hideTrajectory('Uranus')
cosmo.hideTrajectory('Neptune')

cosmo.showObject('FLP_OSIRIS')
cosmo.showObject('FLP_PAMCAM')
cosmo.showObject('FLP_MICS')



#cosmo.quit()