.. osmpt documentation master file, created by
   sphinx-quickstart on Thu Jun 21 22:54:21 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to osmpt's documentation!
=================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:


osmpt package
=============

Submodules
----------

osmpt.main module
-----------------

.. automodule:: osmpt.main
    :members:
    :undoc-members:
    :show-inheritance:

osmpt.test\_1 module
--------------------

.. automodule:: osmpt.test_1
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: osmpt
    :members:
    :undoc-members:
    :show-inheritance:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
